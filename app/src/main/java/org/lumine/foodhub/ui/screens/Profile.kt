package org.lumine.foodhub.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import org.lumine.foodhub.R
import org.lumine.foodhub.ui.styles.HydroBlue
import org.lumine.foodhub.ui.styles.Orange

@Composable
fun Profile(goBack: () -> Unit, goToWelcome: () -> Unit) {
    var fullName by remember { mutableStateOf(value = "Sangonomiya Kokomi") }
    var emailAddress by remember { mutableStateOf(value = "username@gmail.com") }
    var phoneNumber by remember { mutableStateOf(value = "+1 (420) 2345 6789") }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(32.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            BackButton(onClick = { goBack() })
        }
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Box(contentAlignment = Alignment.BottomEnd) {
                ProfilePicture(image = R.drawable.kokomi)
                ProfileEditButton(
                    icon = R.drawable.round_photo_camera_24,
                    onClick = { /*TODO*/ }
                )
            }
            Spacer(modifier = Modifier.height(32.dp))
            InputField(
                text = R.string.full_name,
                value = fullName,
                onValueChange = { fullName = it },
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            )
            Spacer(modifier = Modifier.height(16.dp))
            InputField(
                text = R.string.email_address,
                value = emailAddress,
                onValueChange = { emailAddress = it },
                keyboardType = KeyboardType.Email,
                imeAction = ImeAction.Next
            )
            Spacer(modifier = Modifier.height(16.dp))
            InputField(
                text = R.string.phone_number,
                value = phoneNumber,
                onValueChange = { phoneNumber = it },
                keyboardType = KeyboardType.Phone,
                imeAction = ImeAction.Done
            )
        }
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 100.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            LogOutButton(onClick = { goToWelcome() })
        }
    }
}

@Composable
fun ProfilePicture(image: Int) {
    Image(
        painter = painterResource(id = image),
        contentDescription = null,
        modifier = Modifier
            .size(128.dp)
            .shadow(elevation = 8.dp, shape = CircleShape)
            .clip(shape = CircleShape)
            .background(color = HydroBlue)
            .border(width = 8.dp, color = Color.White, shape = CircleShape)
    )
}

@Composable
fun ProfileEditButton(icon: Int, onClick: () -> Unit) {
    IconButton(
        onClick = onClick,
        modifier = Modifier
            .shadow(elevation = 8.dp, shape = CircleShape)
            .background(color = Color.White, shape = CircleShape)
    ) {
        Icon(
            imageVector = ImageVector.vectorResource(id = icon),
            contentDescription = null
        )
    }
}

@Composable
fun LogOutButton(onClick: () -> Unit) {
    Button(onClick = onClick, shape = RoundedCornerShape(percent = 50)) {
        Icon(
            imageVector = ImageVector.vectorResource(id = R.drawable.round_power_settings_new_24),
            contentDescription = null,
            tint = Orange,
            modifier = Modifier.background(color = Color.White, shape = CircleShape)
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = stringResource(id = R.string.log_out))
    }
}