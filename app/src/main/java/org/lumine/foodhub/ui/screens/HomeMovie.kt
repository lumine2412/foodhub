package org.lumine.foodhub.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import coil.request.ImageRequest
import org.lumine.foodhub.R
import org.lumine.foodhub.data.Movie
import org.lumine.foodhub.data.Response
import org.lumine.foodhub.ui.styles.LightGray
import org.lumine.foodhub.ui.styles.Typography
import org.lumine.foodhub.ui.viewmodels.HomeMovieViewModel

enum class Layout {
    List,
    Grid
}

enum class View {
    NowPlaying,
    TopRated
}

@Composable
fun HomeMovie(goBack: () -> Unit, goToProfile: () -> Unit) {
    val viewModel: HomeMovieViewModel = viewModel()
    var responseNowPlaying by remember { mutableStateOf(Response()) }
    var responseTopRated by remember { mutableStateOf(Response()) }

    var layout by remember { mutableStateOf(Layout.List) }
    var view by remember { mutableStateOf(View.NowPlaying) }
    var response by remember { mutableStateOf(Response()) }

    LaunchedEffect(Unit) {
        responseNowPlaying = viewModel.getNowPlaying()
        responseTopRated = viewModel.getTopRated()
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 32.dp, bottom = 16.dp, start = 32.dp, end = 32.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            BackButton(onClick = goBack)
            Text(text = stringResource(id = R.string.my_movies))
            ProfileIcon(
                onClick = goToProfile,
                icon = R.drawable.kokomi
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 0.dp, bottom = 16.dp, start = 32.dp, end = 32.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            TextButton(onClick = {
                view = when (view) {
                    View.NowPlaying -> View.TopRated
                    View.TopRated -> View.NowPlaying
                }
            }) {
                when (view) {
                    View.NowPlaying -> Text(
                        text = stringResource(id = R.string.now_playing),
                        color = Color.Black
                    )
                    View.TopRated -> Text(
                        text = stringResource(id = R.string.top_rated),
                        color = Color.Black
                    )
                }
            }
            IconButton(onClick = {
                layout = when (layout) {
                    Layout.List -> Layout.Grid
                    Layout.Grid -> Layout.List
                }
            }) {
                when (layout) {
                    Layout.List -> Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.round_view_list_24),
                        contentDescription = null
                    )
                    Layout.Grid -> Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.round_grid_view_24),
                        contentDescription = null
                    )
                }
            }
        }
        response = when (view) {
            View.NowPlaying -> responseNowPlaying
            View.TopRated -> responseTopRated
        }
        when (layout) {
            Layout.List -> {
                MovieColumn(response = response)
            }
            Layout.Grid -> {
                MovieGrid(response = response)
            }
        }
    }
}

@Composable
fun MovieColumn(response: Response) {
    var selectedMovie: Movie? by remember { mutableStateOf(value = null) }

    LazyColumn(
        contentPadding = PaddingValues(start = 32.dp, top = 0.dp, end = 32.dp, bottom = 32.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        items(response.results.orEmpty()) { movie ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .shadow(elevation = 16.dp, shape = RoundedCornerShape(percent = 5))
                    .background(color = LightGray, shape = RoundedCornerShape(percent = 5))
                    .clip(shape = RoundedCornerShape(percent = 5))
                    .clickable(onClick = { selectedMovie = movie }),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data("https://image.tmdb.org/t/p/w500${movie.posterPath}")
                        .crossfade(durationMillis = 500)
                        .build(),
                    contentDescription = null,
                    modifier = Modifier.size(80.dp),
                    contentScale = ContentScale.Crop,
                    placeholder = painterResource(id = R.drawable.round_image_24)
                )
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                ) {
                    Text(
                        text = movie.title!!,
                        fontWeight = FontWeight.Medium,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = movie.overview!!,
                        fontSize = Typography.body2.fontSize,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
    }

    selectedMovie?.let { movie ->
        AlertDialog(
            onDismissRequest = { /*TODO*/ },
            title = { Text(text = movie.title!!) },
            text = {
                Column {
                    Text(text = movie.overview!!)
                    Spacer(modifier = Modifier.height(24.dp))
                    AsyncImage(
                        model = ImageRequest.Builder(LocalContext.current)
                            .data("https://image.tmdb.org/t/p/w500${movie.posterPath}")
                            .crossfade(durationMillis = 500)
                            .build(),
                        contentDescription = null,
                        modifier = Modifier.height(400.dp),
                        contentScale = ContentScale.Fit,
                        placeholder = painterResource(id = R.drawable.round_image_24)
                    )
                }
            },
            confirmButton = {
                TextButton(onClick = { selectedMovie = null }) {
                    Text(text = stringResource(id = R.string.close))
                }
            },
        )
    }
}

@Composable
fun MovieGrid(response: Response) {
    var selectedMovie: Movie? by remember { mutableStateOf(value = null) }

    LazyVerticalGrid(
        columns = GridCells.Fixed(count = 2),
        contentPadding = PaddingValues(start = 32.dp, top = 0.dp, end = 32.dp, bottom = 32.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        items(response.results.orEmpty()) { movie ->
            Column(
                modifier = Modifier
                    .shadow(elevation = 16.dp, shape = RoundedCornerShape(percent = 5))
                    .background(color = LightGray, shape = RoundedCornerShape(percent = 5))
                    .clip(shape = RoundedCornerShape(percent = 5))
                    .clickable(onClick = { selectedMovie = movie })
            ) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data("https://image.tmdb.org/t/p/w500${movie.posterPath}")
                        .crossfade(durationMillis = 500)
                        .build(),
                    contentDescription = null,
                    modifier = Modifier.fillMaxWidth(),
                    contentScale = ContentScale.Crop,
                    placeholder = painterResource(id = R.drawable.round_image_24)
                )
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    Text(
                        text = movie.title!!,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                    Text(
                        text = movie.overview!!,
                        fontSize = Typography.body2.fontSize,
                        letterSpacing = Typography.body2.letterSpacing,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
    }

    selectedMovie?.let { movie ->
        AlertDialog(
            onDismissRequest = { /*TODO*/ },
            title = { Text(text = movie.title!!) },
            text = {
                Column {
                    Text(text = movie.overview!!)
                    Spacer(modifier = Modifier.height(24.dp))
                    AsyncImage(
                        model = ImageRequest.Builder(LocalContext.current)
                            .data("https://image.tmdb.org/t/p/w500${movie.posterPath}")
                            .crossfade(durationMillis = 500)
                            .build(),
                        contentDescription = null,
                        modifier = Modifier.height(400.dp),
                        contentScale = ContentScale.Fit,
                        placeholder = painterResource(id = R.drawable.round_image_24)
                    )
                }
            },
            confirmButton = {
                TextButton(onClick = { selectedMovie = null }) {
                    Text(text = stringResource(id = R.string.close))
                }
            },
        )
    }
}