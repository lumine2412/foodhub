package org.lumine.foodhub.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import org.lumine.foodhub.R
import org.lumine.foodhub.data.Restaurant
import org.lumine.foodhub.data.restaurantList
import org.lumine.foodhub.ui.styles.HydroBlue
import org.lumine.foodhub.ui.styles.LightGray
import org.lumine.foodhub.ui.styles.Orange
import org.lumine.foodhub.ui.styles.Typography

@Composable
fun HomeRestaurant(goBack: () -> Unit = {}, goToProfile: () -> Unit = {}) {
    var layout by remember { mutableStateOf(Layout.List) }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 32.dp, bottom = 16.dp, start = 32.dp, end = 32.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            BackButton(onClick = { goBack() })
            Text(text = stringResource(id = R.string.my_restaurants))
            ProfileIcon(
                onClick = { goToProfile() },
                icon = R.drawable.kokomi
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 0.dp, bottom = 16.dp, start = 32.dp, end = 32.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            when (layout) {
                Layout.List -> Text(
                    text = stringResource(id = R.string.list_view),
                    color = Color.Black
                )
                Layout.Grid -> Text(
                    text = stringResource(id = R.string.grid_view),
                    color = Color.Black
                )
            }
            IconButton(onClick = {
                layout = when (layout) {
                    Layout.List -> Layout.Grid
                    Layout.Grid -> Layout.List
                }
            }) {
                when (layout) {
                    Layout.List -> Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.round_view_list_24),
                        contentDescription = null
                    )
                    Layout.Grid -> Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.round_grid_view_24),
                        contentDescription = null
                    )
                }
            }
        }
        when (layout) {
            Layout.List -> RestaurantListView()
            Layout.Grid -> RestaurantGridView()
        }
    }
}

@Composable
fun ProfileIcon(onClick: () -> Unit, icon: Int) {
    IconButton(onClick = onClick) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = null,
            tint = Color.Unspecified,
            modifier = Modifier
                .size(48.dp)
                .clip(shape = RoundedCornerShape(percent = 25))
                .background(color = HydroBlue, shape = RoundedCornerShape(percent = 25))
        )
    }
}

@Composable
fun RestaurantListView() {
    var selectedRestaurant by remember { mutableStateOf<Restaurant?>(value = null) }

    LazyColumn(
        contentPadding = PaddingValues(start = 32.dp, top = 0.dp, end = 32.dp, bottom = 32.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        items(restaurantList) { restaurant ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .shadow(elevation = 16.dp, shape = RoundedCornerShape(percent = 5))
                    .background(color = LightGray, shape = RoundedCornerShape(percent = 5))
                    .clip(shape = RoundedCornerShape(percent = 5))
                    .clickable(onClick = { selectedRestaurant = restaurant }),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(restaurant.image)
                        .crossfade(durationMillis = 500)
                        .build(),
                    contentDescription = restaurant.name,
                    modifier = Modifier.size(80.dp),
                    contentScale = ContentScale.Crop,
                    placeholder = painterResource(id = R.drawable.round_image_24)
                )
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                ) {
                    Text(
                        text = restaurant.name,
                        fontWeight = FontWeight.Medium,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = restaurant.address,
                        fontSize = Typography.body2.fontSize,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
    }

    selectedRestaurant?.let { restaurant ->
        AlertDialog(
            onDismissRequest = { },
            title = { Text(text = stringResource(id = R.string.ask_delete_restaurant)) },
            text = {
                Text(text = buildAnnotatedString {
                    append(text = stringResource(id = R.string.ask_delete_restaurant_name))
                    withStyle(style = SpanStyle(color = Orange)) { append(text = restaurant.name) }
                    append(text = "?")
                })
            },
            confirmButton = {
                Button(onClick = {
                    restaurantList.remove(element = restaurant)
                    selectedRestaurant = null
                }) {
                    Text(text = stringResource(id = R.string.delete))
                }
            },
            dismissButton = {
                TextButton(onClick = { selectedRestaurant = null }) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            }
        )
    }
}

@Composable
fun RestaurantGridView() {
    var selectedRestaurant by remember { mutableStateOf<Restaurant?>(value = null) }

    LazyVerticalGrid(
        columns = GridCells.Fixed(count = 2),
        contentPadding = PaddingValues(start = 32.dp, top = 0.dp, end = 32.dp, bottom = 32.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
        content = {
            items(restaurantList) { restaurant ->
                Column(
                    modifier = Modifier
                        .shadow(elevation = 16.dp, shape = RoundedCornerShape(percent = 5))
                        .background(
                            color = LightGray,
                            shape = RoundedCornerShape(percent = 5)
                        )
                        .clip(shape = RoundedCornerShape(percent = 5))
                        .clickable(onClick = { selectedRestaurant = restaurant })
                ) {
                    AsyncImage(
                        model = ImageRequest.Builder(LocalContext.current)
                            .data(restaurant.image)
                            .crossfade(durationMillis = 500)
                            .build(),
                        contentDescription = restaurant.name,
                        modifier = Modifier.fillMaxWidth(),
                        contentScale = ContentScale.Crop,
                        placeholder = painterResource(id = R.drawable.round_image_24)
                    )
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp)
                    ) {
                        Text(text = restaurant.name, maxLines = 1, overflow = TextOverflow.Ellipsis)
                        Text(
                            text = restaurant.address,
                            fontSize = Typography.body2.fontSize,
                            letterSpacing = Typography.body2.letterSpacing,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                }
            }
        }
    )

    selectedRestaurant?.let { restaurant ->
        AlertDialog(
            onDismissRequest = { },
            title = { Text(text = stringResource(id = R.string.ask_delete_restaurant)) },
            text = {
                Text(text = buildAnnotatedString {
                    append(text = stringResource(id = R.string.ask_delete_restaurant_name))
                    withStyle(style = SpanStyle(color = Orange)) { append(text = restaurant.name) }
                    append(text = "?")
                })
            },
            confirmButton = {
                Button(onClick = {
                    restaurantList.remove(element = restaurant)
                    selectedRestaurant = null
                }) {
                    Text(text = stringResource(id = R.string.delete))
                }
            },
            dismissButton = {
                TextButton(onClick = { selectedRestaurant = null }) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            }
        )
    }
}