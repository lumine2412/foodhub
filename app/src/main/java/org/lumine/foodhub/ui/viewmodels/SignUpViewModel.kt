package org.lumine.foodhub.ui.viewmodels

import android.content.Context
import android.widget.Toast
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import org.lumine.foodhub.R

class SignUpViewModel : ViewModel() {
    var name by mutableStateOf(value = "")
    var email by mutableStateOf(value = "")
    var password by mutableStateOf(value = "")

    fun signUp(context: Context, goToSignIn: () -> Unit) {
        val emailRegex = Regex(pattern = "[a-zA-Z\\d._%+-]+@[a-zA-Z\\d.-]+\\.[a-zA-Z]{2,}")
        val passwordRegex = Regex(pattern = ".{8,}")

        if (name.isEmpty() || email.isEmpty() || password.isEmpty()) {
            Toast.makeText(context, R.string.missing_info, Toast.LENGTH_SHORT).show()
        } else if (!email.matches(emailRegex) && !password.matches(passwordRegex)) {
            Toast.makeText(context, R.string.invalid_email_password, Toast.LENGTH_SHORT).show()
        } else if (!email.matches(emailRegex)) {
            Toast.makeText(context, R.string.invalid_email, Toast.LENGTH_SHORT).show()
        } else if (!password.matches(passwordRegex)) {
            Toast.makeText(context, R.string.invalid_password, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, R.string.sign_up_successfully, Toast.LENGTH_SHORT).show()
            goToSignIn()
        }
    }
}
