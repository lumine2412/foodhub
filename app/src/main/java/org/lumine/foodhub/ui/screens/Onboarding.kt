package org.lumine.foodhub.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch
import org.lumine.foodhub.R
import org.lumine.foodhub.ui.styles.*

@OptIn(ExperimentalPagerApi::class)
@Composable
fun Onboarding(goToWelcome: () -> Unit = {}) {
    val pagerState = rememberPagerState()
    val scope = rememberCoroutineScope()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 64.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
    ) {
        Box(
            modifier = Modifier.fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            HorizontalPager(count = 3, state = pagerState) { page ->
                when (page) {
                    0 -> PagerScreen(
                        image = R.drawable.onboarding_image1,
                        title = R.string.onboarding_title1,
                        description = R.string.onboarding_description
                    )
                    1 -> PagerScreen(
                        image = R.drawable.onboarding_image2,
                        title = R.string.onboarding_title2,
                        description = R.string.onboarding_description
                    )
                    2 -> PagerScreen(
                        image = R.drawable.onboarding_image3,
                        title = R.string.onboarding_title3,
                        description = R.string.onboarding_description
                    )
                }
            }
            HorizontalPagerIndicator(
                pagerState = pagerState,
                activeColor = Yellow,
                modifier = Modifier.offset(y = 80.dp)
            )
        }
        IconButton(
            onClick = {
                scope.launch {
                    when (pagerState.currentPage) {
                        0 -> pagerState.animateScrollToPage(1)
                        1 -> pagerState.animateScrollToPage(2)
                        2 -> goToWelcome()
                    }
                }
            },
            modifier = Modifier.background(color = Orange, shape = CircleShape)
        ) {
            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.round_arrow_forward_24),
                contentDescription = null,
                tint = Color.White
            )
        }
    }
}

@Composable
fun PagerScreen(image: Int, title: Int, description: Int) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(id = image),
            contentDescription = null,
            modifier = Modifier.size(400.dp)
        )
        Text(
            text = stringResource(id = title),
            textAlign = TextAlign.Center,
            fontSize = Typography.h4.fontSize
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = stringResource(id = description),
            textAlign = TextAlign.Center,
            fontSize = Typography.subtitle1.fontSize,
            color = DarkGray
        )
    }
}