package org.lumine.foodhub.ui.viewmodels

import android.content.Context
import android.widget.Toast
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import org.lumine.foodhub.R

class SignInViewModel : ViewModel() {
    var email by mutableStateOf(value = "")
    var password by mutableStateOf(value = "")

    fun signIn(context: Context, goToHome: () -> Unit) {
        if (email.isEmpty() && password.isEmpty()) {
            Toast.makeText(context, R.string.missing_info, Toast.LENGTH_SHORT).show()
        } else if (email == "username@gmail.com" && password == "12345678") {
            goToHome()
        } else {
            Toast.makeText(context, R.string.wrong_email_password, Toast.LENGTH_SHORT).show()
        }
    }
}