package org.lumine.foodhub.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import org.lumine.foodhub.R
import org.lumine.foodhub.ui.styles.Typography
import org.lumine.foodhub.ui.viewmodels.SignUpViewModel

@Composable
fun SignUp(goBack: () -> Unit, goToSignIn: () -> Unit) {
    val viewModel: SignUpViewModel = viewModel()
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(32.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            BackButton(onClick = { goBack() })
        }
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            AuthenticationTitle(text = R.string.sign_up)
            Spacer(modifier = Modifier.height(32.dp))
            InputField(
                text = R.string.full_name,
                value = viewModel.name,
                onValueChange = { viewModel.name = it },
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            )
            Spacer(modifier = Modifier.height(16.dp))
            InputField(
                text = R.string.email_address,
                value = viewModel.email,
                onValueChange = { viewModel.email = it },
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            )
            Spacer(modifier = Modifier.height(16.dp))
            PasswordField(
                text = R.string.password,
                value = viewModel.password,
                onValueChange = { viewModel.password = it },
                imeAction = ImeAction.Done
            )
            Spacer(modifier = Modifier.height(48.dp))
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                AuthenticationButton(
                    text = R.string.sign_up,
                    onClick = {
                        viewModel.signUp(
                            context = context,
                            goToSignIn = { goToSignIn() })
                    })
                Spacer(modifier = Modifier.height(16.dp))
                AskAuthenticate(
                    text1 = R.string.sign_in_ask,
                    color = Color.Black,
                    text2 = R.string.sign_in,
                    onClick = { goToSignIn() }
                )
            }
        }
        Column(modifier = Modifier.fillMaxWidth()) {
            GlowyAuthentication(text = R.string.sign_up_with, color = Color.Black)
        }
    }
}

@Composable
fun BackButton(onClick: () -> Unit) {
    IconButton(
        onClick = onClick,
        modifier = Modifier.background(
            color = Color.White,
            shape = RoundedCornerShape(percent = 25)
        )
    ) {
        Icon(
            imageVector = ImageVector.vectorResource(id = R.drawable.round_keyboard_arrow_left_24),
            contentDescription = null
        )
    }
}

@Composable
fun AuthenticationTitle(text: Int) {
    Text(
        text = stringResource(id = text),
        fontSize = Typography.h4.fontSize,
        letterSpacing = Typography.h4.letterSpacing,
        fontWeight = FontWeight.Medium
    )
}

@Composable
fun InputField(
    text: Int,
    value: String,
    onValueChange: (String) -> Unit,
    keyboardType: KeyboardType,
    imeAction: ImeAction
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = stringResource(id = text))
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier.fillMaxWidth(),
            shape = RoundedCornerShape(percent = 20),
            singleLine = true,
            keyboardOptions = KeyboardOptions(keyboardType = keyboardType, imeAction = imeAction)
        )
    }
}

@Composable
fun PasswordField(
    text: Int,
    value: String,
    onValueChange: (String) -> Unit,
    imeAction: ImeAction
) {
    var passwordVisibility: Boolean by remember { mutableStateOf(value = false) }
    val visibilityIcon = if (passwordVisibility) {
        R.drawable.round_visibility_off_24
    } else {
        R.drawable.round_visibility_24
    }

    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = stringResource(id = text))
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier.fillMaxWidth(),
            shape = RoundedCornerShape(percent = 20),
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = imeAction
            ),
            visualTransformation = if (passwordVisibility) {
                VisualTransformation.None
            } else {
                PasswordVisualTransformation()
            },
            trailingIcon = {
                IconButton(onClick = { passwordVisibility = !passwordVisibility }) {
                    Icon(
                        imageVector = ImageVector.vectorResource(id = visibilityIcon),
                        contentDescription = null
                    )
                }
            }
        )
    }
}

@Composable
fun AuthenticationButton(onClick: () -> Unit, text: Int) {
    Button(
        onClick = onClick,
        modifier = Modifier.size(width = 200.dp, height = 48.dp),
        shape = RoundedCornerShape(percent = 50)
    ) {
        Text(text = stringResource(id = text).uppercase())
    }
}