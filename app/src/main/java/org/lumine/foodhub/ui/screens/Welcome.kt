package org.lumine.foodhub.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import org.lumine.foodhub.R
import org.lumine.foodhub.ui.styles.DarkGray
import org.lumine.foodhub.ui.styles.Orange
import org.lumine.foodhub.ui.styles.Typography

@Composable
fun Welcome(
    goSkip: () -> Unit = {},
    goToSignUp: () -> Unit = {},
    goToSignIn: () -> Unit = {}
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .paint(
                painter = painterResource(id = R.drawable.welcome_background),
                contentScale = ContentScale.Crop
            )
            .background(
                brush = Brush.verticalGradient(
                    colors = listOf(
                        Color.Transparent,
                        DarkGray
                    )
                )
            )
            .padding(32.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End,
            verticalAlignment = Alignment.CenterVertically
        ) {
            SkipButton(
                onClick = { goSkip() },
                text = R.string.skip
            )
        }
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            WelcomeGreeting(text = R.string.app_name)
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 64.dp)
            ) {
                WelcomeDescription(text = R.string.welcome_description)
            }
        }
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            GlowyAuthentication(
                text = R.string.sign_up_with,
                color = Color.White
            )
            Spacer(modifier = Modifier.height(16.dp))
            StartWithEmailButton(
                text = R.string.start_with_email_address,
                onClick = { goToSignUp() })
            Spacer(modifier = Modifier.height(16.dp))
            AskAuthenticate(
                text1 = R.string.sign_in_ask,
                color = Color.White,
                text2 = R.string.sign_in,
                onClick = { goToSignIn() }
            )
        }
    }
}

@Composable
fun SkipButton(onClick: () -> Unit, text: Int) {
    Button(
        onClick = onClick,
        shape = RoundedCornerShape(percent = 50),
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.White)
    ) {
        Text(text = stringResource(id = text), color = Orange)
    }
}

@Composable
fun WelcomeGreeting(text: Int) {
    Text(text = buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                fontSize = Typography.h2.fontSize,
                letterSpacing = Typography.h2.letterSpacing,
                fontWeight = FontWeight.Medium,
            )
        ) {
            append(text = stringResource(id = R.string.welcome_to))
            withStyle(style = SpanStyle(color = Orange)) {
                append(text = stringResource(id = text))
            }
        }
    })
}

@Composable
fun WelcomeDescription(text: Int) {
    Text(
        text = stringResource(id = text),
        fontSize = Typography.h6.fontSize,
        letterSpacing = Typography.h6.letterSpacing
    )
}

@Composable
fun GlowyButton(icon: Int, text: Int) {
    Button(
        onClick = { /*TODO*/ },
        modifier = Modifier.fillMaxWidth(),
        shape = RoundedCornerShape(percent = 50),
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.White)
    ) {
        Icon(
            imageVector = ImageVector.vectorResource(id = icon),
            contentDescription = null,
            tint = Color.Unspecified,
            modifier = Modifier.size(32.dp)
        )
        Spacer(modifier = Modifier.width(4.dp))
        Text(
            text = stringResource(id = text).uppercase(),
            letterSpacing = Typography.body1.letterSpacing
        )
    }
}

@Composable
fun GlowyAuthentication(text: Int, color: Color) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = stringResource(id = text),
            color = color,
            fontSize = Typography.body2.fontSize
        )
        Spacer(modifier = Modifier.height(16.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(modifier = Modifier.weight(1f)) {
                GlowyButton(icon = R.drawable.logo_facebook, text = R.string.facebook)
            }
            Spacer(modifier = Modifier.width(12.dp))
            Box(modifier = Modifier.weight(1f)) {
                GlowyButton(icon = R.drawable.logo_google, text = R.string.google)
            }
        }
    }
}

@Composable
fun StartWithEmailButton(onClick: () -> Unit, text: Int) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp)
            .border(width = 2.dp, color = Color.White, shape = RoundedCornerShape(percent = 50)),
        shape = RoundedCornerShape(percent = 50),
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent)
    ) {
        Text(text = stringResource(id = text), color = Color.White)
    }
}

@Composable
fun AskAuthenticate(
    text1: Int,
    color: Color,
    text2: Int,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = stringResource(id = text1),
            color = color,
            fontSize = Typography.body2.fontSize,
            letterSpacing = Typography.body2.letterSpacing
        )
        Text(
            text = stringResource(id = text2),
            color = Orange,
            fontSize = Typography.body2.fontSize,
            letterSpacing = Typography.body2.letterSpacing,
            modifier = Modifier.clickable(onClick = onClick)
        )
    }
}