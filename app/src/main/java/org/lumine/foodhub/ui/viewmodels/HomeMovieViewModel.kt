package org.lumine.foodhub.ui.viewmodels

import androidx.lifecycle.ViewModel
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import org.lumine.foodhub.data.Response

class HomeMovieViewModel : ViewModel() {
    private val baseUrl = "api.themoviedb.org"
    private val client = HttpClient {
        defaultRequest {
            url {
                protocol = URLProtocol.HTTPS
                host = baseUrl
                path("3/", "movie/")
                parameters.append("api_key", "cff6d0bf70c83ace71a2b8e75da3295b")
                parameters.append("language", "en-US")
                parameters.append("page", "1")
            }
        }
        install(Logging) {
            logger = Logger.SIMPLE
            level = LogLevel.HEADERS
            filter { request ->
                request.url.host.contains("themoviedb")
            }
        }
        install(ContentNegotiation) {
            json(Json { ignoreUnknownKeys = true })
        }
    }

    suspend fun getNowPlaying(): Response {
        return client.get("now_playing").body()
    }

    suspend fun getTopRated(): Response {
        return client.get("top_rated").body()
    }
}