package org.lumine.foodhub.ui.styles

import androidx.compose.ui.graphics.Color

val Orange = Color(0xFFFE724C)
val Yellow = Color(0xFFFFC529)
val DarkGray = Color(0xFF30384F)
val Gray = Color(0xFF616772)
val LightGray = Color(0xFFC4C4C4)
val HydroBlue = Color(0xFF518DFF)