package org.lumine.foodhub.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import org.lumine.foodhub.R
import org.lumine.foodhub.ui.styles.Orange
import org.lumine.foodhub.ui.styles.Typography
import org.lumine.foodhub.ui.viewmodels.SignInViewModel

@Composable
fun SignIn(
    goBack: () -> Unit,
    goToSignUp: () -> Unit,
    goToHome: () -> Unit
) {
    val context = LocalContext.current
    val viewModel: SignInViewModel = viewModel()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(32.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            BackButton(onClick = { goBack() })
        }
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            AuthenticationTitle(text = R.string.sign_in)
            Spacer(modifier = Modifier.height(32.dp))
            InputField(
                text = R.string.email_address,
                value = viewModel.email,
                onValueChange = { viewModel.email = it },
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            )
            Spacer(modifier = Modifier.height(16.dp))
            PasswordField(
                text = R.string.password,
                value = viewModel.password,
                onValueChange = { viewModel.password = it },
                imeAction = ImeAction.Done
            )
            Spacer(modifier = Modifier.height(48.dp))
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                AskForgotPassword()
                Spacer(modifier = Modifier.height(16.dp))
                AuthenticationButton(
                    text = R.string.sign_in,
                    onClick = { viewModel.signIn(context = context, goToHome = { goToHome() }) })
                Spacer(modifier = Modifier.height(16.dp))
                AskAuthenticate(
                    text1 = R.string.sign_up_ask,
                    color = Color.Black,
                    text2 = R.string.sign_up,
                    onClick = { goToSignUp() }
                )
            }
        }
        Column(modifier = Modifier.fillMaxWidth()) {
            GlowyAuthentication(text = R.string.sign_in_with, color = Color.Black)
        }
    }
}

@Composable
fun AskForgotPassword() {
    Text(
        text = stringResource(id = R.string.forgot_password_ask),
        color = Orange,
        fontSize = Typography.body2.fontSize,
        letterSpacing = Typography.body2.letterSpacing,
        modifier = Modifier.clickable(onClick = { /*TODO*/ })
    )
}