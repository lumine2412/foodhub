package org.lumine.foodhub

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import org.lumine.foodhub.ui.screens.*

sealed class Screen(val route: String) {
    object Onboarding : Screen(route = "onboarding")
    object Welcome : Screen(route = "welcome")
    object SignUp : Screen(route = "sign_up")
    object SignIn : Screen(route = "sign_in")
    object Home : Screen(route = "home")
    object Profile : Screen(route = "profile")
}

@Composable
fun FoodHub() {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = Screen.Onboarding.route
    ) {
        composable(route = Screen.Onboarding.route) {
            Onboarding(
                goToWelcome = { navController.navigate(route = Screen.Welcome.route) }
            )
        }
        composable(route = Screen.Welcome.route) {
            Welcome(
                goSkip = { navController.navigate(route = Screen.Home.route) },
                goToSignUp = { navController.navigate(route = Screen.SignUp.route) },
                goToSignIn = { navController.navigate(route = Screen.SignIn.route) }
            )
        }
        composable(route = Screen.SignUp.route) {
            SignUp(
                goBack = { navController.navigate(route = Screen.Welcome.route) },
                goToSignIn = { navController.navigate(route = Screen.SignIn.route) }
            )
        }
        composable(route = Screen.SignIn.route) {
            SignIn(
                goBack = { navController.navigate(route = Screen.Welcome.route) },
                goToSignUp = { navController.navigate(route = Screen.SignUp.route) },
                goToHome = { navController.navigate(route = Screen.Home.route) }
            )
        }
        composable(route = Screen.Home.route) {
//            HomeRestaurant(
//                goBack = { navController.navigate(route = Screen.Welcome.route) },
//                goToProfile = { navController.navigate(route = Screen.Profile.route) }
//            )
            HomeMovie(
                goBack = { navController.navigate(route = Screen.Welcome.route) },
                goToProfile = { navController.navigate(route = Screen.Profile.route) }
            )
        }
        composable(route = Screen.Profile.route) {
            Profile(
                goBack = { navController.navigate(route = Screen.Home.route) },
                goToWelcome = { navController.navigate(route = Screen.Welcome.route) }
            )
        }
    }
}