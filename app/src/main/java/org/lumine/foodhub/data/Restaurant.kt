package org.lumine.foodhub.data

import androidx.compose.runtime.mutableStateListOf

data class Restaurant(
    val id: Int,
    val name: String,
    val address: String,
    val image: String
)

val restaurantList = mutableStateListOf(
    Restaurant(
        id = 33760,
        name = "Du Miên Garden Cafe - Phan Văn Trị",
        address = "7 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g4/33760/prof/s180x180/foody-mobile-du-mien-garden-mb-jp-309-635785114907746480.jpg"
    ),
    Restaurant(
        id = 978,
        name = "Country House Cafe",
        address = "18C Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g1/978/prof/s180x180/foody-mobile-countryhouse-jpg-246-635683259648435945.jpg"
    ),
    Restaurant(
        id = 82801,
        name = "Hẻm Spaghetti - Nguyễn Oanh",
        address = "212/22 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g9/82801/prof/s180x180/foody-upload-api-foody-mobile-gi4-jpg-181001160804.jpg"
    ),
    Restaurant(
        id = 149154,
        name = "Lava Coffee - Quang Trung",
        address = "61 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g15/149154/prof/s180x180/foody-upload-api-foody-mobile-4-190111100956.jpg"
    ),
    Restaurant(
        id = 194867,
        name = "Mì Cay Naga - 224 Phạm Văn Đồng",
        address = "224 Phạm Văn Đồng, P. 1 ",
        image = "https://images.foody.vn/res/g20/194867/prof/s180x180/foody-mobile-9zbk996o-jpg-112-636143635496125338.jpg"
    ),
    Restaurant(
        id = 25656,
        name = "City House Cafe - Sân Vườn Lãng Mạn",
        address = "21 Huỳnh Khương An, P. 5",
        image = "https://images.foody.vn/res/g3/25656/prof/s180x180/foody-mobile-city-house-jpg-354-636133373358973749.jpg"
    ),
    Restaurant(
        id = 66844,
        name = "Nhi Nhi Quán - Đặc Sản Phan Rang",
        address = "125/48 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g7/66844/prof/s180x180/foody-mobile-bpzy3ni3-jpg-841-636143721820688231.jpg"
    ),
    Restaurant(
        id = 33645,
        name = "Yuri Yuri - Ẩm Thực Hàn Quốc",
        address = "358 Nguyễn Văn Nghi, P. 7",
        image = "https://images.foody.vn/res/g4/33645/prof/s180x180/foody-mobile-bayef6n3-jpg-308-636023635570133301.jpg"
    ),
    Restaurant(
        id = 252442,
        name = "Trà Sữa Gong Cha - 貢茶 - Phan Văn Trị",
        address = "595 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g26/252442/prof/s180x180/foody-mobile-12-jpg-323-636148043123028599.jpg"
    ),
    Restaurant(
        id = 12513,
        name = "Oasis Cafe",
        address = "26/14 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g2/12513/prof/s180x180/foody-mobile-oasis-cafe-tp-hcm.jpg"
    ),
    Restaurant(
        id = 199622,
        name = "Buzza Pizza - Emart Gò Vấp",
        address = "Tầng Trệt Emart Gò Vấp - 366 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g20/199622/prof/s180x180/201652411519-mobile-foody-logo-2-640x400.jpg"
    ),
    Restaurant(
        id = 651063,
        name = "Dallas Cakes & Coffee - Quang Trung",
        address = "6 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g66/651063/prof/s180x180/foody-mobile-11090821_80492442289-251-636277684533992519.jpg"
    ),
    Restaurant(
        id = 60915,
        name = "Hot & Cold - Trà Sữa & Xiên Que - Quang Trung",
        address = "804 Quang Trung",
        image = "https://images.foody.vn/res/g7/60915/prof/s180x180/foody-upload-api-foody-mobile-1-190605180036.jpg"
    ),
    Restaurant(
        id = 112585,
        name = "Papaxốt - Quang Trung",
        address = "458 Quang Trung",
        image = "https://images.foody.vn/res/g12/112585/prof/s180x180/foody-upload-api-foody-mobile-7-190109171633.jpg"
    ),
    Restaurant(
        id = 91979,
        name = "SaiGon Chic Cafe",
        address = "82 Đường Số 27, P. 6",
        image = "https://images.foody.vn/res/g10/91979/prof/s180x180/foody-mobile-kjuxujry-jpg-982-635838930416211667.jpg"
    ),
    Restaurant(
        id = 28883,
        name = "Pizza Hut - Quang Trung",
        address = "283 Quang Trung",
        image = "https://images.foody.vn/res/g3/28883/prof/s180x180/foody-mobile-p-jpg-800-635757703358128351.jpg"
    ),
    Restaurant(
        id = 129725,
        name = "Bánh Mì Chảo & Món Ngon Hoa Việt - Quán Cô 3 Hậu",
        address = "36 Đường Số 18, P. 8",
        image = "https://images.foody.vn/res/g13/129725/prof/s180x180/foody-mobile-foody-quan-co-3-hau--960-635652896252263911.jpg"
    ),
    Restaurant(
        id = 46668,
        name = "Kichi Kichi Lẩu Băng Chuyền - Quang Trung",
        address = "1 Quang Trung",
        image = "https://images.foody.vn/res/g5/46668/prof/s180x180/foody-mobile-rfokfbsk-jpg-859-635796372049634356.jpg"
    ),
    Restaurant(
        id = 158187,
        name = "The Coffee House - Quang Trung",
        address = "293 Quang Trung",
        image = "https://images.foody.vn/res/g16/158187/prof/s180x180/foody-mobile-coffeehousequangtrun-822-635754530645782536.jpg"
    ),
    Restaurant(
        id = 30102,
        name = "Cánh Đồng Quán - Lẩu Nướng Tại Bàn - Dương Quảng Hàm",
        address = "310/14 Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g4/30102/prof/s180x180/foody-mobile-shwadjuj-jpg-413-636204369735412311.jpg"
    ),
    Restaurant(
        id = 83857,
        name = "Chú Tèo Buffet Nướng 99K - K26 Dương Quảng Hàm",
        address = "N01 Chung Cư K26 Dương Quảng Hàm, P. 7",
        image = "https://images.foody.vn/res/g9/83857/prof/s180x180/201722393753-avatar1.jpg"
    ),
    Restaurant(
        id = 212102,
        name = "Food Court - E Mart Gò Vấp",
        address = "Tầng Trệt E - Mart Gò Vấp, 168 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g22/212102/prof/s180x180/foody-mobile-bqzipmry-jpg-492-636107585581158265.jpg"
    ),
    Restaurant(
        id = 67764,
        name = "Bánh Tráng Deli & Xiên Que - Lê Đức Thọ",
        address = "730/7/5 Lê Đức Thọ, P. 15",
        image = "https://images.foody.vn/res/g7/67764/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 232015,
        name = "Cowboy Jack's - American Dining - Lotte Mart Gò Vấp",
        address = "Tầng 1, Lotte Mart Gò Vấp, 242 Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g24/232015/prof/s180x180/foody-mobile-untitled-1-jpg-931-636143900507885296.jpg"
    ),
    Restaurant(
        id = 674432,
        name = "Food House - Nguyễn Kiệm",
        address = "800 Nguyễn Kiệm",
        image = "https://images.foody.vn/res/g68/674432/prof/s180x180/foody-upload-api-foody-mobile-333-190612180628.jpg"
    ),
    Restaurant(
        id = 106797,
        name = "Domino's Pizza - Quang Trung",
        address = "582 Quang Trung , P. 10",
        image = "https://images.foody.vn/res/g11/106797/prof/s180x180/foody-mobile-r1-jpg-557-635751482461830639.jpg"
    ),
    Restaurant(
        id = 147114,
        name = "Hana BBQ & Hot Pot Buffet - Phan Văn Trị",
        address = "705 - 707 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g15/147114/prof/s180x180/foody-mobile-14218097465_77c6d3a9-840-635745609441630967.jpg"
    ),
    Restaurant(
        id = 231730,
        name = "Lẩu Cua Đất Mũi - Quang Trung",
        address = "769 Quang Trung, P. 12",
        image = "https://images.foody.vn/res/g24/231730/prof/s180x180/foody-mobile-12-jpg-878-636169633493640640.jpg"
    ),
    Restaurant(
        id = 135862,
        name = "Trà Sữa Hiberry - Nguyễn Văn Bảo",
        address = "7 Nguyễn Văn Bảo, P. 4",
        image = "https://images.foody.vn/res/g14/135862/prof/s180x180/foody-mobile-7-jpg.jpg"
    ),
    Restaurant(
        id = 78167,
        name = "Trà Sữa Bobapop - Quang Trung",
        address = "343 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g8/78167/prof/s180x180/foody-mobile-x1-jpg-224-635744521403523929.jpg"
    ),
    Restaurant(
        id = 5023,
        name = "I Love Kem - Thống Nhất",
        address = "257 Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g1/5023/prof/s180x180/foody-mobile-mobile-jpg-221-635744777525709784.jpg"
    ),
    Restaurant(
        id = 236020,
        name = "OH-LA DELI - Quán Ăn Hàn Quốc",
        address = "72/14A Đường Số 30, P. 6",
        image = "https://images.foody.vn/res/g24/236020/prof/s180x180/foody-mobile-oh-la-jpg-315-636286406947435771.jpg"
    ),
    Restaurant(
        id = 166732,
        name = "Texas Chicken - Quang Trung",
        address = "578 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g17/166732/prof/s180x180/foody-upload-api-foody-mobile-92a-jpg-180831085121.jpg"
    ),
    Restaurant(
        id = 43019,
        name = "Sumo BBQ - Quang Trung - Buffet Nướng & Lẩu",
        address = "50 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g5/43019/prof/s180x180/foody-mobile-sumo-bbq-jpg-676-636052105888858762.jpg"
    ),
    Restaurant(
        id = 4146,
        name = "Gà Rán KFC - BigC Gò Vấp",
        address = "792 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g1/4146/prof/s180x180/foody-upload-api-foody-mobile-jhjhjh-190612135957.jpg"
    ),
    Restaurant(
        id = 165239,
        name = "King BBQ Buffet Vincom Quang Trung",
        address = "B1-11 Vincom Quang Trung, 190 Quang Trung",
        image = "https://images.foody.vn/res/g17/165239/prof/s180x180/foody-mobile-w5omrta7-jpg-878-635781680073858982.jpg"
    ),
    Restaurant(
        id = 197985,
        name = "Gogi House - Thịt Nướng Hàn Quốc - E-Mart Gò Vấp",
        address = "Tầng 1 E Mart Gò Vấp, 168 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g20/197985/prof/s180x180/foody-mobile-t1-jpg-927-635866380239214232.jpg"
    ),
    Restaurant(
        id = 8266,
        name = "Nhà Hàng Những Người Bạn",
        address = "14 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g1/8266/prof/s180x180/foody-mobile-dscn1936-jpg-663-635767009603516450.jpg"
    ),
    Restaurant(
        id = 289632,
        name = "Bún Mắm Hai Lúa",
        address = "401 Cây Trâm",
        image = "https://images.foody.vn/res/g29/289632/prof/s180x180/foody-upload-api-foody-mobile-112-jpg-181119145532.jpg"
    ),
    Restaurant(
        id = 245681,
        name = "Gà Nướng Ò Ó O - Quang Trung",
        address = "50/25 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g25/245681/prof/s180x180/foody-upload-api-foody-mobile-6-190527111425.jpg"
    ),
    Restaurant(
        id = 114471,
        name = "Kem Baskin Robbins - Quang Trung",
        address = "82 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g12/114471/prof/s180x180/foody-mobile-foody-mobile-f3-jpg--439-636014088064299302.jpg"
    ),
    Restaurant(
        id = 3629,
        name = "Xôi Chè Bùi Thị Xuân - Nguyễn Oanh",
        address = "144 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g1/3629/prof/s180x180/foody-mobile-woz31fnb-jpg-577-635975441516227121.jpg"
    ),
    Restaurant(
        id = 201198,
        name = "Starbucks Coffee - Emart",
        address = "Emart, 366 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g21/201198/prof/s180x180/foody-upload-api-foody-mobile-4-190125163158.jpg"
    ),
    Restaurant(
        id = 73213,
        name = "Thanh Thúy - Ẩm Thực Miền Trung",
        address = "679 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g8/73213/prof/s180x180/foody-upload-api-foody-mobile-foody-mobile-rqofoik-180623113654.jpg"
    ),
    Restaurant(
        id = 628238,
        name = "Nem Nướng & Bún Thịt Nướng Mr. Nem - Cây Trâm",
        address = "400 Cây Trâm. P. 8",
        image = "https://images.foody.vn/res/g63/628238/prof/s180x180/foody-mobile-nem-nuong-phan-jpg-957-636386648154212630.jpg"
    ),
    Restaurant(
        id = 673904,
        name = "Đông Mập - Trà Sữa & Gà Cay Phô Mai",
        address = "164/5 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g68/673904/prof/s180x180/foody-mobile-10-jpg-815-636380593760820863.jpg"
    ),
    Restaurant(
        id = 70256,
        name = "Bánh Mì Cóc - Bánh Mì Gà Xé - 112A Nguyễn Thái Sơn",
        address = "112A Nguyễn Thái Sơn, P. 3",
        image = "https://images.foody.vn/res/g8/70256/prof/s180x180/foody-mobile-banh-mi-coc-mb-jpg-372-635745577757039316.jpg"
    ),
    Restaurant(
        id = 209038,
        name = "Sweet Sushi - Sushi & More",
        address = "61 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g21/209038/prof/s180x180/foody-mobile-222-jpg-245-636053250238066686.jpg"
    ),
    Restaurant(
        id = 145745,
        name = "Chip Chip - Trà Sữa Thạch Phô Mai - Lê Đức Thọ",
        address = "40/20 Lê Đức Thọ, P. 7",
        image = "https://images.foody.vn/res/g15/145745/prof/s180x180/foody-mobile-chip-chip-mb-jpg-979-635704021861263314.jpg"
    ),
    Restaurant(
        id = 256733,
        name = "Thức Coffee - Nguyễn Thái Sơn",
        address = "320 Nguyễn Thái Sơn",
        image = "https://images.foody.vn/res/g26/256733/prof/s180x180/foody-upload-api-foody-mobile-5-jpg-180920135233.jpg"
    ),
    Restaurant(
        id = 69227,
        name = "Hồng Diên - Bánh Căn & Bánh Xèo",
        address = "269 Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g7/69227/prof/s180x180/foody-mobile-3uvhax97-jpg-524-636192080414789363.jpg"
    ),
    Restaurant(
        id = 128303,
        name = "Trà Sữa Milk Tea Land - Lê Đức Thọ",
        address = "650 Lê Đức Thọ, P. 15",
        image = "https://images.foody.vn/res/g13/128303/prof/s180x180/foody-mobile-c2-jpg-564-636165296410116011.jpg"
    ),
    Restaurant(
        id = 766016,
        name = "Làng Vòng - Bún Đậu Mắm Tôm & Bún Chả",
        address = "413 Phan Huy Ích, P. 14",
        image = "https://images.foody.vn/res/g77/766016/prof/s180x180/foody-upload-api-foody-mobile-19a-jpg-180820115422.jpg"
    ),
    Restaurant(
        id = 147845,
        name = "Trà Sữa Tiên Hưởng - Phan Văn Trị",
        address = "551 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g15/147845/prof/s180x180/foody-mobile-tra-tien-huong-mb-jp-442-635751684786854004.jpg"
    ),
    Restaurant(
        id = 10547,
        name = "Quán Bò Hai Châu - Cây Trâm",
        address = "268 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g2/10547/prof/s180x180/foody-mobile-szc05syu-jpg-111-635826776703640309.jpg"
    ),
    Restaurant(
        id = 124798,
        name = "Cút Vịt Lộn 10 Món",
        address = "775 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g13/124798/prof/s180x180/foody-mobile-mobile-jpg-885-636155123685587115.jpg"
    ),
    Restaurant(
        id = 753428,
        name = "TL Sushi",
        address = "34 Đường Số 45, P. 14",
        image = "https://images.foody.vn/res/g76/753428/prof/s180x180/foody-upload-api-foody-mobile-172542_mentahcover-j-180622162050.jpg"
    ),
    Restaurant(
        id = 120212,
        name = "McDonald's - Quang Trung",
        address = "1 Quang Trung, P. 3",
        image = "https://images.foody.vn/res/g13/120212/prof/s180x180/foody-upload-api-foody-mobile-asdasdasd-jpg-181227145606.jpg"
    ),
    Restaurant(
        id = 21131,
        name = "Ốc Chánh",
        address = " 55/7 Nguyễn Văn Công, P. 3",
        image = "https://images.foody.vn/res/g3/21131/prof/s180x180/foody-mobile-oc-chanh-mb-jpg-375-635786140342163561.jpg"
    ),
    Restaurant(
        id = 125133,
        name = "Phá Lấu Cô Ba Vân",
        address = "288 Thống Nhất, P. 10",
        image = "https://images.foody.vn/res/g13/125133/prof/s180x180/foody-mobile-pha-lau-ba-van-jpg-707-636053162094153698.jpg"
    ),
    Restaurant(
        id = 35772,
        name = "Lotteria - Quang Trung",
        address = "9 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g4/35772/prof/s180x180/foody-mobile-1-jpg-580-635769714379167137.jpg"
    ),
    Restaurant(
        id = 112595,
        name = "Food Court - AEON Citimart Quang Trung",
        address = "672 Quang Trung",
        image = "https://images.foody.vn/res/g12/112595/prof/s180x180/foody-mobile-hmb-an-jpg-128-635563929688153330.jpg"
    ),
    Restaurant(
        id = 49791,
        name = "Chất BBQ - Xiên Nướng Đồng Giá",
        address = "192/2 Nguyễn Oanh",
        image = "https://images.foody.vn/res/g5/49791/prof/s180x180/foody-mobile-isw3j7u9-jpg-745-635822453669412048.jpg"
    ),
    Restaurant(
        id = 166259,
        name = "Sườn Cây Nướng & Beer - Quang Trung",
        address = "474 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g17/166259/prof/s180x180/foody-mobile-hmb-suon-jpg-253-635773051671214020.jpg"
    ),
    Restaurant(
        id = 139759,
        name = "Trà Sữa Tiên Hưởng - Quang Trung",
        address = "69 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g14/139759/prof/s180x180/201732311120-banner-1-.jpg"
    ),
    Restaurant(
        id = 732364,
        name = "Busan Korean Food - Phan Văn Trị",
        address = "763 - 765 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g74/732364/prof/s180x180/foody-upload-api-foody-mobile-hmbbd-jpg-180516154050.jpg"
    ),
    Restaurant(
        id = 9238,
        name = "Địa Đàng Cafe ",
        address = "389 Cây Trâm,  P. 8",
        image = "https://images.foody.vn/res/g1/9238/prof/s180x180/foody-mobile-dia-dang-cafe-tp-hcm.jpg"
    ),
    Restaurant(
        id = 769154,
        name = "Koi Sushi",
        address = "549 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g77/769154/prof/s180x180/foody-upload-api-foody-mobile-3-jpg-181030094001.jpg"
    ),
    Restaurant(
        id = 87381,
        name = "Quán Nướng Ngói Sài Gòn",
        address = "232 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g9/87381/prof/s180x180/foody-mobile-q1-jpg-336-635772345109157009.jpg"
    ),
    Restaurant(
        id = 4861,
        name = "Bò Né Hai Chị Em - Quang Trung",
        address = "460 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g1/4861/prof/s180x180/foody-mobile-bo-ne-hai-chi-em-quang-trung.jpg"
    ),
    Restaurant(
        id = 146525,
        name = "Pizza Đà Lạt - Lê Lợi",
        address = "13 Lê Lợi",
        image = "https://images.foody.vn/res/g15/146525/prof/s180x180/foody-mobile--7-_hinhmob-jpg-306-635792089988388808.jpg"
    ),
    Restaurant(
        id = 200593,
        name = "Bún Đậu Hoàng Anh",
        address = "40/78 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g21/200593/prof/s180x180/foody-upload-api-foody-mobile-28-jpg-181222172840.jpg"
    ),
    Restaurant(
        id = 165175,
        name = "Tasaki BBQ Vincom Quang Trung",
        address = "Tầng B1, Vincom Quang Trung, 190 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g17/165175/prof/s180x180/foody-mobile-6qcw7awh-jpg-330-636159380531732219.jpg"
    ),
    Restaurant(
        id = 103121,
        name = "Phố Nướng Anh Em - Dương Quảng Hàm",
        address = "258 Dương Quảng Hàm",
        image = "https://images.foody.vn/res/g11/103121/prof/s180x180/foody-mobile-mobile-jpg-743-635785129944300891.jpg"
    ),
    Restaurant(
        id = 83883,
        name = "Trà Sữa Tam Cốc",
        address = "543A/3 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g9/83883/prof/s180x180/foody-mobile-mobile-jpg-480-635744811765741924.jpg"
    ),
    Restaurant(
        id = 91344,
        name = "Lâm Bô Hotpot & Barbecue - Quang Trung",
        address = "1 Quang Trung, P. 3",
        image = "https://images.foody.vn/res/g10/91344/prof/s180x180/foody-mobile-lam-bo-lau-jpg-334-635788952968669020.jpg"
    ),
    Restaurant(
        id = 209045,
        name = "Gyu-Kaku Japanese BBQ - Vincom Plaza Gò Vấp",
        address = "Tầng 4, Vincom Plaza Gò Vấp, 12 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g21/209045/prof/s180x180/foody-upload-api-foody-mobile-6-jpg-181121175232.jpg"
    ),
    Restaurant(
        id = 292105,
        name = "Bún Cay Thái 2 Thuận - Nguyễn Văn Khối (Cây Trâm)",
        address = "320 Nguyễn Văn Khối (Cây Trâm), P. 9",
        image = "https://images.foody.vn/res/g30/292105/prof/s180x180/foody-upload-api-foody-mobile-img_20180814_153632--180814154313.jpg"
    ),
    Restaurant(
        id = 44618,
        name = "ABC Bakery - Quang Trung",
        address = "34 - 36 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g5/44618/prof/s180x180/foody-mobile-thkt79ik-jpg-452-635799125407730346.jpg"
    ),
    Restaurant(
        id = 777657,
        name = "Chattea - Trà Chất - Phan Văn Trị",
        address = "624 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g78/777657/prof/s180x180/foody-upload-api-foody-mobile-chat-jpg-181016144145.jpg"
    ),
    Restaurant(
        id = 3692,
        name = "Bún Bò Sông Hương - Quang Trung",
        address = "740 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g1/3692/prof/s180x180/foody-mobile-f9up7v8u-jpg-132-635863825327439838.jpg"
    ),
    Restaurant(
        id = 3711,
        name = "Mộc Miên Cafe ",
        address = "830 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g1/3711/prof/s180x180/foody-mobile-hmb-mm-jpg-244-635775825366151184.jpg"
    ),
    Restaurant(
        id = 10531,
        name = "Lẩu Ếch Sáu Hiếu",
        address = "429/9 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g2/10531/prof/s180x180/foody-mobile-9sy86ckq-jpg-931-636153242594546679.jpg"
    ),
    Restaurant(
        id = 165167,
        name = "Daruma - Quán Ăn Nhật Bản - Vincom Quang Trung",
        address = "Tầng B1 Vincom Quang Trung, 190 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g17/165167/prof/s180x180/foody-mobile-slm89vz5-jpg-887-635785101847559541.jpg"
    ),
    Restaurant(
        id = 79097,
        name = "Steak Bin & Pizza - Đường Số 8",
        address = "85 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g8/79097/prof/s180x180/foody-mobile-o69p7z1b-jpg-740-636147367210853098.jpg"
    ),
    Restaurant(
        id = 159575,
        name = "MNS ICE SNOW - Đá Bào Mật Trái Cây Rừng - Quang Trung",
        address = "69D Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g16/159575/prof/s180x180/foody-mobile-da-bao-mat-tria-cay--133-635859613811960598.jpg"
    ),
    Restaurant(
        id = 8494,
        name = "Quán Ăn Hàn Quốc - Quang Trung",
        address = "445 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g1/8494/prof/s180x180/foody-mobile-aytrlsm2-jpg-645-636191392528827373.jpg"
    ),
    Restaurant(
        id = 242782,
        name = "Metropolis Coffee",
        address = "1230 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g25/242782/prof/s180x180/foody-mobile-13307352_19361662770-227-636009901325882486.jpg"
    ),
    Restaurant(
        id = 244259,
        name = "Bún Thịt Nướng Kiều Bảo 2",
        address = "850 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g25/244259/prof/s180x180/foody-mobile-foody-quan-thien-bao-292-636011457479912150.jpg"
    ),
    Restaurant(
        id = 14923,
        name = "Ốc Vui Vẻ",
        address = "153/48/38 Lê Văn Thọ, P. 8",
        image = "https://images.foody.vn/res/g2/14923/prof/s180x180/foody-mobile-sdrzfub9-jpg-875-635854237813426054.jpg"
    ),
    Restaurant(
        id = 233233,
        name = "Lavida Coffee And Tea - Quang Trung",
        address = "661 Quang Trung",
        image = "https://images.foody.vn/res/g24/233233/prof/s180x180/foody-upload-api-foody-mobile-57-jpg-181116112840.jpg"
    ),
    Restaurant(
        id = 127789,
        name = "Xiên Nướng KingKong",
        address = "19 Phạm Ngũ Lão, P. 3",
        image = "https://images.foody.vn/res/g13/127789/prof/s180x180/foody-mobile-xien-4-mb-jpg-564-635732865372795364.jpg"
    ),
    Restaurant(
        id = 152670,
        name = "Megustas Coffee & More",
        address = "301 Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g16/152670/prof/s180x180/foody-upload-api-foody-mobile-9-jpg-181220154729.jpg"
    ),
    Restaurant(
        id = 25677,
        name = "Trà Sữa & Thức Ăn Nhanh Tombo - Thống Nhất",
        address = "447 Thống Nhất",
        image = "https://images.foody.vn/res/g3/25677/prof/s180x180/foody-mobile-mobile-jpg-831-635775913901142688.jpg"
    ),
    Restaurant(
        id = 206130,
        name = "NewDays Japanese Matcha Cafe - Vincom Plaza Gò Vấp",
        address = "Tầng 4, Vincom Plaza Gò Vấp, 12 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g21/206130/prof/s180x180/foody-mobile-hinh-jpg-570-636331324996326460.jpg"
    ),
    Restaurant(
        id = 195094,
        name = "Sushi Đường Phố - Sake Kissho - Lê Văn Thọ",
        address = "368 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g20/195094/prof/s180x180/foody-upload-api-foody-mobile-13-jpg-180807113052.jpg"
    ),
    Restaurant(
        id = 259142,
        name = "Chè Thái Ý Phương - Quang Trung",
        address = "663 Quang Trung",
        image = "https://images.foody.vn/res/g26/259142/prof/s180x180/foody-mobile-untitled-1-jpg-390-636047855512241877.jpg"
    ),
    Restaurant(
        id = 228809,
        name = "Bánh Bông Lan Trứng Muối Sunny Bakery - Shop Online",
        address = "1244/40/2B Lê Đức Thọ, P. 13",
        image = "https://images.foody.vn/res/g23/228809/prof/s180x180/foody-upload-api-foody-mobile-kjkj-190610085353.jpg"
    ),
    Restaurant(
        id = 17010,
        name = "Bún Mắm Cô Ba - Phan Huy Ích",
        address = "451 Phan Huy Ích, P. 14 ",
        image = "https://images.foody.vn/res/g2/17010/prof/s180x180/foody-mobile-bun-mam-co-ba-phan-huy-ich-tp-hcm.jpg"
    ),
    Restaurant(
        id = 211125,
        name = "Hotpot Story - Vincom Plaza Gò Vấp",
        address = "L4 - 07, Vincom Plaza Gò Vấp, 12 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g22/211125/prof/s180x180/foody-mobile-12717832_12092423557-275-635912275431173744.jpg"
    ),
    Restaurant(
        id = 107543,
        name = "Dunkin' Donuts - Quang Trung",
        address = "Tầng Trệt, 304A Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g11/107543/prof/s180x180/foody-upload-api-foody-mobile-9-jpg-180912151434.jpg"
    ),
    Restaurant(
        id = 739277,
        name = "The Alley - Trà Sữa Đài Loan - Quang Trung",
        address = "56 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g74/739277/prof/s180x180/foody-upload-api-foody-mobile-16-190617174527.jpg"
    ),
    Restaurant(
        id = 731577,
        name = "Toocha - Trà Sữa Không Mập - Phan Văn Trị",
        address = "366A23 Phan Văn Trị",
        image = "https://images.foody.vn/res/g74/731577/prof/s180x180/foody-upload-api-foody-mobile-8-jpg-180913175404.jpg"
    ),
    Restaurant(
        id = 273430,
        name = "Lẩu Gà Ớt Hiểm 109 - Phạm Ngũ Lão",
        address = "19 Phạm Ngũ Lão, P. 3",
        image = "https://images.foody.vn/res/g28/273430/prof/s180x180/foody-upload-api-foody-mobile-3-jpg-181227111956.jpg"
    ),
    Restaurant(
        id = 98714,
        name = "Bò Cười Quán",
        address = "39 Lê Đức Thọ",
        image = "https://images.foody.vn/res/g10/98714/prof/s180x180/foody-mobile-y41g20zd-jpg-815-635796361863577913.jpg"
    ),
    Restaurant(
        id = 710953,
        name = "Đen Đá Cafe - Trà Sữa & Trà Đào - Phan Văn Trị",
        address = "366 Phan Văn Trị",
        image = "https://images.foody.vn/res/g72/710953/prof/s180x180/foody-mobile-vddvdv-jpg.jpg"
    ),
    Restaurant(
        id = 736453,
        name = "Con Gà Mái - Cơm Gà Phú Yên - Quang Trung",
        address = "181 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g74/736453/prof/s180x180/foody-upload-api-foody-mobile-hmbb-jpg-180426114739.jpg"
    ),
    Restaurant(
        id = 718524,
        name = "Trà Sữa Heekcaa Việt Nam - Phan Văn Trị",
        address = "15K (Hẻm 424) Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g72/718524/prof/s180x180/foody-upload-api-foody-mobile-12-190111140737.jpg"
    ),
    Restaurant(
        id = 28874,
        name = "Beefsteak Hoàng Gia Quý Tộc",
        address = "77 - 79 Đường Số 11",
        image = "https://images.foody.vn/res/g3/28874/prof/s180x180/foody-mobile-1-jpg-125-636094398745247668.jpg"
    ),
    Restaurant(
        id = 95695,
        name = "Ăn Vặt Kem Gà - Giao Hàng Tận Nơi",
        address = "80/5 Nguyễn Văn Công, P. 3",
        image = "https://images.foody.vn/res/g10/95695/prof/s180x180/foody-mobile-61nzdirz-jpg-331-635839711786484071.jpg"
    ),
    Restaurant(
        id = 40839,
        name = "Món Huế O Hiệp - Shop Online",
        address = "44/9 Đường Số 45",
        image = "https://images.foody.vn/res/g5/40839/prof/s180x180/foody-mobile-mon-hue-o-hiep-cong-hoa-tp-hcm-131019093955.jpg"
    ),
    Restaurant(
        id = 4592,
        name = "The Adora - Tiệc Cưới Hội Nghị - Nguyễn Kiệm",
        address = "371 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g1/4592/prof/s180x180/foody-mobile-adora-mb-jpg-734-635743019721174364.jpg"
    ),
    Restaurant(
        id = 23005,
        name = "Phá Lấu Bò - Cây Trâm",
        address = "208 Cây Trâm",
        image = "https://images.foody.vn/res/g3/23005/prof/s180x180/foody-mobile-foody-pha-lau-bo-cay-994-635785382670360780.jpg"
    ),
    Restaurant(
        id = 103426,
        name = "Bánh Flan & Bánh Bèo - Chợ Hạnh Thông Tây",
        address = "Chợ Hạnh Thông Tây",
        image = "https://images.foody.vn/res/g11/103426/prof/s180x180/foody-mobile-jfowldjt-jpg-888-635806085210964039.jpg"
    ),
    Restaurant(
        id = 77277,
        name = "Bánh Flan Tây Ban Nha",
        address = "119 Đường Số 1, P. 11",
        image = "https://images.foody.vn/res/g8/77277/prof/s180x180/foody-mobile-df97n2zr-jpg-196-635806094398132175.jpg"
    ),
    Restaurant(
        id = 733764,
        name = "Koi Thé Café - Phan Văn Trị",
        address = "L6.2 Khu Dân Cư City Land, Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g74/733764/prof/s180x180/foody-upload-api-foody-mobile-ghgh-jpg-180417135854.jpg"
    ),
    Restaurant(
        id = 688453,
        name = "Royaltea Việt Nam - Trà Sữa Hồng Kông - Phan Văn Trị",
        address = "366A18 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g69/688453/prof/s180x180/foody-mobile-foody-mobile-1801408-139-636409943259267299.jpg"
    ),
    Restaurant(
        id = 102741,
        name = "Lộ Thiên Quán - Nguyễn Oanh",
        address = "222 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g11/102741/prof/s180x180/foody-mobile-1-jpg-950-636416001692016668.jpg"
    ),
    Restaurant(
        id = 683740,
        name = "Family Garden Cafe",
        address = "438 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g69/683740/prof/s180x180/foody-mobile-9-jpg.jpg"
    ),
    Restaurant(
        id = 649168,
        name = "Bách Restaurant - Ẩm Thực Trung Hoa",
        address = "481 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g65/649168/prof/s180x180/foody-mobile-brif1qsf-jpg-376-636287086675236508.jpg"
    ),
    Restaurant(
        id = 286930,
        name = "The Hideout Coffee & Desserts",
        address = "190 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g29/286930/prof/s180x180/foody-mobile-14641951_18931645484-213-636128989126335378.jpg"
    ),
    Restaurant(
        id = 704599,
        name = "Royaltea Việt Nam - 108 Quang Trung",
        address = "108 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g71/704599/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 31538,
        name = "Lẩu Đồng Quê 3 - Đường 13",
        address = "57/2 Đường 13, P. 11",
        image = "https://images.foody.vn/res/g4/31538/prof/s180x180/foody-mobile-mobile-jpg-344-635525127586486563.jpg"
    ),
    Restaurant(
        id = 713996,
        name = "TocoToco Bubble Tea - Phan Văn Trị",
        address = "449 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g72/713996/prof/s180x180/foody-mobile-toco-jpg.jpg"
    ),
    Restaurant(
        id = 235771,
        name = "Highlands Coffee - Lotte Gò Vấp",
        address = "Lotte Mart Gò Vấp, 242 Nguyễn Văn Lượng",
        image = "https://images.foody.vn/res/g24/235771/prof/s180x180/foody-mobile-hl-jpg-365-635990067692319357.jpg"
    ),
    Restaurant(
        id = 126289,
        name = "Cremista - Kem Cuộn Thái Lan",
        address = "621 Lê Đức Thọ,P. 16",
        image = "https://images.foody.vn/res/g13/126289/prof/s180x180/foody-mobile-traxanh-jpg-550-635642719542038211.jpg"
    ),
    Restaurant(
        id = 100543,
        name = "Bánh Bèo Chén",
        address = "107/39 Đường Số 11, P. 11",
        image = "https://images.foody.vn/res/g11/100543/prof/s180x180/foody-mobile-11-2-jpg-967-635512150404715455.jpg"
    ),
    Restaurant(
        id = 890440,
        name = "Ù Quay - Chân Gà Sả Tắc - Lê Thị Hồng - Shop Online",
        address = "28/25 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g90/890440/prof/s180x180/foody-upload-api-foody-mobile-hmn-190227150230.jpg"
    ),
    Restaurant(
        id = 255841,
        name = "Cháo Sườn Cô Giang - Nguyễn Oanh",
        address = "164 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g26/255841/prof/s180x180/foody-mobile-img_8465-jpg.jpg"
    ),
    Restaurant(
        id = 128039,
        name = "Jollibee - Coopmart Quang Trung",
        address = "Tầng 2, Coopmart Quang Trung, 304A Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g13/128039/prof/s180x180/foody-mobile-5879-jpg-855-636341506736349148.jpg"
    ),
    Restaurant(
        id = 658947,
        name = "The Pizza Company - Quang Trung Gò Vấp",
        address = "638 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g66/658947/prof/s180x180/foody-upload-api-foody-mobile-seafood-cocktail-jpg-180823134453.jpg"
    ),
    Restaurant(
        id = 644658,
        name = "Food In Box - Fastfood",
        address = "285B Phạm Văn Chiêu, P. 14",
        image = "https://images.foody.vn/res/g65/644658/prof/s180x180/foody-upload-api-foody-mobile-book-190401105028.jpg"
    ),
    Restaurant(
        id = 10685,
        name = "Pizza Inn - Quang Trung",
        address = "229 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g2/10685/prof/s180x180/foody-mobile-pizza-inn-quang-trung-tp-hcm.jpg"
    ),
    Restaurant(
        id = 629960,
        name = "Effoc Cafe - Quang Trung",
        address = "777 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g63/629960/prof/s180x180/foody-mobile-hmdp-jpg-161-636209385316495728.jpg"
    ),
    Restaurant(
        id = 73933,
        name = "Sushi Street - Sushi Đường Phố",
        address = "913 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g8/73933/prof/s180x180/foody-mobile-sushi-street-sushi-duong-pho-tp-hcm-140612012958.jpg"
    ),
    Restaurant(
        id = 652500,
        name = "Sky Heaven Coffee",
        address = "41/29 Nguyễn Oanh",
        image = "https://images.foody.vn/res/g66/652500/prof/s180x180/foody-mobile-foody-img_9997-63631-839-636313214451660240.jpg"
    ),
    Restaurant(
        id = 631756,
        name = "Ajikoi - Ice Cream & Matcha - Phạm Văn Đồng",
        address = "286 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g64/631756/prof/s180x180/foody-mobile-c2-jpg-238-636219678790824610.jpg"
    ),
    Restaurant(
        id = 73628,
        name = "Xiên Nướng Pé Mập",
        address = "256/6 Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g8/73628/prof/s180x180/foody-mobile-xcahk2ui-jpg-735-635842214255503835.jpg"
    ),
    Restaurant(
        id = 232280,
        name = "Susalan - Coffee & Food House",
        address = "590/2 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g24/232280/prof/s180x180/foody-mobile-c2-jpg-909-635979732383383949.jpg"
    ),
    Restaurant(
        id = 69359,
        name = "Bánh Tráng Nướng Gà Xé",
        address = "2E Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g7/69359/prof/s180x180/foody-mobile-banh-trang-nuong-ga-xe-tp-hcm-140410094517.jpg"
    ),
    Restaurant(
        id = 130206,
        name = "Kem Xôi Dừa - Nguyễn Văn Nghi",
        address = "290 Nguyễn Văn Nghi, P. 7",
        image = "https://images.foody.vn/res/g14/130206/prof/s180x180/foody-mobile-lau-kem-jpg-921-635789859062720490.jpg"
    ),
    Restaurant(
        id = 130164,
        name = "Bảo Lộc - Nướng Ngói - Phạm Văn Đồng",
        address = "160 Phạm Văn Đồng ",
        image = "https://images.foody.vn/res/g14/130164/prof/s180x180/foody-mobile-foody-mobile-cuavdzo-803-636154110088818386.jpg"
    ),
    Restaurant(
        id = 663059,
        name = "Naga - Mì Gà Quay Da Giòn",
        address = "C22 Phan Văn Trị",
        image = "https://images.foody.vn/res/g67/663059/prof/s180x180/foody-mobile-dfgdf-jpg-577-636317506569839267.jpg"
    ),
    Restaurant(
        id = 124745,
        name = "Han Flan - Trà Sữa & Chè Khúc Bạch - Lê Văn Thọ",
        address = "336 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g13/124745/prof/s180x180/foody-upload-api-foody-mobile-2-jpg-181220164547.jpg"
    ),
    Restaurant(
        id = 314815,
        name = "Gà Rán Popeyes - Lê Đức Thọ",
        address = "121 Lê Đức Thọ",
        image = "https://images.foody.vn/res/g32/314815/prof/s180x180/foody-mobile-2-jpg-728-636181039022118008.jpg"
    ),
    Restaurant(
        id = 20143,
        name = "Súp Cua Hạnh - Nguyễn Thái Sơn",
        address = "12 Nguyễn Thái Sơn, P. 3",
        image = "https://images.foody.vn/res/g3/20143/prof/s180x180/foody-mobile-sup-cua-hanh-jpg-876-635859937524385169.jpg"
    ),
    Restaurant(
        id = 2372,
        name = "2 Tòn Quán - Món Ngon Phan Rang",
        address = "570 Lê Quang Định, P. 1",
        image = "https://images.foody.vn/res/g1/2372/prof/s180x180/foody-mobile-vx5z6uoa-jpg-320-635841450237127899.jpg"
    ),
    Restaurant(
        id = 144524,
        name = "Bánh Cuốn Tây Sơn - Phạm Văn Đồng",
        address = "132 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g15/144524/prof/s180x180/foody-mobile-banh-cuon-jpg-288-636041012869372708.jpg"
    ),
    Restaurant(
        id = 703949,
        name = "Bún Bò Huế Xưa - Phạm Văn Đồng",
        address = "309 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g71/703949/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 708034,
        name = "Tân Hải Vân - Ẩm Thực Trung Hoa - Phan Văn Trị",
        address = "503 Phan Văn Trị",
        image = "https://images.foody.vn/res/g71/708034/prof/s180x180/foody-upload-api-foody-mobile-4-jpg-181115165317.jpg"
    ),
    Restaurant(
        id = 104821,
        name = "Racing Beer Club - RBC",
        address = "1 Quang Trung",
        image = "https://images.foody.vn/res/g11/104821/prof/s180x180/foody-mobile-hmb-racing-beer-club-803-635532984810835616.jpg"
    ),
    Restaurant(
        id = 316270,
        name = "SaSin - Mì Cay 7 Cấp Độ Hàn Quốc - Lê Văn Thọ",
        address = "351 Lê Văn Thọ, P. 9",
        image = "https://images.foody.vn/res/g32/316270/prof/s180x180/foody-mobile-12-jpg-998-636197403211083473.jpg"
    ),
    Restaurant(
        id = 658101,
        name = "Gành Ốc Quán",
        address = "68 Đường Số 1, P. 16",
        image = "https://images.foody.vn/res/g66/658101/prof/s180x180/foody-mobile-fghfghfghgfh-jpg-845-636300106914447364.jpg"
    ),
    Restaurant(
        id = 739290,
        name = "Bánh Bột Lọc Cô Bê",
        address = "327/28/9 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g74/739290/prof/s180x180/foody-upload-api-foody-mobile-5-jpg-180702160635.jpg"
    ),
    Restaurant(
        id = 82871,
        name = "Ốc Hoàng - Lê Thị Hồng",
        address = "40/40 Lê Thị Hồng",
        image = "https://images.foody.vn/res/g9/82871/prof/s180x180/foody-mobile-oc-hoang-le-thi-hong-tp-hcm-140616112411.jpg"
    ),
    Restaurant(
        id = 29344,
        name = "Bánh Canh Cá Lóc Cường Đô La - Lê Thị Hồng",
        address = "Đối Diện 42 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g3/29344/prof/s180x180/foody-mobile-4g5nemho-jpg-192-636128967083399739.jpg"
    ),
    Restaurant(
        id = 769777,
        name = "Royaltea Center Hills - Gò Vấp",
        address = "71 Đường Số 7, KDC Cityland Center Hills, P. 7",
        image = "https://images.foody.vn/res/g77/769777/prof/s180x180/foody-upload-api-foody-mobile-17a-jpg-180820103147.jpg"
    ),
    Restaurant(
        id = 299285,
        name = "Mẹt - Bún Đậu Mắm Tôm - Phan Văn Trị",
        address = "825 Phan Văn Trị",
        image = "https://images.foody.vn/res/g30/299285/prof/s180x180/foody-mobile-hmbb-jpg-800-636156815972603330.jpg"
    ),
    Restaurant(
        id = 707241,
        name = "Ngon 24h - Ăn Vặt Miền Quê",
        address = "19 Phạm Văn Đồng",
        image = "https://images.foody.vn/res/g71/707241/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 656152,
        name = "Uncle Tea - Trà Đài Loan - Lê Lợi",
        address = "60 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g66/656152/prof/s180x180/foody-mobile-12-jpg.jpg"
    ),
    Restaurant(
        id = 125075,
        name = "Bột Chiên A Bình - Đường Số 18",
        address = "28 Đường Số 18, P. 8",
        image = "https://images.foody.vn/res/g13/125075/prof/s180x180/foody-mobile-1-jpg-543-636341675731927644.jpg"
    ),
    Restaurant(
        id = 68497,
        name = "Bún Thịt Nướng Ca Na",
        address = "656/17 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g7/68497/prof/s180x180/foody-mobile-ca-na-mb-jpg-481-635691192351294245.jpg"
    ),
    Restaurant(
        id = 649966,
        name = "Gà Nướng Lu Việt Hương - Phạm Văn Đồng",
        address = "20 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g65/649966/prof/s180x180/foody-mobile-6iatjdeu-jpg-149-636325181160761485.jpg"
    ),
    Restaurant(
        id = 753717,
        name = "Dì Ba - Nem Nướng Nha Trang",
        address = "354/3 Nguyễn Thái Sơn, P. 5",
        image = "https://images.foody.vn/res/g76/753717/prof/s180x180/foody-upload-api-foody-mobile-4-jpg-181107100246.jpg"
    ),
    Restaurant(
        id = 138289,
        name = "Kem Cuộn Mập Mập - Phạm Văn Đồng",
        address = "343 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g14/138289/prof/s180x180/foody-mobile-hmb-f-jpg-194-635780216293647988.jpg"
    ),
    Restaurant(
        id = 202110,
        name = "Sentosa Food - Cháo Ếch Singapore - Quang Trung",
        address = "648 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g21/202110/prof/s180x180/foody-mobile--d334d84ac7910fc5e13-836-635881186611850240.jpg"
    ),
    Restaurant(
        id = 210811,
        name = "Beefsteak Hai Con Bò - Quang Trung",
        address = "417 Quang Trung",
        image = "https://images.foody.vn/res/g22/210811/prof/s180x180/foody-mobile-c2-jpg-783-635913881618926866.jpg"
    ),
    Restaurant(
        id = 698823,
        name = "Royaltea Vietnam - Lotte Mart Gò Vấp",
        address = "Tầng Trệt Lotte Mart Gò Vấp, 242 Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g70/698823/prof/s180x180/foody-mobile-dfg-jpg.jpg"
    ),
    Restaurant(
        id = 160920,
        name = "Uyên Phương - Bánh Canh & Chả Cuốn",
        address = "24 Lê Đức Thọ, P. 7",
        image = "https://images.foody.vn/res/g17/160920/prof/s180x180/foody-mobile--3-_hinhmob-jpg-410-635754011473126658.jpg"
    ),
    Restaurant(
        id = 287030,
        name = "Mì Cay Ớt Ma - Lê Văn Thọ",
        address = "4 Lê Văn Thọ. P. 11",
        image = "https://images.foody.vn/res/g29/287030/prof/s180x180/foody-mobile-mobile-jpg-851-636153368102549117.jpg"
    ),
    Restaurant(
        id = 292136,
        name = "Mẹt Quán - Ẩm Thực Tây Bắc",
        address = "1117 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g30/292136/prof/s180x180/foody-mobile-foody-1-636205176316-760-636207627268741104.jpg"
    ),
    Restaurant(
        id = 691981,
        name = "SNOB Coffee - Phan Văn Trị",
        address = "527 Phan Văn Trị",
        image = "https://images.foody.vn/res/g70/691981/prof/s180x180/foody-mobile-c2-jpg-338-636425346356729987.jpg"
    ),
    Restaurant(
        id = 96348,
        name = "Bánh Khọt Vũng Tàu",
        address = "62 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g10/96348/prof/s180x180/foody-mobile-12-jpg-849-636178511481320008.jpg"
    ),
    Restaurant(
        id = 23579,
        name = "Ốc Bống - Nguyễn Kiệm",
        address = "101/717C Nguyễn Kiệm",
        image = "https://images.foody.vn/res/g3/23579/prof/s180x180/foody-mobile-oc-bong-ho-chi-minh.jpg"
    ),
    Restaurant(
        id = 760594,
        name = "Dinosaur Hotpot & Sushi",
        address = "543/13 Lê Đức Thọ, P. 7",
        image = "https://images.foody.vn/res/g77/760594/prof/s180x180/foody-upload-api-foody-mobile-hmmmmmm-jpg-180723114429.jpg"
    ),
    Restaurant(
        id = 773195,
        name = "Three O'clock - Coffee & Tea - Phan Văn Trị",
        address = "459 - 461 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g78/773195/prof/s180x180/foody-upload-api-foody-mobile-10-jpg-180830165919.jpg"
    ),
    Restaurant(
        id = 81428,
        name = "Hủ Tíu Mì 217",
        address = "217 Nguyễn Văn Nghi, P. 7 ",
        image = "https://images.foody.vn/res/g9/81428/prof/s180x180/foody-mobile-hu-tieu-mi-217-mb-jp-142-635860366033893806.jpg"
    ),
    Restaurant(
        id = 85647,
        name = "Xiên Nướng 5K",
        address = "70Bis Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g9/85647/prof/s180x180/foody-mobile-dur47o28-jpg-714-635845583595000607.jpg"
    ),
    Restaurant(
        id = 86008,
        name = "Steak Viet - Phan Văn Trị",
        address = "610 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g9/86008/prof/s180x180/foody-mobile-steak-vietsteak-viet-tp-hcm-140716092753.jpg"
    ),
    Restaurant(
        id = 232712,
        name = "Hana - Mì Cay 7 Cấp Độ Hàn Quốc - Quang Trung",
        address = "751 Quang Trung, P. 12",
        image = "https://images.foody.vn/res/g24/232712/prof/s180x180/foody-mobile-1-jpg-352-636288193476021415.jpg"
    ),
    Restaurant(
        id = 4239,
        name = "Sài Gòn Givral Bakery - 513 Quang Trung",
        address = "513 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g1/4239/prof/s180x180/foody-mobile-12-jpg-169-636227705270291385.jpg"
    ),
    Restaurant(
        id = 774377,
        name = "Phúc Long Coffee & Tea - Vincom Quang Trung",
        address = "190 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g78/774377/prof/s180x180/foody-upload-api-foody-mobile-foody-mobile-phuc-lo-180829173119.jpg"
    ),
    Restaurant(
        id = 740250,
        name = "Thế Giới Steak - Phan Văn Trị",
        address = "493 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g75/740250/prof/s180x180/foody-upload-api-foody-mobile-10-jpg-180511113835.jpg"
    ),
    Restaurant(
        id = 699146,
        name = "Taste Tea - Nguyễn Oanh",
        address = "89 Nguyễn Oanh",
        image = "https://images.foody.vn/res/g70/699146/prof/s180x180/foody-mobile-gdfgd-jpg.jpg"
    ),
    Restaurant(
        id = 66896,
        name = "Mì Vịt Tiềm A Cón - Quang Trung",
        address = "557 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g7/66896/prof/s180x180/foody-mobile-g3el0cgn-jpg-492-636163719284199172.jpg"
    ),
    Restaurant(
        id = 660382,
        name = "Trà Sữa Huy's - Quang Trung",
        address = "781 Quang Trung, P. 12",
        image = "https://images.foody.vn/res/g67/660382/prof/s180x180/foody-mobile-10-jpg.jpg"
    ),
    Restaurant(
        id = 76906,
        name = "Bánh Xèo Bình Định - 244 Lê Đức Thọ",
        address = "244 Lê Đức Thọ, P. 6",
        image = "https://images.foody.vn/res/g8/76906/prof/s180x180/foody-mobile-banh-xeo-binh-dinh-tp-hcm-140529092908.jpg"
    ),
    Restaurant(
        id = 24925,
        name = "Bánh Canh Ghẹ Bảy Liên",
        address = "778F Nguyễn Kiệm, P. 4",
        image = "https://images.foody.vn/res/g3/24925/prof/s180x180/foody-mobile-banh-canh-ghe-bay-lien-tp-hcm.jpg"
    ),
    Restaurant(
        id = 2849,
        name = "Brodard Bakery - Quang Trung ",
        address = "08 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g1/2849/prof/s180x180/foody-mobile-brodard-bakery-quang-trung.jpg"
    ),
    Restaurant(
        id = 112575,
        name = "Tài Vượng Quán 2 - Lẩu & Nướng - Phạm Văn Đồng",
        address = "35 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g12/112575/prof/s180x180/foody-mobile-pvd-jpg-536-636286277093573508.jpg"
    ),
    Restaurant(
        id = 165237,
        name = "Cơm Nướng Hotto - Vincom Quang Trung",
        address = "Tầng B1 Vincom Quang Trung, 190 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g17/165237/prof/s180x180/foody-mobile-hmb-com-jpg-290-635768975088876643.jpg"
    ),
    Restaurant(
        id = 313089,
        name = "Mizu - Trà Kiểu Nhật",
        address = "33 Nguyễn Văn Bảo, P. 4",
        image = "https://images.foody.vn/res/g32/313089/prof/s180x180/foody-mobile-2cgluiqw-jpg-166-636263055408929678.jpg"
    ),
    Restaurant(
        id = 25208,
        name = "Mì Quảng Chính Hiệu",
        address = "163 Lê Văn Thọ, P. 8",
        image = "https://images.foody.vn/res/g3/25208/prof/s180x180/foody-mobile-ya8zk7ud-jpg-916-635878398160148584.jpg"
    ),
    Restaurant(
        id = 638672,
        name = "Youth House Coffee - Nguyễn Văn Nghi",
        address = "29 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g64/638672/prof/s180x180/foody-mobile-12-jpg-994-636251654909241904.jpg"
    ),
    Restaurant(
        id = 159153,
        name = "Trà Sữa Bubble Fly - 23 Lê Lợi",
        address = "23 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g16/159153/prof/s180x180/foody-mobile-t-2-jpg-454-635749861474487268.jpg"
    ),
    Restaurant(
        id = 710828,
        name = "Moda House Coffee",
        address = "11 Nguyễn Oanh, P. 10",
        image = "https://images.foody.vn/res/g72/710828/prof/s180x180/201821153020-img_1924.jpg"
    ),
    Restaurant(
        id = 242389,
        name = "Mì Cay Hara",
        address = "11 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g25/242389/prof/s180x180/foody-mobile-t1-jpg-119-636008074296760420.jpg"
    ),
    Restaurant(
        id = 212471,
        name = "Hủ Tiếu Mực Ông Già Cali - Phạm Văn Đồng",
        address = "288 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g22/212471/prof/s180x180/foody-mobile-10599587_26553083697-143-635919939426908829.jpg"
    ),
    Restaurant(
        id = 190586,
        name = "Obi Cafe",
        address = "5 Nguyễn Duy Cung, P. 12",
        image = "https://images.foody.vn/res/g20/190586/prof/s180x180/foody-mobile-109-jpg-206-635857044211483333.jpg"
    ),
    Restaurant(
        id = 796994,
        name = "Hà Đô - Bún Thái Hải Sản - Shop Online",
        address = "42 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g80/796994/prof/s180x180/foody-upload-api-foody-mobile-foody-bun-thai-2-thu-181128150504.jpg"
    ),
    Restaurant(
        id = 775540,
        name = "Chuti Korean Food - Phan Văn Trị",
        address = "477 Phan Văn Trị",
        image = "https://images.foody.vn/res/g78/775540/prof/s180x180/foody-upload-api-foody-mobile-12-jpg-181001143432.jpg"
    ),
    Restaurant(
        id = 171029,
        name = "Pizza Pan",
        address = "446 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g18/171029/prof/s180x180/foody-mobile-12-jpg-313-635791210757896521.jpg"
    ),
    Restaurant(
        id = 108610,
        name = "Bụi Sài Gòn Nhà Hàng & Cafe",
        address = "792 Nguyễn Kiệm, P. 3 ",
        image = "https://images.foody.vn/res/g11/108610/prof/s180x180/foody-mobile-t-1-jpg-488-635748251075546752.jpg"
    ),
    Restaurant(
        id = 222491,
        name = "Mì Nhật Tano Q",
        address = "77 - 79 Đường Số 11, P. 11",
        image = "https://images.foody.vn/res/g23/222491/prof/s180x180/foody-mobile-qwe-jpg-621-636081695818572737.jpg"
    ),
    Restaurant(
        id = 44574,
        name = "Lẩu Dê Đức Dưỡng",
        address = "607/2/8 Tân Sơn, P. 12",
        image = "https://images.foody.vn/res/g5/44574/prof/s180x180/foody-mobile-m2u8pibm-jpg-259-635806091235850621.jpg"
    ),
    Restaurant(
        id = 781782,
        name = "Ghiền Sữa Chua Nếp Cẩm 39",
        address = "1351/1/2 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g79/781782/prof/s180x180/foody-upload-api-foody-mobile-6-jpg-181206101006.jpg"
    ),
    Restaurant(
        id = 190751,
        name = "Food House Restaurant",
        address = "18B Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g20/190751/prof/s180x180/foody-mobile-food-house-mb-jpg-856-635913176077383645.jpg"
    ),
    Restaurant(
        id = 721062,
        name = "Hutong - Hotpot Paradise - Quang Trung",
        address = "1 Quang Trung",
        image = "https://images.foody.vn/res/g73/721062/prof/s180x180/foody-mobile-gh-jpg.jpg"
    ),
    Restaurant(
        id = 1024,
        name = "Tuệ Mẫn Cafe - Êm Đềm Một Không Gian",
        address = "452 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g1/1024/prof/s180x180/foody-mobile-tue-man-cafe-em-dem-mot-khong-gian.jpg"
    ),
    Restaurant(
        id = 3691,
        name = "Thủy Mộc Cafe - Lê Lợi",
        address = "94 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g1/3691/prof/s180x180/foody-mobile-thuy-moc-cafe-le-loi.jpg"
    ),
    Restaurant(
        id = 240633,
        name = "Cô Túc - Bánh Canh & Mì - Lê Đức Thọ",
        address = "115/47 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g25/240633/prof/s180x180/foody-mobile-hmb-jpg-234-636002145947153031.jpg"
    ),
    Restaurant(
        id = 259843,
        name = "Canh Bún Thái Sơn",
        address = "269 Nguyễn Thái Sơn, P. 7",
        image = "https://images.foody.vn/res/g26/259843/prof/s180x180/foody-mobile-13432343_63381467344-239-636056432074004709.jpg"
    ),
    Restaurant(
        id = 228210,
        name = "Quán Chay Thiên Kim ",
        address = "359 Cây Trâm, P. 8",
        image = "https://images.foody.vn/res/g23/228210/prof/s180x180/foody-mobile-foody-album13-jpg-68-150-635962414570166110.jpg"
    ),
    Restaurant(
        id = 91201,
        name = "Trà Sữa Hoa Hướng Dương - Nguyễn Văn Nghi",
        address = "318 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g10/91201/prof/s180x180/foody-upload-api-foody-mobile-23-jpg-180810144210.jpg"
    ),
    Restaurant(
        id = 633665,
        name = "Hàu Né 3 Ngon - Hàu Né & Bò Bít Tết",
        address = "414 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g64/633665/prof/s180x180/foody-upload-api-foody-mobile-9-jpg-181019145457.jpg"
    ),
    Restaurant(
        id = 72067,
        name = "Bánh Bèo 53",
        address = "53 Nguyễn Thái Sơn, P. 4 ",
        image = "https://images.foody.vn/res/g8/72067/prof/s180x180/foody-mobile-banh-beo-53-tp-hcm-140509090254.jpg"
    ),
    Restaurant(
        id = 123349,
        name = "Mây Thai Tea - Ăn Vặt Thái",
        address = "1074 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g13/123349/prof/s180x180/foody-mobile-ljsabsy7-jpg-597-635989866547649893.jpg"
    ),
    Restaurant(
        id = 242876,
        name = "Phá Lấu & Súp Ngũ Sắc",
        address = "535/8 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g25/242876/prof/s180x180/foody-mobile-es65oj8t-jpg-741-636136777022561001.jpg"
    ),
    Restaurant(
        id = 70776,
        name = "Cơm Tấm Bụi Sài Gòn - Gò Vấp",
        address = "4 Đường Số 7, KDC Cityland, P. 7",
        image = "https://images.foody.vn/res/g8/70776/prof/s180x180/foody-mobile-1x61j7k5-jpg-768-636149725615853397.jpg"
    ),
    Restaurant(
        id = 713718,
        name = "Hẹ Quán - Ăn Vặt Phú Yên",
        address = "68 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g72/713718/prof/s180x180/foody-upload-api-foody-mobile-qn-jpg-180811121445.jpg"
    ),
    Restaurant(
        id = 702084,
        name = "Alas - Pets & Garden Coffee",
        address = "50/8 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g71/702084/prof/s180x180/foody-mobile-23519025_32309684149.jpg"
    ),
    Restaurant(
        id = 148111,
        name = "Bún Đậu Hà Nội - Thống Nhất",
        address = "284 - 286 Thống Nhất. P. 10",
        image = "https://images.foody.vn/res/g15/148111/prof/s180x180/foody-mobile-clgkvryy-jpg-348-636265679475211702.jpg"
    ),
    Restaurant(
        id = 786623,
        name = "Ajikoi By Tea, Coffee & Cream - Phan Văn Trị",
        address = "507 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g79/786623/prof/s180x180/foody-upload-api-foody-mobile-48a-png-181019144133.jpg"
    ),
    Restaurant(
        id = 681773,
        name = "Buffet Haha",
        address = "292 Nguyễn Văn Lượng, P. 17",
        image = "https://images.foody.vn/res/g69/681773/prof/s180x180/foody-mobile-3-jpg-866-636437581940271931.jpg"
    ),
    Restaurant(
        id = 292957,
        name = "I.Korea.U - Ẩm Thực Hàn Quốc",
        address = "808 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g30/292957/prof/s180x180/foody-mobile-untitled-1-jpg-341-636161912695451922.jpg"
    ),
    Restaurant(
        id = 161512,
        name = "Papiyon Coffee - Nguyễn Văn Lượng",
        address = "46 Nguyễn Văn Lượng, P. 17",
        image = "https://images.foody.vn/res/g17/161512/prof/s180x180/foody-mobile-v1-jpg-484-635756589500727964.jpg"
    ),
    Restaurant(
        id = 745008,
        name = "Gà Chảnh - Gà Nướng Tóp Mỡ - Shop Online",
        address = "214/50/6 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g75/745008/prof/s180x180/foody-upload-api-foody-mobile-hms-jpg-180528093530.jpg"
    ),
    Restaurant(
        id = 650843,
        name = "Nhà Hàng Những Chàng Trai - Buffet BBQ Không Khói",
        address = "233 Phan Huy Ích, P. 14",
        image = "https://images.foody.vn/res/g66/650843/prof/s180x180/foody-mobile-1-jpg-497-636277580792105274.jpg"
    ),
    Restaurant(
        id = 746877,
        name = "Bean Plus Coffee & Tea",
        address = "465/7 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g75/746877/prof/s180x180/foody-upload-api-foody-mobile-11-jpg-180620172547.jpg"
    ),
    Restaurant(
        id = 891423,
        name = "Rau Má Pha - Phan Văn Trị",
        address = "507 Phan Văn Trị",
        image = "https://images.foody.vn/res/g90/891423/prof/s180x180/foody-upload-api-foody-mobile-hmk-190304114009.jpg"
    ),
    Restaurant(
        id = 218822,
        name = "Super Prime Pizza - E Mart Gò Vấp",
        address = "Tầng 1 E - Mart Gò Vấp, 366 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g22/218822/prof/s180x180/foody-mobile-hmb-s-jpg-164-635936316457963833.jpg"
    ),
    Restaurant(
        id = 30737,
        name = "Món Ngon Quy Nhơn",
        address = "194 Nguyễn Thái Sơn (Số Cũ 142 Nguyễn Thái Sơn)",
        image = "https://images.foody.vn/res/g4/30737/prof/s180x180/foody-mobile-foody-mon-ngon-qui-n-741-635883049579655328.jpg"
    ),
    Restaurant(
        id = 644029,
        name = "Bánh Su Kem Chewy Junior - Coopmart Quang Trung",
        address = "GF-09B, Tầng Trệt Coopmart Quang Trung, 304A Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g65/644029/prof/s180x180/foody-mobile-chewy-junior-8-jpg-811-636252753156171344.jpg"
    ),
    Restaurant(
        id = 131869,
        name = "Bún Đậu Xóm Mới - Lê Đức Thọ",
        address = "1022 Lê Đức Thọ",
        image = "https://images.foody.vn/res/g14/131869/prof/s180x180/foody-mobile-t-2-jpg-366-635652039670047404.jpg"
    ),
    Restaurant(
        id = 743484,
        name = "FOX Tea - Trà Sữa & Ăn Vặt",
        address = "202 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g75/743484/prof/s180x180/foody-upload-api-foody-mobile-51-jpg-180723171221.jpg"
    ),
    Restaurant(
        id = 122310,
        name = "Mai Hoa - Bò Lá Lốt, Mỡ Chài ",
        address = "570 Lê Đức Thọ, P. 17 ",
        image = "https://images.foody.vn/res/g13/122310/prof/s180x180/foody-mobile-hmb-lot-jpg-654-635604529864560978.jpg"
    ),
    Restaurant(
        id = 699352,
        name = "Tokyo Deli - Phan Văn Trị",
        address = "366A3 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g70/699352/prof/s180x180/foody-mobile-111-jpg.jpg"
    ),
    Restaurant(
        id = 109822,
        name = "Ngộ - Lẩu & Nướng Hải Sản",
        address = "69 Phạm Văn Chiêu, P. 14",
        image = "https://images.foody.vn/res/g11/109822/prof/s180x180/foody-mobile-12354789-jpg-750-636265767963344215.jpg"
    ),
    Restaurant(
        id = 705229,
        name = "Cơm Tấm Cali 20 - Phạm Ngũ Lão",
        address = "278 Phạm Ngũ Lão",
        image = "https://images.foody.vn/res/g71/705229/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 121843,
        name = "Thành Danh - Bánh Tráng Trộn Tây Ninh",
        address = "245 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g13/121843/prof/s180x180/foody-mobile-11-jpg-336-636131860337102634.jpg"
    ),
    Restaurant(
        id = 207545,
        name = "Quán Tư Mập - Cá Lóc Nướng",
        address = "33 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g21/207545/prof/s180x180/foody-mobile-hmb-s-jpg-307-635893150900237942.jpg"
    ),
    Restaurant(
        id = 232296,
        name = "Food Court - Lotte Mart Gò Vấp",
        address = "242 Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g24/232296/prof/s180x180/foody-mobile-foody-food-court-lot-390-635975367536541630.jpg"
    ),
    Restaurant(
        id = 664391,
        name = "Minh Anh - Quán Mì Cay & Bánh Mì Chảo",
        address = "440/60 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g67/664391/prof/s180x180/foody-mobile-1-jpg-892-636343512710995191.jpg"
    ),
    Restaurant(
        id = 769066,
        name = "Kem Xôi Dừa Bu Bu - Phan Văn Trị",
        address = "622 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g77/769066/prof/s180x180/foody-upload-api-foody-mobile-hj-gif-180813110339.jpg"
    ),
    Restaurant(
        id = 893821,
        name = "Chè Thanh Xuân - Chè Tuyết Yến Nhựa Đào",
        address = "28/25 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g90/893821/prof/s180x180/foody-upload-api-foody-mobile-hmm-190313101516.jpg"
    ),
    Restaurant(
        id = 629788,
        name = "JNGON - Cơm Gà Chiên Giòn",
        address = "54 Đường Số 4, P. 7",
        image = "https://images.foody.vn/res/g63/629788/prof/s180x180/foody-mobile-123-jpg-157-636219724874900620.jpg"
    ),
    Restaurant(
        id = 1357,
        name = "Lẩu Dê SOS Làng Hoa",
        address = "12/6A Đường 10, P. 9",
        image = "https://images.foody.vn/res/g1/1357/prof/s180x180/foody-mobile-hmb-lang-hoa-jpg-170-635739344563109371.jpg"
    ),
    Restaurant(
        id = 714938,
        name = "Sương Sáo Đài Loan",
        address = "2 Đường Số 2, KDC Cityland, P. 7",
        image = "https://images.foody.vn/res/g72/714938/prof/s180x180/foody-mobile-hmn-jpg.jpg"
    ),
    Restaurant(
        id = 669899,
        name = "Hali Sushi - Nguyễn Văn Lượng",
        address = "227 Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g67/669899/prof/s180x180/foody-mobile-64347406c21784b7a686-842-636341701356409022.jpg"
    ),
    Restaurant(
        id = 236293,
        name = "Bếp Ông Bà Tư - Quán Ăn Hàn Nhật",
        address = "127/32 Nguyễn Tư Giản, P. 12",
        image = "https://images.foody.vn/res/g24/236293/prof/s180x180/foody-mobile-obt-jpg-414-636403946012148428.jpg"
    ),
    Restaurant(
        id = 10511,
        name = "Ốc Hoàng - Trương Minh Giảng",
        address = "11 Trương Minh Giảng, P. 17",
        image = "https://images.foody.vn/res/g2/10511/prof/s180x180/foody-mobile-q0yvghan-jpg-905-636035845580689763.jpg"
    ),
    Restaurant(
        id = 107629,
        name = "Rin-O Tea - Trà Sữa & Trái Cây Tô",
        address = "40/36 Lê Thị Hồng, Khu Phố 12, P. 17",
        image = "https://images.foody.vn/res/g11/107629/prof/s180x180/foody-upload-api-foody-mobile-33323815_39160839801-180622111704.jpg"
    ),
    Restaurant(
        id = 677028,
        name = "Bò Bía 98 - Shop Online",
        address = "98 Nguyễn Thượng Hiền, P. 1",
        image = "https://images.foody.vn/res/g68/677028/prof/s180x180/foody-upload-api-foody-mobile-4-jpg-180716093438.jpg"
    ),
    Restaurant(
        id = 213883,
        name = "Sữa Chua Tin Tin - Shop Online",
        address = "2 Đường Số 20, Dương Quảng Hàm",
        image = "https://images.foody.vn/res/g22/213883/prof/s180x180/foody-mobile-k4lsvgh4-jpg-687-635929613312295679.jpg"
    ),
    Restaurant(
        id = 71105,
        name = "Quán Phương - Bún Bắp Bò",
        address = "2 Nguyễn Oanh, P. 7",
        image = "https://images.foody.vn/res/g8/71105/prof/s180x180/foody-mobile-phuong-bun-bap-bo-tp-hcm-140429102152.jpg"
    ),
    Restaurant(
        id = 247811,
        name = "Xoài Lắc Lão Xoài",
        address = "60 Phạm Văn Chiêu, P. 8",
        image = "https://images.foody.vn/res/g25/247811/prof/s180x180/foody-upload-api-foody-mobile-2-190617101700.jpg"
    ),
    Restaurant(
        id = 727088,
        name = "Ăn Vặt Cô Thơ - Shop Online",
        address = "15/29bis Nguyễn Du",
        image = "https://images.foody.vn/res/g73/727088/prof/s180x180/foody-upload-api-foody-mobile-dfdfb-jpg-180328174019.jpg"
    ),
    Restaurant(
        id = 165240,
        name = "BreadTalk - Vincom Quang Trung",
        address = "L1-11 Vincom Quang Trung",
        image = "https://images.foody.vn/res/g17/165240/prof/s180x180/foody-upload-api-foody-mobile-2-190226091621.jpg"
    ),
    Restaurant(
        id = 4484,
        name = "Phố Nướng Tư Trì - Lê Văn Thọ",
        address = "499 Lê Văn Thọ, P. 9",
        image = "https://images.foody.vn/res/g1/4484/prof/s180x180/foody-mobile-tu-tri-jpg-309-636135017286341014.jpg"
    ),
    Restaurant(
        id = 156929,
        name = "Velvet Garden - Bùi Quang Là",
        address = "114 Bùi Quang Là, P. 12",
        image = "https://images.foody.vn/res/g16/156929/prof/s180x180/foody-mobile-x3-jpg-826-635760107760206748.jpg"
    ),
    Restaurant(
        id = 142098,
        name = "MỘC - Ẩm Thực Than Hồng",
        address = "1B Lê Đức Thọ, P. 7",
        image = "https://images.foody.vn/res/g15/142098/prof/s180x180/foody-mobile-h1-jpg-820-636002237479629308.jpg"
    ),
    Restaurant(
        id = 795931,
        name = "Bò Lế Rồ - Bò Nhúng Sốt & Nhúng Lẩu - Phan Văn Trị",
        address = "1445 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g80/795931/prof/s180x180/foody-upload-api-foody-mobile-hmnnn-jpg-181107111013.jpg"
    ),
    Restaurant(
        id = 49571,
        name = "Bánh Bao Thơm Ngon - Lê Lợi",
        address = "58 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g5/49571/prof/s180x180/foody-mobile-banh-bao-thom-ngon-le-loi-tp-hcm-140215025155.jpg"
    ),
    Restaurant(
        id = 188302,
        name = "Bánh Đúc Nóng Hương Quê",
        address = "258 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g19/188302/prof/s180x180/foody-mobile-t10hanvj-jpg-697-636124079659538406.jpg"
    ),
    Restaurant(
        id = 2414,
        name = "Khoảng Lặng Coffee",
        address = "E10C An Nhơn, P. 17",
        image = "https://images.foody.vn/res/g1/2414/prof/s180x180/foody-mobile-khoang-lang.jpg"
    ),
    Restaurant(
        id = 82999,
        name = "Miss Kim - Mực Sạch Rim",
        address = "566/137/6 Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g9/82999/prof/s180x180/foody-mobile-miss-kim-muc-sach-rim-tp-hcm-140616045529.jpg"
    ),
    Restaurant(
        id = 209046,
        name = "Trà Sữa Lee Cường",
        address = "115/53 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g21/209046/prof/s180x180/foody-mobile-1-jpg-295-636361574789086963.jpg"
    ),
    Restaurant(
        id = 95070,
        name = "Bò Tơ Tây Ninh Năm Sánh - Nguyễn Oanh",
        address = "152 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g10/95070/prof/s180x180/foody-mobile-7oy9ciu3-jpg-450-635866505139741608.jpg"
    ),
    Restaurant(
        id = 258640,
        name = "Sữa Việt - Trà Sữa Nhà Làm",
        address = "309 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g26/258640/prof/s180x180/foody-upload-api-foody-mobile-8-190128081253.jpg"
    ),
    Restaurant(
        id = 93529,
        name = "Ngói Đỏ BBQ",
        address = "918 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g10/93529/prof/s180x180/foody-mobile-22209524_0ck-bh2h83y-635464591733598298.jpg"
    ),
    Restaurant(
        id = 233081,
        name = "Kem Dairy Queen - Lotte Mart Gò Vấp",
        address = "Tầng Trệt Lotte Mart Gò Vấp, 242 Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g24/233081/prof/s180x180/foody-upload-api-foody-mobile-hh-190308104724.jpg"
    ),
    Restaurant(
        id = 666766,
        name = "Bún Đậu 426 - Cây Trâm",
        address = "395 Cây Trâm, P. 8",
        image = "https://images.foody.vn/res/g67/666766/prof/s180x180/foody-mobile-hmbd-jpg-273-636330460776645292.jpg"
    ),
    Restaurant(
        id = 892524,
        name = "Don Chicken - Nhà Hàng Gà Nướng Hàn Quốc - Phan Văn Trị",
        address = "366A32 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g90/892524/prof/s180x180/foody-upload-api-foody-mobile-hmk-190308102716.jpg"
    ),
    Restaurant(
        id = 72507,
        name = "Cơm Chay Việt - Nguyễn Thái Sơn",
        address = "103 Nguyễn Thái Sơn, P. 4",
        image = "https://images.foody.vn/res/g8/72507/prof/s180x180/foody-mobile-taa9svoy-jpg-850-636171506040472783.jpg"
    ),
    Restaurant(
        id = 703989,
        name = "Không Gian Xanh - Ẩm Thực Hội An",
        address = "872/25/15 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g71/703989/prof/s180x180/foody-mobile-3-jpg.jpg"
    ),
    Restaurant(
        id = 126552,
        name = "Cơm Tấm Cây Khế 3 - Cây Trâm",
        address = "126 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g13/126552/prof/s180x180/foody-mobile-1236-jpg-306-636396142547109066.jpg"
    ),
    Restaurant(
        id = 130239,
        name = "Vịt Lộn - Nguyễn Oanh",
        address = "Ngã 3 Nguyễn Oanh - Nguyễn Văn Lượng",
        image = "https://images.foody.vn/res/g14/130239/prof/s180x180/foody-mobile-12354-jpg-365-636168972188720881.jpg"
    ),
    Restaurant(
        id = 879286,
        name = "Your Coffee Cart - Cafe Sữa Bọt",
        address = "254 Phạm Ngũ Lão, P. 7",
        image = "https://images.foody.vn/res/g88/879286/prof/s180x180/foody-upload-api-foody-mobile-19b-190112152045.jpg"
    ),
    Restaurant(
        id = 743954,
        name = "Cô Thảo - Bánh Tráng Nướng",
        address = "405/47 Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g75/743954/prof/s180x180/foody-upload-api-foody-mobile-hms-jpg-180523171644.jpg"
    ),
    Restaurant(
        id = 73333,
        name = "Ụt Ụt Coffee",
        address = "40 Nguyễn Tuân, P. 3",
        image = "https://images.foody.vn/res/g8/73333/prof/s180x180/foody-mobile-c8a22i09-jpg-234-635779325400263216.jpg"
    ),
    Restaurant(
        id = 295471,
        name = "Bún Bò Ghẹ 7 Ghiền",
        address = "660 Lê Văn Thọ, P. 13",
        image = "https://images.foody.vn/res/g30/295471/prof/s180x180/foody-upload-api-foody-mobile-15-190607110539.jpg"
    ),
    Restaurant(
        id = 729798,
        name = "Sữa Chua Nếp Cẩm - Gà Cay Minh Hà - Shop Online",
        address = "200 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g73/729798/prof/s180x180/foody-upload-api-foody-mobile-foody-sua-chua-nep-c-180407104128.jpg"
    ),
    Restaurant(
        id = 72472,
        name = "Tiệm Bánh Mì Những Chàng Trai",
        address = "591 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g8/72472/prof/s180x180/foody-mobile-kaud1rff-jpg-291-636219915377120539.jpg"
    ),
    Restaurant(
        id = 217594,
        name = "Tiên Tiên - Bún Đậu Mắm Tôm & Nem Nướng",
        address = "206 Thống Nhất, P. 10",
        image = "https://images.foody.vn/res/g22/217594/prof/s180x180/foody-mobile-dad-jpg-498-635932019739765047.jpg"
    ),
    Restaurant(
        id = 801641,
        name = "Cháo Lòng Bà Tư Mập",
        address = "14 Nguyễn Văn Dung, P. 6",
        image = "https://images.foody.vn/res/g81/801641/prof/s180x180/foody-upload-api-foody-mobile-hmn447-jpg-181121161921.jpg"
    ),
    Restaurant(
        id = 250935,
        name = "Rất Quảng Quán - Mì Quảng",
        address = "35 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g26/250935/prof/s180x180/foody-mobile-t2-jpg-875-636028150237713186.jpg"
    ),
    Restaurant(
        id = 96301,
        name = "Style - Quán Nướng Ngói",
        address = "Chung Cư K26, P. 7",
        image = "https://images.foody.vn/res/g10/96301/prof/s180x180/foody-mobile-tyle-mb-jpg-795-635859505889723042.jpg"
    ),
    Restaurant(
        id = 260306,
        name = "Nhà Hàng Đùi Cừu Nướng - Tân Sơn",
        address = "395/2 Tân Sơn",
        image = "https://images.foody.vn/res/g27/260306/prof/s180x180/foody-mobile-c2-jpg-506-636051429704140905.jpg"
    ),
    Restaurant(
        id = 699733,
        name = "Friends By Pontian - Mì Singapore - Lotte Mart Gò Vấp",
        address = "Lotte Mart Gò Vấp, 242 Nguyễn Văn Lượng",
        image = "https://images.foody.vn/res/g70/699733/prof/s180x180/foody-mobile-t501h35j-jpg.jpg"
    ),
    Restaurant(
        id = 1259,
        name = "Khúc Mùa Thu Cafe",
        address = "22/3 Đường Số 21, P. 8",
        image = "https://images.foody.vn/res/g1/1259/prof/s180x180/foody-mobile-hmb-kmt-jpg-476-635772333598832792.jpg"
    ),
    Restaurant(
        id = 273798,
        name = "Mặt Trời Đỏ - Buffet BBQ",
        address = "295 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g28/273798/prof/s180x180/foody-mobile-img_2527-jpg-557-636144753941043791.jpg"
    ),
    Restaurant(
        id = 107309,
        name = "Lẩu & Nướng Leng Keng",
        address = "66 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g11/107309/prof/s180x180/foody-mobile-vevj8yrt-jpg-756-636003728950074703.jpg"
    ),
    Restaurant(
        id = 204747,
        name = "Kem Fanny - Emart Gò Vấp",
        address = "Tầng B2 - 3, Emart Gò Vấp, 366 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g21/204747/prof/s180x180/foody-upload-api-foody-mobile-1-jpg-180607162442.jpg"
    ),
    Restaurant(
        id = 632537,
        name = "Cơm Niêu Thanh Dung - Vincom Quang Trung",
        address = "Tầng B1 Vincom Quang Trung, 190 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g64/632537/prof/s180x180/foody-mobile-foody-com-nieu-thanh-267-636226007562347658.jpg"
    ),
    Restaurant(
        id = 710785,
        name = "Kenshin Tea - Trà Sữa Đồng Giá 19k - Quang Trung",
        address = "1096 Quang Trung",
        image = "https://images.foody.vn/res/g72/710785/prof/s180x180/foody-upload-api-foody-mobile-11-jpg-181207155819.jpg"
    ),
    Restaurant(
        id = 921963,
        name = "Hằng Ù - Chân Gà Rút Xương Ngâm Sả Tắc - Shop Online",
        address = "28/25 Lê Thị Hồng",
        image = "https://images.foody.vn/res/g93/921963/prof/s180x180/foody-upload-api-foody-mobile-5-190531101349.jpg"
    ),
    Restaurant(
        id = 243022,
        name = "Rest - Milk Tea & Coffee",
        address = "233 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g25/243022/prof/s180x180/foody-mobile-yummy-pucca-tra-sua--608-636008184788294962.jpg"
    ),
    Restaurant(
        id = 84955,
        name = "Xì Trum Quán - Lê Thị Hồng",
        address = "40/72 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g9/84955/prof/s180x180/foody-mobile-xi-trum-quan-le-thi-hong-tp-hcm-140702021648.jpg"
    ),
    Restaurant(
        id = 720505,
        name = "Ding Tea - Quang Trung",
        address = "972 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g73/720505/prof/s180x180/foody-mobile-hmn-jpg.jpg"
    ),
    Restaurant(
        id = 258232,
        name = "Đường Ray Quán - Lẩu & Nướng",
        address = "211 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g26/258232/prof/s180x180/foody-mobile-t1-jpg-855-636045351104238311.jpg"
    ),
    Restaurant(
        id = 241191,
        name = "Mì Cay & Cơm Chảo - Gia Hân Hân",
        address = "36 Đường 18, P. 8",
        image = "https://images.foody.vn/res/g25/241191/prof/s180x180/foody-mobile-1-jpg-859-636004798869374643.jpg"
    ),
    Restaurant(
        id = 313286,
        name = "Đất Sài Gòn - Cafe Ngắm Máy Bay",
        address = "389/24 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g32/313286/prof/s180x180/foody-mobile-h09tno9j-jpg-124-636269816477870433.jpg"
    ),
    Restaurant(
        id = 752573,
        name = "Trà Sữa R&B Tea - 177 Quang Trung",
        address = "177 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g76/752573/prof/s180x180/foody-upload-api-foody-mobile-40a-190508162641.jpg"
    ),
    Restaurant(
        id = 715855,
        name = "Mr.Good Tea - Cây Trâm",
        address = "433 Cây Trâm, P. 8 ",
        image = "https://images.foody.vn/res/g72/715855/prof/s180x180/foody-mobile-26168472_21618152305.jpg"
    ),
    Restaurant(
        id = 33317,
        name = "Quán Ăn Tâm Ký - Phan Huy Ích",
        address = "333 Phan Huy Ích, P. 12",
        image = "https://images.foody.vn/res/g4/33317/prof/s180x180/foody-mobile-3o46h0z9-jpg-754-635826581737461869.jpg"
    ),
    Restaurant(
        id = 12548,
        name = "Đào Nguyên - Cafe Sân Thượng",
        address = "101 Quang Trung",
        image = "https://images.foody.vn/res/g2/12548/prof/s180x180/foody-mobile-daonguyen-jpg-189-635741015050089363.jpg"
    ),
    Restaurant(
        id = 164913,
        name = "Món Ngon - Ăn Vặt - Shop Online",
        address = "108/794B Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g17/164913/prof/s180x180/foody-mobile-an-vat-shop-mb-jpg-142-635791345765369649.jpg"
    ),
    Restaurant(
        id = 698695,
        name = "Ken's Coffee - Lê Lợi",
        address = "101 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g70/698695/prof/s180x180/foody-mobile--95c43bb871bfc33feb4.jpg"
    ),
    Restaurant(
        id = 145406,
        name = "Ăn Vặt Green Up - Phan Văn Trị",
        address = "558 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g15/145406/prof/s180x180/foody-mobile-an-vat-green-mb-jpg-638-635736330995373465.jpg"
    ),
    Restaurant(
        id = 245970,
        name = "Bamboo Dimsum - Lotte Mart Gò Vấp",
        address = "Tầng Trệt Lotte Mart Gò Vấp, 242 Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g25/245970/prof/s180x180/foody-mobile-3uqreebi-jpg-438-636274380089796325.jpg"
    ),
    Restaurant(
        id = 128384,
        name = "Cơm Tấm Tài - Nguyễn Văn Nghi",
        address = "100 Nguyễn Văn Nghi",
        image = "https://images.foody.vn/res/g13/128384/prof/s180x180/foody-mobile-hmb-com-tam-tai-jpg-968-635634776675955533.jpg"
    ),
    Restaurant(
        id = 105851,
        name = "Mực Rim Me Ngọc Phương - Đặc Sản Ba Miền",
        address = " 372 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g11/105851/prof/s180x180/foody-mobile-12-1-jpg-826-635537318836096478.jpg"
    ),
    Restaurant(
        id = 94302,
        name = "Trọng Tín - Xôi Gà",
        address = "104 Nguyễn Văn Nghi, P. 5 ",
        image = "https://images.foody.vn/res/g10/94302/prof/s180x180/foody-mobile-xc7z9379-jpg-307-636165426834390084.jpg"
    ),
    Restaurant(
        id = 31650,
        name = "Mì Quảng Vinh",
        address = "111/23 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g4/31650/prof/s180x180/foody-mobile-5s66c63j-jpg-318-636020096300746914.jpg"
    ),
    Restaurant(
        id = 715701,
        name = "Ốc Lắc Cô Mai - Lê Thị Hồng",
        address = "36/114 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g72/715701/prof/s180x180/foody-mobile-23434948_94257029589.jpg"
    ),
    Restaurant(
        id = 707716,
        name = "Cơm Tấm Tròn",
        address = "18 Nguyễn Tuân, P. 3",
        image = "https://images.foody.vn/res/g71/707716/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 2039,
        name = "Vuông Tròn Cafe",
        address = "24/2A Quang Trung",
        image = "https://images.foody.vn/res/g1/2039/prof/s180x180/foody-mobile-vuong-tron-cafe-jpg-734-635783579310426815.jpg"
    ),
    Restaurant(
        id = 677494,
        name = "Hana Sushi - Nguyễn Văn Lượng",
        address = "282 Nguyễn Văn Lượng, P. 17",
        image = "https://images.foody.vn/res/g68/677494/prof/s180x180/foody-mobile-dgfdgfgdgf-jpg-781-636371781877700554.jpg"
    ),
    Restaurant(
        id = 721439,
        name = "Meet & More - Trà Sữa Hàn Quốc - Lê Đức Thọ",
        address = "164 Lê Đức Thọ, P. 6",
        image = "https://images.foody.vn/res/g73/721439/prof/s180x180/foody-mobile-28577074_22557506467.jpg"
    ),
    Restaurant(
        id = 681213,
        name = "Quán Ngon - Bánh Hỏi Cháo Lòng & Bánh Canh Bột Gạo - Cây Trâm",
        address = "160 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g69/681213/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 106032,
        name = "Cháo Ếch Singapore - Quang Trung",
        address = "357 Quang Trung ",
        image = "https://images.foody.vn/res/g11/106032/prof/s180x180/foody-mobile-ucfkoblb-jpg-205-636269966576026659.jpg"
    ),
    Restaurant(
        id = 137120,
        name = "Funny - Trà Sữa & Bánh Tráng Trộn - Nguyễn Văn Công",
        address = "402 Nguyễn Văn Công, P. 3",
        image = "https://images.foody.vn/res/g14/137120/prof/s180x180/foody-upload-api-foody-mobile-6-jpg-180412110531.jpg"
    ),
    Restaurant(
        id = 127146,
        name = "Minh Khoa Café Phim ",
        address = "256/53 Phan Huy Ích, P. 12",
        image = "https://images.foody.vn/res/g13/127146/prof/s180x180/foody-mobile-minh-khoa-cafe-mb-jp-421-635780876150158965.jpg"
    ),
    Restaurant(
        id = 673770,
        name = "Chả Lụi Lagi",
        address = "21 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g68/673770/prof/s180x180/foody-mobile-11-jpg-190-636375261580621430.jpg"
    ),
    Restaurant(
        id = 779983,
        name = "BuoNo - Mì Ý Cay",
        address = "538/2 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g78/779983/prof/s180x180/foody-upload-api-foody-mobile-11-jpg-181128153021.jpg"
    ),
    Restaurant(
        id = 877709,
        name = "Mr. Sumo - Mì Ý - Shop Online",
        address = "448/30 Phan Huy Ích, P. 12",
        image = "https://images.foody.vn/res/g88/877709/prof/s180x180/foody-upload-api-foody-mobile-mrsumo-190108100807.jpg"
    ),
    Restaurant(
        id = 730602,
        name = "Uno Quán - Tea & Milktea - Phan Văn Trị",
        address = "733 Phan Văn Trị",
        image = "https://images.foody.vn/res/g74/730602/prof/s180x180/foody-upload-api-foody-mobile-31170868_16036878997-180720181549.jpg"
    ),
    Restaurant(
        id = 776883,
        name = "Ten Ren's Tea - 485 Phan Văn Trị",
        address = "485 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g78/776883/prof/s180x180/foody-upload-api-foody-mobile-8-190318155216.jpg"
    ),
    Restaurant(
        id = 6011,
        name = "Cõi Đá Cafe - Thống Nhất",
        address = "51/4A Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g1/6011/prof/s180x180/foody-mobile-coi-da-cafe-thong-nhat.jpg"
    ),
    Restaurant(
        id = 728551,
        name = "Effoc Cafe - Phan Văn Trị",
        address = "661 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g73/728551/prof/s180x180/foody-upload-api-foody-mobile-27073073_20569406909-180406101305.jpg"
    ),
    Restaurant(
        id = 8201,
        name = "Data Sushi & Cafe",
        address = "4/72 Quang Trung  , P. 10",
        image = "https://images.foody.vn/res/g1/8201/prof/s180x180/foody-mobile-data-sushi-mb-jpg-673-635797431709885407.jpg"
    ),
    Restaurant(
        id = 1363,
        name = "Phở Quê Hương",
        address = "1223/4 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g1/1363/prof/s180x180/foody-mobile-db39r5ui-jpg-249-635806092843433445.jpg"
    ),
    Restaurant(
        id = 739921,
        name = "Vua Cà Ri - Nguyễn Oanh",
        address = "41/16 Nguyễn Oanh, P. 10",
        image = "https://images.foody.vn/res/g74/739921/prof/s180x180/foody-upload-api-foody-mobile-hmm-190312171706.jpg"
    ),
    Restaurant(
        id = 898045,
        name = "Kimbap Hoàng Tử - Món Hàn Quốc - Phan Văn Trị",
        address = "1069 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g90/898045/prof/s180x180/foody-upload-api-foody-mobile-13-190417084510.jpg"
    ),
    Restaurant(
        id = 156893,
        name = "Kem Cuộn Thái Lan - 1124 Quang Trung",
        address = "1124 Quang Trung",
        image = "https://images.foody.vn/res/g16/156893/prof/s180x180/foody-mobile-mobile-jpg-908-635742849784147885.jpg"
    ),
    Restaurant(
        id = 880821,
        name = "K Quán 2 - Kem Chuối & Yaourt Bịch",
        address = "405/14 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g89/880821/prof/s180x180/foody-upload-api-foody-mobile-suachua12-190114115314.jpg"
    ),
    Restaurant(
        id = 83550,
        name = "Tre Vàng - Quán Nhậu Sân Vườn",
        address = "538 Lê Văn Thọ, P. 13",
        image = "https://images.foody.vn/res/g9/83550/prof/s180x180/foody-mobile-5-jpg-491-635845013954852087.jpg"
    ),
    Restaurant(
        id = 163002,
        name = "Ẩm Thực Vợ Tôi - Hải Sản Nha Trang",
        address = "404 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g17/163002/prof/s180x180/foody-mobile-67zvvozo-jpg-282-636153367879463619.jpg"
    ),
    Restaurant(
        id = 733142,
        name = "Basax - Chicken & Korean Food - E Mart Gò Vấp",
        address = "366 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g74/733142/prof/s180x180/foody-upload-api-foody-mobile-26167448_14083822326-180424110115.jpg"
    ),
    Restaurant(
        id = 138535,
        name = "Nướng Tại Bàn 29K - Quang Trung",
        address = "Hẻm 872 Quang Trung",
        image = "https://images.foody.vn/res/g14/138535/prof/s180x180/foody-mobile-nuong-tai-ban-mb-jpg-773-635857336299220358.jpg"
    ),
    Restaurant(
        id = 231391,
        name = "Bánh Mì Chảo 211",
        address = "211 Đường Số 3, P. 11",
        image = "https://images.foody.vn/res/g24/231391/prof/s180x180/foody-mobile-banh-mi-chao-jpg-241-636086661710803880.jpg"
    ),
    Restaurant(
        id = 180574,
        name = "Bếp Của Mèo - Chả Hoa Online",
        address = "145/51/30 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g19/180574/prof/s180x180/foody-mobile-h-jpg-278-635816486898788742.jpg"
    ),
    Restaurant(
        id = 256266,
        name = "Xôi Bình Tiên - Nguyễn Văn Nghi",
        address = "254 Nguyễn Văn Nghi",
        image = "https://images.foody.vn/res/g26/256266/prof/s180x180/foody-mobile-c2-jpg-255-636040929440230262.jpg"
    ),
    Restaurant(
        id = 70062,
        name = "Bánh Căn - Bánh Xèo Quán Tui",
        address = "1/30 Nguyễn Thái Sơn, P. 3",
        image = "https://images.foody.vn/res/g8/70062/prof/s180x180/foody-mobile-banh-can-banh-xeo-quan-tui-tp-hcm-140417093048.jpg"
    ),
    Restaurant(
        id = 93036,
        name = "Bánh Canh Cua Tư Ù - Lê Quang Định",
        address = "672A Lê Quang Định, P. 1",
        image = "https://images.foody.vn/res/g10/93036/prof/s180x180/foody-mobile-tu-u-jpg-669-635730118914831384.jpg"
    ),
    Restaurant(
        id = 683306,
        name = "Ẩm Thực Thành Huế - Cây Trâm",
        address = "287 Cây Trâm",
        image = "https://images.foody.vn/res/g69/683306/prof/s180x180/foody-mobile-dsfsdf-jpg-645-636391859543029399.jpg"
    ),
    Restaurant(
        id = 662963,
        name = "Cuốn Việt - Coopmart Quang Trung",
        address = "Tầng Trệt Coopmart Quang Trung, 304A Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g67/662963/prof/s180x180/foody-mobile-cuon-viet-quang-trun.jpg"
    ),
    Restaurant(
        id = 858264,
        name = "TonyTony Chopper - Lẩu & Mì Cay",
        address = "482 Nguyễn Thái Sơn, P. 5",
        image = "https://images.foody.vn/res/g86/858264/prof/s180x180/foody-upload-api-foody-mobile-47245794_19702954264-181204154615.jpg"
    ),
    Restaurant(
        id = 741447,
        name = "Trà Đá Bơ 1102",
        address = "1 Phạm Văn Đồng",
        image = "https://images.foody.vn/res/g75/741447/prof/s180x180/foody-upload-api-foody-mobile-6-jpg-180712110022.jpg"
    ),
    Restaurant(
        id = 45296,
        name = "Tre Coffee",
        address = "468/2 Phan Văn Trị, P. 7 ",
        image = "https://images.foody.vn/res/g5/45296/prof/s180x180/foody-mobile-mobile-jpg-665-635509763270261435.jpg"
    ),
    Restaurant(
        id = 73069,
        name = "Bún Bò Bà Chiểu 2 - Lê Văn Thọ",
        address = "218A Lê Văn Thọ",
        image = "https://images.foody.vn/res/g8/73069/prof/s180x180/foody-mobile-bun-bo-ba-chieu-jpg-965-636129861648712326.jpg"
    ),
    Restaurant(
        id = 633889,
        name = "Sài Gòn Nướng - Chung Cư K26",
        address = "372 Chung Cư K26 Dương Quảng Hàm",
        image = "https://images.foody.vn/res/g64/633889/prof/s180x180/foody-mobile-hmb-jpg-331-636227685533045806.jpg"
    ),
    Restaurant(
        id = 30813,
        name = "Đồng Xanh Cafe - Lê Đức Thọ",
        address = "1053 Lê Đức Thọ, P. 13",
        image = "https://images.foody.vn/res/g4/30813/prof/s180x180/foody-mobile-7gmyp30t-jpg-375-635838857878424261.jpg"
    ),
    Restaurant(
        id = 663957,
        name = "King Beef BBQ",
        address = "16 Phạm Ngũ Lão, P. 4",
        image = "https://images.foody.vn/res/g67/663957/prof/s180x180/foody-mobile-hmb-jpg-331-636319994984141622.jpg"
    ),
    Restaurant(
        id = 155632,
        name = "Na Coffee - Nguyễn Văn Bảo",
        address = "15 Nguyễn Văn Bảo, P. 4",
        image = "https://images.foody.vn/res/g16/155632/prof/s180x180/foody-mobile-na-coffee-mb-jpg-672-635986737421787717.jpg"
    ),
    Restaurant(
        id = 729080,
        name = "Joah Tea - Street Coffee",
        address = "198 Nguyễn Văn Nghi, P. 7",
        image = "https://images.foody.vn/res/g73/729080/prof/s180x180/foody-upload-api-foody-mobile-28576245_91347590549-180402093249.jpg"
    ),
    Restaurant(
        id = 174833,
        name = "Kem Tự Chọn Tank",
        address = "422 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g18/174833/prof/s180x180/foody-mobile-t2-jpg-303-635798953552965114.jpg"
    ),
    Restaurant(
        id = 704155,
        name = "Triệu Vy - Bún Đậu Mắm Tôm",
        address = "17 Dương Quảng Hàm. P. 5",
        image = "https://images.foody.vn/res/g71/704155/prof/s180x180/foody-mobile-foody-mobile-y3h3qgu.jpg"
    ),
    Restaurant(
        id = 645490,
        name = "Cake Garden Coffee",
        address = "31/30 Lê Lai, P. 3",
        image = "https://images.foody.vn/res/g65/645490/prof/s180x180/foody-mobile-2-jpg-844-636257752840145187.jpg"
    ),
    Restaurant(
        id = 3454,
        name = "The Valentine - Tiệc Cưới",
        address = "18 Phan Văn Trị",
        image = "https://images.foody.vn/res/g1/3454/prof/s180x180/foody-mobile-happy-garden-tan-binh.jpg"
    ),
    Restaurant(
        id = 246140,
        name = "Ốc 20k",
        address = "688/13 Quang Trung ",
        image = "https://images.foody.vn/res/g25/246140/prof/s180x180/foody-mobile-zxas-jpg-725-636159221864391904.jpg"
    ),
    Restaurant(
        id = 31540,
        name = "Phở Cô Lệ - Đường Số 1",
        address = "69 Đường Số 1, P. 11",
        image = "https://images.foody.vn/res/g4/31540/prof/s180x180/foody-mobile-mobile-jpg-132-636135229634254996.jpg"
    ),
    Restaurant(
        id = 134198,
        name = "Cây Si Cafe - Lê Đức Thọ",
        address = "2 Lê Đức Thọ",
        image = "https://images.foody.vn/res/g14/134198/prof/s180x180/foody-mobile-0b1xmz9m-jpg-446-635953570288373048.jpg"
    ),
    Restaurant(
        id = 651172,
        name = "Bún Bò Vương - Lê Thị Hồng",
        address = "42 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g66/651172/prof/s180x180/foody-mobile-8j5uslu9-jpg-335-636280241169027455.jpg"
    ),
    Restaurant(
        id = 72142,
        name = "Đắc Hùng - Vịt & Heo Quay - Nguyễn Oanh",
        address = "295 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g8/72142/prof/s180x180/foody-mobile-vit-quay-dac-hung-jp-491-636336374760894478.jpg"
    ),
    Restaurant(
        id = 705928,
        name = "Mochi Mochi - Kem Tự Chọn - Lê Văn Thọ",
        address = "324 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g71/705928/prof/s180x180/foody-mobile-mochi-jpg.jpg"
    ),
    Restaurant(
        id = 315708,
        name = "Phở Ngọc - Đường Số 3",
        address = "201 Đường Số 3, P. 9",
        image = "https://images.foody.vn/res/g32/315708/prof/s180x180/foody-mobile-ugftqmea-jpg-960-636198317208884139.jpg"
    ),
    Restaurant(
        id = 207455,
        name = "Saigon NEW Restaurant - Phạm Văn Đồng",
        address = "327 - 329 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g21/207455/prof/s180x180/foody-mobile-18sud5nl-jpg-516-636208446787414339.jpg"
    ),
    Restaurant(
        id = 232011,
        name = "Vuvuzela Beer Club - Lotte Mart Gò Vấp",
        address = "18 Phan Văn Trị - Nguyễn Văn Lượng, P. 10",
        image = "https://images.foody.vn/res/g24/232011/prof/s180x180/foody-mobile-f8hylh48-jpg-350-636044303233235820.jpg"
    ),
    Restaurant(
        id = 94294,
        name = "Ba Duy - Bánh Canh Cá Lóc",
        address = "243 Nguyễn Thái Sơn",
        image = "https://images.foody.vn/res/g10/94294/prof/s180x180/foody-mobile-98kw8u4o-jpg-137-635846741196125826.jpg"
    ),
    Restaurant(
        id = 240865,
        name = "S Phượt Coffee",
        address = "891/9 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g25/240865/prof/s180x180/foody-mobile-13332781_10249056975-437-636003056069146520.jpg"
    ),
    Restaurant(
        id = 194454,
        name = "Cây Si Cafe - Phạm Văn Đồng",
        address = "295 Phạm Văn Đồng",
        image = "https://images.foody.vn/res/g20/194454/prof/s180x180/foody-mobile-cafe-cay-si-mb-jpg-944-635929709161672030.jpg"
    ),
    Restaurant(
        id = 678684,
        name = "Chè - Súp Cua Quỳnh Như",
        address = "269 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g68/678684/prof/s180x180/foody-mobile-hmb-jpg-844-636373534623106219.jpg"
    ),
    Restaurant(
        id = 147580,
        name = "Bánh Tráng Nướng - Lê Lợi",
        address = "30 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g15/147580/prof/s180x180/foody-mobile--13-_hinhmob-jpg-850-635712738593286366.jpg"
    ),
    Restaurant(
        id = 733209,
        name = "Cream DaLat - Kem Bơ - Phạm Văn Đồng",
        address = "330 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g74/733209/prof/s180x180/foody-upload-api-foody-mobile-hmbb-jpg-180416093628.jpg"
    ),
    Restaurant(
        id = 89784,
        name = "Bánh Trung Thu Rau Câu - Nguyễn Du",
        address = "22 Nguyễn Du, P. 7",
        image = "https://images.foody.vn/res/g9/89784/prof/s180x180/foody-mobile-hmb-banh-trung-thu-r-635434506452790819.jpg"
    ),
    Restaurant(
        id = 116961,
        name = "Đồng Hương Quán",
        address = "199 Phạm Văn Đồng, P. 4",
        image = "https://images.foody.vn/res/g12/116961/prof/s180x180/foody-mobile-ujjktwch-jpg-114-635895864257931703.jpg"
    ),
    Restaurant(
        id = 279030,
        name = "Quán Messi - Kem Tự Chọn",
        address = "1096 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g28/279030/prof/s180x180/foody-mobile-4-jpg-899-636105908984534351.jpg"
    ),
    Restaurant(
        id = 84292,
        name = "Chị Ngọc - Bánh Tráng Trộn Sài Gòn",
        address = "193 Đường số 28",
        image = "https://images.foody.vn/res/g9/84292/prof/s180x180/foody-mobile-12-jpg-490-636242380948777391.jpg"
    ),
    Restaurant(
        id = 47118,
        name = "Quán Chín Nghĩa - Dương Quảng Hàm",
        address = "173/45C Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g5/47118/prof/s180x180/foody-mobile-drfg-jpg-973-635683998543260433.jpg"
    ),
    Restaurant(
        id = 718552,
        name = "Trà Sữa Tea Expert - Phan Huy Ích",
        address = "303 Phan Huy Ích, P. 14",
        image = "https://images.foody.vn/res/g72/718552/prof/s180x180/foody-upload-api-foody-mobile-3a-jpg-180312082842.jpg"
    ),
    Restaurant(
        id = 180118,
        name = "Kopapi & Juice",
        address = "95 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g19/180118/prof/s180x180/foody-mobile-mobile-jpg-411-635841589425266379.jpg"
    ),
    Restaurant(
        id = 225669,
        name = "Thanh Vân - Bánh Ướt Ban Mê",
        address = "106 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g23/225669/prof/s180x180/foody-mobile-hmb-s-jpg-574-635984058954573221.jpg"
    ),
    Restaurant(
        id = 14112,
        name = "Tường Phát - Hủ Tiếu Nam Vang - Quang Trung",
        address = "439 Quang Trung",
        image = "https://images.foody.vn/res/g2/14112/prof/s180x180/foody-mobile-9-jpg-230-635983830099604288.jpg"
    ),
    Restaurant(
        id = 195230,
        name = "Happy Cafecinema ",
        address = "1144/18 Lê Đức Thọ, P. 13",
        image = "https://images.foody.vn/res/g20/195230/prof/s180x180/foody-mobile-hmb-s-jpg-759-635856807783708070.jpg"
    ),
    Restaurant(
        id = 762015,
        name = "Sushi - Lunch Town - Shop Online",
        address = "294/40/16 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g77/762015/prof/s180x180/foody-upload-api-foody-mobile-1-jpg-180830094853.jpg"
    ),
    Restaurant(
        id = 133148,
        name = "Nướng Ngói Sài Gòn Chi Nhánh 2 - Phạm Văn Đồng",
        address = "201 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g14/133148/prof/s180x180/foody-mobile-hmb-f-jpg-780-635780142859327007.jpg"
    ),
    Restaurant(
        id = 282092,
        name = "Mì Cay Singa",
        address = "699 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g29/282092/prof/s180x180/foody-mobile-3l-jpg-697-636113421742045221.jpg"
    ),
    Restaurant(
        id = 197899,
        name = "Nhà Hàng Thoáng Việt - Quang Trung",
        address = "1 Quang Trung",
        image = "https://images.foody.vn/res/g20/197899/prof/s180x180/foody-mobile-r-jpg-647-635864585901442644.jpg"
    ),
    Restaurant(
        id = 708583,
        name = "Kap Kap - Ẩm Thực Hoa & Cafe",
        address = "368 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g71/708583/prof/s180x180/foody-upload-api-foody-mobile-123-190515093949.jpg"
    ),
    Restaurant(
        id = 182988,
        name = "Bánh Mì Xối Mỡ Mr. Beo",
        address = "327 Lê Văn Thọ",
        image = "https://images.foody.vn/res/g19/182988/prof/s180x180/foody-mobile-t2-jpg-929-635823133513538135.jpg"
    ),
    Restaurant(
        id = 681149,
        name = "Trà Sữa Cú Mèo - Cây Trâm",
        address = "544 Cây Trâm",
        image = "https://images.foody.vn/res/g69/681149/prof/s180x180/foody-mobile-t7-jpg-696-636384146403688815.jpg"
    ),
    Restaurant(
        id = 281550,
        name = "Delimanjoo - Bánh Bắp Kem Bơ",
        address = "945/4 Quang Trung, P. 14",
        image = "https://images.foody.vn/res/g29/281550/prof/s180x180/foody-mobile-10-jpg-984-636110888499147875.jpg"
    ),
    Restaurant(
        id = 729740,
        name = "Đạt Lý - Bánh Bột Lọc",
        address = "548/5 Nguyễn Thái Sơn, P. 5",
        image = "https://images.foody.vn/res/g73/729740/prof/s180x180/foody-upload-api-foody-mobile-foody-banh-bot-loc-d-180407084437.jpg"
    ),
    Restaurant(
        id = 216569,
        name = "Lẩu Công Chúa - Vincom Gò Vấp",
        address = "Tầng 4 Vincom Gò Vấp, 12 Phan Văn Trị",
        image = "https://images.foody.vn/res/g22/216569/prof/s180x180/foody-mobile-hmb-s-jpg-729-635931343718190976.jpg"
    ),
    Restaurant(
        id = 772531,
        name = "Lyn Tây - Kem, Sinh Tố & Cafe",
        address = "40 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g78/772531/prof/s180x180/foody-upload-api-foody-mobile-foody-siam-juice-cre-180825162935.jpg"
    ),
    Restaurant(
        id = 92353,
        name = "Mì Vịt Tiềm Chí Phát - Nguyễn Văn Nghi",
        address = "402 Nguyễn Văn Nghi, P. 7",
        image = "https://images.foody.vn/res/g10/92353/prof/s180x180/foody-mobile-hmb-1-jpg-635458534093774182.jpg"
    ),
    Restaurant(
        id = 894224,
        name = "Bún Quậy Phú Quốc - Phan Văn Trị",
        address = "736 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g90/894224/prof/s180x180/foody-upload-api-foody-mobile-3-190314110251.jpg"
    ),
    Restaurant(
        id = 66866,
        name = "Lẩu Dê Đoàn Râu",
        address = "95 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g7/66866/prof/s180x180/foody-mobile-lau-de-doan-rau-tp-hcm-140318113837.jpg"
    ),
    Restaurant(
        id = 723771,
        name = "Thế Giới Ăn Vặt - Thống Nhất",
        address = "362/21/23 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g73/723771/prof/s180x180/foody-upload-api-foody-mobile-tgav-jpg-180319144516.jpg"
    ),
    Restaurant(
        id = 703858,
        name = "May Coffee House",
        address = "395 Phan Huy Ích, P. 14",
        image = "https://images.foody.vn/res/g71/703858/prof/s180x180/foody-mobile-dfbdfb-jpg.jpg"
    ),
    Restaurant(
        id = 31292,
        name = "Núi Ngự - Đặc Sản Ba Miền",
        address = "147/5 Đường số 23",
        image = "https://images.foody.vn/res/g4/31292/prof/s180x180/foody-mobile-nui-ngu-jpg-982-636143670716714118.jpg"
    ),
    Restaurant(
        id = 124822,
        name = "Quán Ốc Trần Đào",
        address = "891/79 Nguyễn Kiệm",
        image = "https://images.foody.vn/res/g13/124822/prof/s180x180/foody-mobile-oc-dao-jpg-466-636143224333450878.jpg"
    ),
    Restaurant(
        id = 128665,
        name = "Nướng 274 - Phạm Văn Đồng",
        address = "274 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g13/128665/prof/s180x180/foody-mobile-nuong-274-jpg-364-635867284403698313.jpg"
    ),
    Restaurant(
        id = 657221,
        name = "NP Quán - Các Món Ăn Vặt",
        address = "369 Lê Văn Thọ, P. 9",
        image = "https://images.foody.vn/res/g66/657221/prof/s180x180/foody-mobile-foody-khuc-bach-than-783-636299248603476191.jpg"
    ),
    Restaurant(
        id = 707845,
        name = "Quán Ăn 2H - Đặc Sản Nha Trang",
        address = "375 Lê Văn Thọ, P. 9",
        image = "https://images.foody.vn/res/g71/707845/prof/s180x180/foody-upload-api-foody-mobile-8-jpg-180820101454.jpg"
    ),
    Restaurant(
        id = 158966,
        name = "Bánh Xèo Cô Nguyệt",
        address = "774 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g16/158966/prof/s180x180/foody-mobile-ic7ryj1k-jpg-532-636404851407055991.jpg"
    ),
    Restaurant(
        id = 724262,
        name = "Bánh Xèo Cô Dung",
        address = "40/56 Lê Thị Hồng, P. 17",
        image = "https://images.foody.vn/res/g73/724262/prof/s180x180/foody-upload-api-foody-mobile-3-jpg-180330151703.jpg"
    ),
    Restaurant(
        id = 1434,
        name = "Salado Cafe - Sân Vườn & Máy Lạnh",
        address = "58 Đường Số 5, Khu Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g1/1434/prof/s180x180/foody-mobile-salado-cafe.jpg"
    ),
    Restaurant(
        id = 713539,
        name = "Bột Chiên - Đường Số 7",
        address = "36 Đường Số 7, P. 3",
        image = "https://images.foody.vn/res/g72/713539/prof/s180x180/foody-mobile-sdc-jpg.jpg"
    ),
    Restaurant(
        id = 32775,
        name = "Quán Ốc Út Nga",
        address = "9/13 Đường Số 11 , P. 11 ",
        image = "https://images.foody.vn/res/g4/32775/prof/s180x180/foody-mobile-o05v74th-jpg-915-636159279432629918.jpg"
    ),
    Restaurant(
        id = 744924,
        name = "Đốm - Milk Tea & Coffee",
        address = "672A47 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g75/744924/prof/s180x180/foody-upload-api-foody-mobile-26-jpg-180625160454.jpg"
    ),
    Restaurant(
        id = 71848,
        name = "Xe Bánh Bèo Chén & Bánh Đập Vỉa Hè",
        address = "Góc Ngã 4 Lê Lợi & Lê Lai",
        image = "https://images.foody.vn/res/g8/71848/prof/s180x180/foody-mobile-hmb-bb-jpg-252-635775884912703772.jpg"
    ),
    Restaurant(
        id = 30039,
        name = "Lẩu Bò 9 Ký",
        address = "117/2 Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g4/30039/prof/s180x180/foody-mobile-jtnsquqk-jpg-112-636126368399932191.jpg"
    ),
    Restaurant(
        id = 187359,
        name = "Mì Cay Naga Viper 7 Cấp Độ",
        address = "140 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g19/187359/prof/s180x180/foody-mobile-810hhzte-jpg-537-636143696531206704.jpg"
    ),
    Restaurant(
        id = 3111,
        name = "Biz Up Coffee Ice  Blended",
        address = "29 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g1/3111/prof/s180x180/foody-mobile-15-jpg-177-635769622200633234.jpg"
    ),
    Restaurant(
        id = 50012,
        name = "Bánh Xèo Tài Tử - Gò Vấp",
        address = " Lý Thường Kiệt",
        image = "https://images.foody.vn/res/g6/50012/prof/s180x180/foody-mobile-banh-xeo-tai-tu-go-vap-tp-hcm-140220102246.jpg"
    ),
    Restaurant(
        id = 139220,
        name = "Fins Coffee - Đường Số 10",
        address = "20 Đường Số 10, P. 9",
        image = "https://images.foody.vn/res/g14/139220/prof/s180x180/foody-mobile-untitled-1-jpg-501-635846727872610425.jpg"
    ),
    Restaurant(
        id = 225792,
        name = "Bánh Canh Thống Nhất",
        address = "17 Lê Lợi",
        image = "https://images.foody.vn/res/g23/225792/prof/s180x180/foody-upload-api-foody-mobile-9-jpg-180409153221.jpg"
    ),
    Restaurant(
        id = 138592,
        name = "Nhà Hàng Vườn Cau 2 - Nguyễn Thái Sơn",
        address = "171D Nguyễn Thái Sơn",
        image = "https://images.foody.vn/res/g14/138592/prof/s180x180/foody-mobile-t-1-jpg-398-635681507653345416.jpg"
    ),
    Restaurant(
        id = 42457,
        name = "Phở Trang - Nguyễn Oanh",
        address = "391 Nguyễn Oanh",
        image = "https://images.foody.vn/res/g5/42457/prof/s180x180/foody-mobile-o1-jpg-599-635763670538144437.jpg"
    ),
    Restaurant(
        id = 900641,
        name = "Cơm Tấm Kuru",
        address = "305 Phạm Văn Đồng, P. 1",
        image = "https://images.foody.vn/res/g91/900641/prof/s180x180/foody-upload-api-foody-mobile-2-190403095049.jpg"
    ),
    Restaurant(
        id = 802969,
        name = "Bún Đậu Mẹt 128",
        address = "128 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g81/802969/prof/s180x180/foody-upload-api-foody-mobile-hmbb-jpg-181119105848.jpg"
    ),
    Restaurant(
        id = 277480,
        name = "Hoa Ly - Trà Sữa Nhà Làm - Phan Văn Trị",
        address = "637 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g28/277480/prof/s180x180/foody-mobile-t1-jpg-248-636099904976969733.jpg"
    ),
    Restaurant(
        id = 168242,
        name = "Trà Đào 168",
        address = "168 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g17/168242/prof/s180x180/foody-mobile-untitled-1-jpg-945-635780870401080867.jpg"
    ),
    Restaurant(
        id = 79642,
        name = "Cơm Gà Xối Mỡ SuSu",
        address = "440/65 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g8/79642/prof/s180x180/foody-mobile-com-ga-xoi-mo-susu-tp-hcm-140611092417.jpg"
    ),
    Restaurant(
        id = 262620,
        name = "Kim Chi Fc - Chuyên Các Món Hàn Quốc",
        address = "157 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g27/262620/prof/s180x180/foody-mobile-8-jpg-586-636060020594306962.jpg"
    ),
    Restaurant(
        id = 85581,
        name = "Bánh Cóng Sóc Trăng",
        address = "8 Lý Thường Kiệt,  P. 8 ",
        image = "https://images.foody.vn/res/g9/85581/prof/s180x180/foody-mobile-banh-cong-nhan-pate-tp-hcm-140707022816.jpg"
    ),
    Restaurant(
        id = 24585,
        name = "Minh Hằng Bakery",
        address = "226 Nguyễn Thượng Hiền, P. 1",
        image = "https://images.foody.vn/res/g3/24585/prof/s180x180/foody-mobile-hmb-su-jpg-137-635611484233856168.jpg"
    ),
    Restaurant(
        id = 198787,
        name = "Hà Thúy Sài Gòn - Cơm Cháy Chà Bông Gia Truyền - Phan Văn Trị - Shop Online",
        address = "551/88 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g20/198787/prof/s180x180/foody-mobile-47-jpg-475-635868973853728423.jpg"
    ),
    Restaurant(
        id = 157840,
        name = "Quán Đồng Quê 2A - Quang Trung",
        address = "16 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g16/157840/prof/s180x180/foody-mobile-t-1-jpg-780-635744555065731054.jpg"
    ),
    Restaurant(
        id = 141727,
        name = "YOLO Beer & Barbecue",
        address = "543/1 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g15/141727/prof/s180x180/foody-mobile-yolo-beer-mb-jpg-434-635839609079023675.jpg"
    ),
    Restaurant(
        id = 102771,
        name = "Cơm Tấm Loan - Nguyễn Thái Sơn",
        address = "346 Nguyễn Thái Sơn",
        image = "https://images.foody.vn/res/g11/102771/prof/s180x180/foody-mobile-mobile-jpg-273-636192952803833507.jpg"
    ),
    Restaurant(
        id = 783069,
        name = "Xuân Đức - Lẩu Cua Đồng",
        address = "106 Đường Số 1, Khu Dân Cư Cityland Center Hills, P. 7",
        image = "https://images.foody.vn/res/g79/783069/prof/s180x180/foody-upload-api-foody-mobile-30a-jpg-181001151340.jpg"
    ),
    Restaurant(
        id = 741810,
        name = "Alô Cú Đêm - Ăn Đêm Online",
        address = "590/2/10 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g75/741810/prof/s180x180/foody-upload-api-foody-mobile-hmbbd-jpg-180516153516.jpg"
    ),
    Restaurant(
        id = 653237,
        name = "Bún Vịt Miến Gà - Ngon Nhất Quán",
        address = "1271 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g66/653237/prof/s180x180/foody-mobile-hmbbd-jpg-457-636282993901178955.jpg"
    ),
    Restaurant(
        id = 746125,
        name = "TO's Tea & Food - Thiên Đường Trà Sữa Nhật - Lê Đức Thọ",
        address = "996 Lê Đức Thọ (Đối Diện Chợ Xóm Mới), P. 13",
        image = "https://images.foody.vn/res/g75/746125/prof/s180x180/foody-upload-api-foody-mobile-6-jpg-180614100842.jpg"
    ),
    Restaurant(
        id = 646398,
        name = "3T - Trà Sữa",
        address = "Hẻm 904 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g65/646398/prof/s180x180/foody-mobile-t1-jpg-847-636262083036898539.jpg"
    ),
    Restaurant(
        id = 210782,
        name = "Bún Đậu Mắm Tôm Hà Nội - Thống Nhất",
        address = "51 Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g22/210782/prof/s180x180/foody-mobile-t1-jpg-904-635912336738501424.jpg"
    ),
    Restaurant(
        id = 76914,
        name = "Delizia Coffee",
        address = "7 Phạm Ngũ Lão, P. 3",
        image = "https://images.foody.vn/res/g8/76914/prof/s180x180/foody-mobile-delizia-coffee-tp-hcm-140529091350.jpg"
    ),
    Restaurant(
        id = 128659,
        name = "Ốc 10 Em",
        address = "50 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g13/128659/prof/s180x180/foody-mobile-xf-jpg-561-635635628005174813.jpg"
    ),
    Restaurant(
        id = 256201,
        name = "Ẩm Thực Chay Đỉnh Bồ Đề",
        address = "304 Nguyễn Thái Sơn, P. 4",
        image = "https://images.foody.vn/res/g26/256201/prof/s180x180/foody-mobile-c2-jpg-744-636040880076123532.jpg"
    ),
    Restaurant(
        id = 233002,
        name = "Canh Bún & Bún Riêu Ốc",
        address = "656/18 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g24/233002/prof/s180x180/foody-mobile-foody-bun-rieu-oc-23-125-635980385937200837.jpg"
    ),
    Restaurant(
        id = 776030,
        name = "K-Pop Chicken Restaurant",
        address = "37 Phạm Văn Đồng, P. 3",
        image = "https://images.foody.vn/res/g78/776030/prof/s180x180/foody-upload-api-foody-mobile-hmkp-jpg-180906111157.jpg"
    ),
    Restaurant(
        id = 168452,
        name = "Oxy - Trà Sữa",
        address = "165 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g17/168452/prof/s180x180/foody-upload-api-foody-mobile-31-jpg-181214141817.jpg"
    ),
    Restaurant(
        id = 244526,
        name = "Quán Ba Cư - Quán Ăn Gia Đình",
        address = "1 Chung Cư K26, Dương Quảng Hàm",
        image = "https://images.foody.vn/res/g25/244526/prof/s180x180/foody-upload-api-foody-mobile-3-jpg-180508102931.jpg"
    ),
    Restaurant(
        id = 785898,
        name = "Cơm Gà Xối Mỡ 365 - Đặc Biệt Các Loại Sốt",
        address = "365 Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g79/785898/prof/s180x180/foody-upload-api-foody-mobile-25b-jpg-181017095047.jpg"
    ),
    Restaurant(
        id = 646949,
        name = "Mì Cay Singa",
        address = "1457 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g65/646949/prof/s180x180/foody-mobile-hmbbs-jpg-410-636262200095013597.jpg"
    ),
    Restaurant(
        id = 227739,
        name = "Ngọc Thái - Các Loại Chè",
        address = "162 Nguyễn Văn Khối",
        image = "https://images.foody.vn/res/g23/227739/prof/s180x180/foody-mobile-foody-che-thai-ha-to-394-635961385906945973.jpg"
    ),
    Restaurant(
        id = 1969,
        name = "Sao 2 Cafe - Lê Lợi",
        address = "79 Lê Lợi, P. 4",
        image = "https://images.foody.vn/res/g1/1969/prof/s180x180/foody-mobile-sao-2-cafe-le-loi.jpg"
    ),
    Restaurant(
        id = 86994,
        name = "Quốc Anh Ký - Quán Cơm",
        address = "585 Lê Quang Định, P. 1",
        image = "https://images.foody.vn/res/g9/86994/prof/s180x180/foody-mobile-hmb-f-jpg-908-635780188460859102.jpg"
    ),
    Restaurant(
        id = 165243,
        name = "BUD'S Ice Cream - Vincom Quang Trung",
        address = "Tầng B1 Vincom Quang Trung, 190 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g17/165243/prof/s180x180/foody-mobile-hmb-kem-jpg-684-635768969498138823.jpg"
    ),
    Restaurant(
        id = 198048,
        name = "Mahalo Beer Club - Quang Trung",
        address = "97 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g20/198048/prof/s180x180/foody-mobile-vinalo-1423450460c37-327-635864877923035553.jpg"
    ),
    Restaurant(
        id = 914781,
        name = "Avatar Coffee & Tea - Nguyễn Văn Nghi",
        address = "27 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g92/914781/prof/s180x180/foody-upload-api-foody-mobile-hjhjjh-190529101415.jpg"
    ),
    Restaurant(
        id = 728663,
        name = "Swiss Yogurt VietNam - Sữa Chua Uống Vị Trái Cây",
        address = "416/25 Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g73/728663/prof/s180x180/foody-upload-api-foody-mobile-20-jpg-180820123409.jpg"
    ),
    Restaurant(
        id = 77601,
        name = "Khu Ẩm Thực - Chợ Gò Vấp",
        address = "197 Nguyễn Thái Sơn, P. 4",
        image = "https://images.foody.vn/res/g8/77601/prof/s180x180/foody-mobile-mi-xao-cho-go-vap-tp-hcm-140603093239.jpg"
    ),
    Restaurant(
        id = 78170,
        name = "New Town Cafe - Phạm Văn Đồng",
        address = "226 Phạm Văn Đồng",
        image = "https://images.foody.vn/res/g8/78170/prof/s180x180/foody-mobile-31pjp7xp-jpg-544-636173040922508723.jpg"
    ),
    Restaurant(
        id = 267985,
        name = "Bún Chả Hà Nội Xưa - Phạm Văn Bạch",
        address = "973 Phạm Văn Bạch, P. 12",
        image = "https://images.foody.vn/res/g27/267985/prof/s180x180/foody-mobile-13173787_10327498601-142-636072023298815466.jpg"
    ),
    Restaurant(
        id = 29203,
        name = "Rio Cafe - Lê Đức Thọ",
        address = "57/29 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g3/29203/prof/s180x180/foody-upload-api-foody-mobile-rio-jpg-181211144725.jpg"
    ),
    Restaurant(
        id = 313128,
        name = "Trà Sữa Blue Cup",
        address = "62 Lê Lai, P. 4",
        image = "https://images.foody.vn/res/g32/313128/prof/s180x180/foody-mobile-1-jpg-333-636179276478510187.jpg"
    ),
    Restaurant(
        id = 217749,
        name = "Quán Nướng Út Thảo ",
        address = "341 Phạm Văn Đồng, P. 11",
        image = "https://images.foody.vn/res/g22/217749/prof/s180x180/foody-mobile-po-jpg-388-635939074591447023.jpg"
    ),
    Restaurant(
        id = 783527,
        name = "The Little Bistro - Food & Drinks",
        address = "Tầng 1 - 2, 25 - 27 Trần Thị Nghỉ, P. 7",
        image = "https://images.foody.vn/res/g79/783527/prof/s180x180/foody-upload-api-foody-mobile-hmnnn-jpg-181002170621.jpg"
    ),
    Restaurant(
        id = 121840,
        name = "AEON Citimart Bakery",
        address = "672 Quang Trung",
        image = "https://images.foody.vn/res/g13/121840/prof/s180x180/foody-mobile-hmb-bakery-aeon-jpg-776-635593392923733812.jpg"
    ),
    Restaurant(
        id = 17893,
        name = "Garden Hills Coffee",
        address = "360 Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g2/17893/prof/s180x180/foody-mobile-fgh-jpg.jpg"
    ),
    Restaurant(
        id = 152796,
        name = "Chè Mè Đen & Sữa Hạt Sen Cô Hoa - Huỳnh Khương An",
        address = "137 Huỳnh Khương An, P. 5",
        image = "https://images.foody.vn/res/g16/152796/prof/s180x180/foody-mobile-12-jpg-245-635729839869935265.jpg"
    ),
    Restaurant(
        id = 745775,
        name = "Trà Sữa Dế Cơm",
        address = "43 Thống Nhất",
        image = "https://images.foody.vn/res/g75/745775/prof/s180x180/foody-upload-api-foody-mobile-2-0838-jpg-180602090852.jpg"
    ),
    Restaurant(
        id = 763967,
        name = "The AOH - Trà Sữa",
        address = "371 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g77/763967/prof/s180x180/foody-upload-api-foody-mobile-aoh-avt-jpg-181221180616.jpg"
    ),
    Restaurant(
        id = 146287,
        name = "Trà Sữa & Cafe H2t - Phan Văn Trị",
        address = "710/9 Phan Văn Trị, P. 10 ",
        image = "https://images.foody.vn/res/g15/146287/prof/s180x180/foody-mobile-untitled-1-jpg-896-635748229205556339.jpg"
    ),
    Restaurant(
        id = 79971,
        name = "Ốc Bà Chiểu 2",
        address = "Lý Thường Kiệt, P. 7",
        image = "https://images.foody.vn/res/g8/79971/prof/s180x180/foody-mobile-oc-ba-chieu-2-ly-thuong-kiet-tp-hcm-140609030907.jpeg"
    ),
    Restaurant(
        id = 264916,
        name = "Ngọc Bảo - Lẩu Đuôi Bò",
        address = "1 Trưng Nữ Vương, P. 4",
        image = "https://images.foody.vn/res/g27/264916/prof/s180x180/foody-mobile-xztrm1nj-jpg-799-636262270526951791.jpg"
    ),
    Restaurant(
        id = 131113,
        name = "Quán Bo - Lê Đức Thọ",
        address = "23/19 Lê Đức Thọ, P. 13",
        image = "https://images.foody.vn/res/g14/131113/prof/s180x180/foody-mobile-hmb-mam-jpg-165-635648580938151086.jpg"
    ),
    Restaurant(
        id = 216962,
        name = "Cô Anh - Bánh Xèo Miền Trung",
        address = "176 Cây Trâm",
        image = "https://images.foody.vn/res/g22/216962/prof/s180x180/foody-mobile-foody-banh-xeo-mien--659-635930236461318182.jpg"
    ),
    Restaurant(
        id = 243110,
        name = "Trà Sữa The Moon",
        address = "185 Lê Văn Thọ, P. 8 ",
        image = "https://images.foody.vn/res/g25/243110/prof/s180x180/foody-mobile-36987-jpg-752-636259434302851295.jpg"
    ),
    Restaurant(
        id = 692049,
        name = "Bún Đậu Mắm Tôm Phố Cổ",
        address = "1157 Phan Văn Trị",
        image = "https://images.foody.vn/res/g70/692049/prof/s180x180/foody-mobile-3-jpg.jpg"
    ),
    Restaurant(
        id = 662197,
        name = "Văn Nghệ - Quán Bánh Khọt",
        address = "8 Lý Thường Kiệt",
        image = "https://images.foody.vn/res/g67/662197/prof/s180x180/foody-mobile-sdasd-jpg-163-636314050912377538.jpg"
    ),
    Restaurant(
        id = 773534,
        name = "Món Ngon Xứ Quảng - Các Bà Dì",
        address = "6 Lê Đức Thọ, P. 7",
        image = "https://images.foody.vn/res/g78/773534/prof/s180x180/foody-upload-api-foody-mobile-1-jpg-181017172429.jpg"
    ),
    Restaurant(
        id = 710044,
        name = "Cachi Tea - Huỳnh Khương An",
        address = "66 Huỳnh Khương An, P. 5",
        image = "https://images.foody.vn/res/g72/710044/prof/s180x180/foody-upload-api-foody-mobile-2-jpg-180913084853.jpg"
    ),
    Restaurant(
        id = 147404,
        name = "Cháo Ếch Sư Phụ Singapore - Quang Trung",
        address = "143 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g15/147404/prof/s180x180/foody-mobile--20-_hinhmob-jpg-581-635711966218063586.jpg"
    ),
    Restaurant(
        id = 82729,
        name = "Phở 10 Lý Quốc Sư - Lê Đức Thọ",
        address = "68 Lê Đức Thọ",
        image = "https://images.foody.vn/res/g9/82729/prof/s180x180/foody-mobile-lpjz0giz-jpg-300-636346745712460087.jpg"
    ),
    Restaurant(
        id = 630168,
        name = "Phở Hùng Ngân - Lê Đức Thọ",
        address = "2 Lê Đức Thọ",
        image = "https://images.foody.vn/res/g64/630168/prof/s180x180/foody-mobile-13697094_59390290077-499-636207620462054455.jpg"
    ),
    Restaurant(
        id = 98919,
        name = "Ẩm Thực 45 - Quang Trung",
        address = "97 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g10/98919/prof/s180x180/foody-mobile-hmb-lau-ca-jpg-175-635501087558618201.jpg"
    ),
    Restaurant(
        id = 47681,
        name = "Bánh Cuốn Nóng Thuỳ Linh",
        address = "150 Nguyễn Văn Nghi",
        image = "https://images.foody.vn/res/g5/47681/prof/s180x180/foody-mobile-ee0etmex-jpg-332-636197401453340388.jpg"
    ),
    Restaurant(
        id = 310140,
        name = "Lốc Coffee - Acoustic",
        address = "338 Nguyễn Văn Lượng, P. 16",
        image = "https://images.foody.vn/res/g32/310140/prof/s180x180/foody-mobile-1-jpg-339-636167233007809676.jpg"
    ),
    Restaurant(
        id = 53589,
        name = "Con Heo Cười - Da Heo Chiên Giòn - Giao Hàng Tận Nơi",
        address = "819/1 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g6/53589/prof/s180x180/foody-mobile-36-jpg-857-636325417545968998.jpg"
    ),
    Restaurant(
        id = 243368,
        name = "Stir Crazy - Mongolian Grill/BBQ",
        address = "890 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g25/243368/prof/s180x180/foody-mobile-wa9x98f1-jpg-460-636027099847602421.jpg"
    ),
    Restaurant(
        id = 720957,
        name = "Bún Chả & Nem Cua Bể Hà Nội - Phan Văn Trị",
        address = "177 Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g73/720957/prof/s180x180/foody-mobile-kj-jpg.jpg"
    ),
    Restaurant(
        id = 98256,
        name = "Ốc Bầu ",
        address = "Hẻm 293 Lê Văn Thọ",
        image = "https://images.foody.vn/res/g10/98256/prof/s180x180/foody-mobile-gou-jpg-671-635495826672430821.jpg"
    ),
    Restaurant(
        id = 763311,
        name = "Dumi Food & Cafe - Quang Trung",
        address = "777 Quang Trung, P. 12",
        image = "https://images.foody.vn/res/g77/763311/prof/s180x180/foody-upload-api-foody-mobile-6-190103151548.jpg"
    ),
    Restaurant(
        id = 710341,
        name = "Bánh Crepe Sầu Riêng - Gò Vấp",
        address = "Chung Cư Dream Home 2, Đường Số 59, P. 14",
        image = "https://images.foody.vn/res/g72/710341/prof/s180x180/foody-upload-api-foody-mobile-53-jpg-180709112058.jpg"
    ),
    Restaurant(
        id = 804177,
        name = "Cơm Gà Hồng Kông - Phạm Ngũ Lão",
        address = "182 Phạm Ngũ Lão, P. 7",
        image = "https://images.foody.vn/res/g81/804177/prof/s180x180/foody-upload-api-foody-mobile--l-l-190607135146.jpg"
    ),
    Restaurant(
        id = 702786,
        name = "Phở Bò Tơ Củ Chi A. Dũng",
        address = "89 Nguyễn Thái Sơn",
        image = "https://images.foody.vn/res/g71/702786/prof/s180x180/foody-mobile-t7-jpg.jpg"
    ),
    Restaurant(
        id = 749516,
        name = "Cá Biển Nha Trang 2",
        address = "Đối Diện 372 Dương Quảng Hàm, Chung Cư K26 Dương Quảng Hàm, P. 7",
        image = "https://images.foody.vn/res/g75/749516/prof/s180x180/foody-upload-api-foody-mobile-hhh-jpg-180611145137.jpg"
    ),
    Restaurant(
        id = 208029,
        name = "Lotus Coffee",
        address = "498/1/3 Lê Quang Định, Phường 1",
        image = "https://images.foody.vn/res/g21/208029/prof/s180x180/foody-mobile-foody-lotus-coffee-2-332-635894123506410235.jpg"
    ),
    Restaurant(
        id = 225171,
        name = "Canteen - Cafe & Giải Khát - Phạm Ngũ Lão",
        address = "5B Phạm Ngũ Lão, P. 3",
        image = "https://images.foody.vn/res/g23/225171/prof/s180x180/foody-mobile-cantin-jpg-150-636143025483991095.jpg"
    ),
    Restaurant(
        id = 654010,
        name = "Gỗ Mục Coffee",
        address = "102/18 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g66/654010/prof/s180x180/foody-mobile-17523556_23807755666-503-636286391027193599.jpg"
    ),
    Restaurant(
        id = 104767,
        name = "Lẩu 38 - Đường Số 11",
        address = "38 Đường Số 11, P. 11",
        image = "https://images.foody.vn/res/g11/104767/prof/s180x180/foody-mobile-hmb-lau-111-jpg-365-635533004901486903.jpg"
    ),
    Restaurant(
        id = 774945,
        name = "Trà Sữa Wozzi",
        address = "41 Nguyễn Văn Nghi, P. 4",
        image = "https://images.foody.vn/res/g78/774945/prof/s180x180/foody-upload-api-foody-mobile-foody-tra-sua-wozzi--180904112359.jpg"
    ),
    Restaurant(
        id = 300651,
        name = "My Coffee",
        address = "Hẻm 452 Phan Văn Trị",
        image = "https://images.foody.vn/res/g31/300651/prof/s180x180/foody-mobile-untitled-1-jpg-841-636177347369008385.jpg"
    ),
    Restaurant(
        id = 742610,
        name = "A Đạt - Bánh Xèo Rau Rừng - Không Chi Nhánh",
        address = "1 Phạm Văn Đồng",
        image = "https://images.foody.vn/res/g75/742610/prof/s180x180/foody-upload-api-foody-mobile-12-jpg-180821090631.jpg"
    ),
    Restaurant(
        id = 232070,
        name = "Nem Nướng Thanh Hùng",
        address = "151 Đường Số 11, P. 11",
        image = "https://images.foody.vn/res/g24/232070/prof/s180x180/foody-mobile-hmb-s-jpg-445-635975409471218745.jpg"
    ),
    Restaurant(
        id = 215011,
        name = "Tàu Hũ Orisoy - Vincom Plaza Gò Vấp",
        address = "Tầng 4 - Vincom Plaza Gò Vấp, 12 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g22/215011/prof/s180x180/foody-mobile-vincom-jpg-699-636402063479985518.jpg"
    ),
    Restaurant(
        id = 49623,
        name = "Sweet Home Bakery - Quang Trung",
        address = "294 Quang Trung. P. 10",
        image = "https://images.foody.vn/res/g5/49623/prof/s180x180/foody-mobile-sweet-home-bakery-quang-trung-tp-hcm-140215022837.jpg"
    ),
    Restaurant(
        id = 281591,
        name = "Vitamin House - Quang Trung",
        address = "330 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g29/281591/prof/s180x180/foody-mobile-hmb-jpg-961-636111055737505634.jpg"
    ),
    Restaurant(
        id = 126815,
        name = "My Life - Quán Bò Né",
        address = "736 Phạm Văn Bạch ,P. 12",
        image = "https://images.foody.vn/res/g13/126815/prof/s180x180/foody-mobile-t-2-jpg-980-635627005972827781.jpg"
    ),
    Restaurant(
        id = 9164,
        name = "Quán Sáu Cua - Hải Sản Tươi Sống",
        address = "125/4 Đường Số 11 , P. 11",
        image = "https://images.foody.vn/res/g1/9164/prof/s180x180/foody-upload-api-foody-mobile-123-jpg-181217164910.jpg"
    ),
    Restaurant(
        id = 635599,
        name = "Rice Club Gò Vấp",
        address = "Tầng Trệt Siêu Thị AeonCitimart Quang Trung, 672 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g64/635599/prof/s180x180/foody-mobile-3-jpg-224-636232654102071659.jpg"
    ),
    Restaurant(
        id = 286061,
        name = "Bánh Bèo Hai Quân - Lê Văn Thọ",
        address = "417/53 Lê Văn Thọ, P. 9",
        image = "https://images.foody.vn/res/g29/286061/prof/s180x180/foody-upload-api-foody-mobile-10-jpg-180717115906.jpg"
    ),
    Restaurant(
        id = 789285,
        name = "Chú Nhiều 444 - Hủ Tiếu Mì & Sủi Cảo",
        address = "891/91 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g79/789285/prof/s180x180/foody-upload-api-foody-mobile-9-jpg-181031140631.jpg"
    ),
    Restaurant(
        id = 3685,
        name = "Gốc Cây Cafe",
        address = "94/1055 Dương Quảng Hàm, P. 6",
        image = "https://images.foody.vn/res/g1/3685/prof/s180x180/foody-mobile-goc-cay-cafe.jpg"
    ),
    Restaurant(
        id = 137182,
        name = "Quán Ăn Gia Đình Bạn Tôi - Nguyễn Thượng Hiền",
        address = "36 Nguyễn Thượng Hiền, P. 1",
        image = "https://images.foody.vn/res/g14/137182/prof/s180x180/foody-mobile-mobile-jpg-355-635772353922860490.jpg"
    ),
    Restaurant(
        id = 317333,
        name = "Quán Bo - Bún Mắm & Bánh Canh Cua",
        address = "1196 Lê Đức Thọ",
        image = "https://images.foody.vn/res/g32/317333/prof/s180x180/foody-mobile-2-jpg-644-636191383423569990.jpg"
    ),
    Restaurant(
        id = 208114,
        name = "Quán Lâm Ký - Bột Chiên",
        address = "652 Lê Quang Định, P. 1",
        image = "https://images.foody.vn/res/g21/208114/prof/s180x180/foody-mobile-foody-bot-chien-lam--835-635894817603421352.jpg"
    ),
    Restaurant(
        id = 788582,
        name = "Đá Bào Kidi",
        address = "70 Nguyễn Tư Giản, P. 12",
        image = "https://images.foody.vn/res/g79/788582/prof/s180x180/foody-upload-api-foody-mobile-1-gif-181019164545.jpg"
    ),
    Restaurant(
        id = 862793,
        name = "Ding Tea - Vincom Plaza Gò Vấp",
        address = "Tầng 4 Vincom Plaza Gò Vấp, 12 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g87/862793/prof/s180x180/foody-upload-api-foody-mobile-hddd-jpg-181211160419.jpg"
    ),
    Restaurant(
        id = 736240,
        name = "Cô Ba Sài Gòn - Bún Thịt Nướng & Cơm Thịt Xiên - Lê Lai",
        address = "79 Lê Lai, P. 3",
        image = "https://images.foody.vn/res/g74/736240/prof/s180x180/foody-upload-api-foody-mobile-25-jpg-180907162800.jpg"
    ),
    Restaurant(
        id = 874849,
        name = "Quán Của Má - Nem Nướng & Đặc Sản Nha Trang - Trần Thị Nghỉ",
        address = "144 Trần Thị Nghỉ, P. 7",
        image = "https://images.foody.vn/res/g88/874849/prof/s180x180/foody-upload-api-foody-mobile-hddd-jpg-190102114820.jpg"
    ),
    Restaurant(
        id = 184670,
        name = "Trái Cây Tô Sốt Dừa Fruta",
        address = "Đối Diện Số 93 Bùi Quang Là",
        image = "https://images.foody.vn/res/g19/184670/prof/s180x180/foody-mobile-trai-cay-to-jpg-353-635864215057903292.jpg"
    ),
    Restaurant(
        id = 228077,
        name = "Lê Uyên - Trà Sữa Thạch Nhà Làm",
        address = "497 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g23/228077/prof/s180x180/foody-mobile-c2-jpg-269-635962229479969754.jpg"
    ),
    Restaurant(
        id = 710782,
        name = "Quán Lươn Củ Nghệ",
        address = "440/44A Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g72/710782/prof/s180x180/foody-upload-api-foody-mobile-cach-nau-chao-luon-d-180601175256.jpg"
    ),
    Restaurant(
        id = 782040,
        name = "Al Fresco's - Phan Văn Trị",
        address = "539 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g79/782040/prof/s180x180/foody-upload-api-foody-mobile-hmkp-jpg-180927102648.jpg"
    ),
    Restaurant(
        id = 786049,
        name = "Bò Tơ Nhân Phát - Lê Quang Định",
        address = "516 Lê Quang Định, P. 1",
        image = "https://images.foody.vn/res/g79/786049/prof/s180x180/foody-upload-api-foody-mobile-fghfgh-jpg-181011110202.jpg"
    ),
    Restaurant(
        id = 670900,
        name = "Quán Đức Đà Nẵng - Mì Quảng & Bún Bò",
        address = "220/9 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g68/670900/prof/s180x180/foody-mobile-t1-jpg-643-636343553443293226.jpg"
    ),
    Restaurant(
        id = 31262,
        name = "Phúc Tam Kỳ - Mì Quảng",
        address = "297 Nguyễn Văn Lượng , P. 10",
        image = "https://images.foody.vn/res/g4/31262/prof/s180x180/foody-mobile-j3m2ft23-jpg-240-636014049290998393.jpg"
    ),
    Restaurant(
        id = 136393,
        name = "Cây Xoài - Bún Thịt Nướng & Cơm Tấm",
        address = "1238 - 1240 Quang Trung, P. 8",
        image = "https://images.foody.vn/res/g14/136393/prof/s180x180/foody-upload-api-foody-mobile-fdg-gif-180822112633.jpg"
    ),
    Restaurant(
        id = 782099,
        name = "Puppy - Trà Sữa & Coffee",
        address = "716 Lê Đức Thọ, P. 15",
        image = "https://images.foody.vn/res/g79/782099/prof/s180x180/foody-upload-api-foody-mobile-4-jpg-181008093327.jpg"
    ),
    Restaurant(
        id = 287418,
        name = "Cát Coffee",
        address = "Tầng 3, 228/4 Phan Huy Ích , P. 12",
        image = "https://images.foody.vn/res/g29/287418/prof/s180x180/foody-upload-api-foody-mobile-7-jpg-181114145704.jpg"
    ),
    Restaurant(
        id = 748777,
        name = "Trâm Anh - Bún Riêu Bạch Tuộc",
        address = "862/13 Lê Đức Thọ, P. 15",
        image = "https://images.foody.vn/res/g75/748777/prof/s180x180/foody-upload-api-foody-mobile-9-jpg-180618110933.jpg"
    ),
    Restaurant(
        id = 93522,
        name = "Bánh Canh Chả Cá Vũng Tàu",
        address = "499/6/15 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g10/93522/prof/s180x180/foody-mobile-18-1-jpg-635463894068392914.jpg"
    ),
    Restaurant(
        id = 204162,
        name = "Trà Sữa Green Life",
        address = "Đường Số 21",
        image = "https://images.foody.vn/res/g21/204162/prof/s180x180/foody-mobile-c2-jpg-991-635882741531098219.jpg"
    ),
    Restaurant(
        id = 91811,
        name = "Phương Thu - Bún Chả Hà Nội",
        address = "152 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g10/91811/prof/s180x180/foody-mobile-hmb-jpg-635454239219362175.jpg"
    ),
    Restaurant(
        id = 642375,
        name = "Feel Coffee & Tea Express - Nguyễn Văn Bảo",
        address = "1 Nguyễn Văn Bảo",
        image = "https://images.foody.vn/res/g65/642375/prof/s180x180/foody-mobile-hmb-jpg-405-636249971911506340.jpg"
    ),
    Restaurant(
        id = 755920,
        name = "Hệ Thống Hàu 5K Sài Gòn - Lê Đức Thọ",
        address = "265 Lê Đức Thọ, P. 17",
        image = "https://images.foody.vn/res/g76/755920/prof/s180x180/foody-upload-api-foody-mobile-img_1373-jpg-180702154003.jpg"
    ),
    Restaurant(
        id = 105242,
        name = "Bánh Tráng Trộn - Cây Trâm",
        address = "Hẻm 21, đường Cây Trâm, P. 8",
        image = "https://images.foody.vn/res/g11/105242/prof/s180x180/foody-mobile-hmb-banh-trang-jpg-295-635533894138148765.jpg"
    ),
    Restaurant(
        id = 190128,
        name = "Kem Dừa",
        address = "Nguyễn Văn Bảo",
        image = "https://images.foody.vn/res/g20/190128/prof/s180x180/foody-mobile-kem-dua-jpg-863-635845790214399515.jpg"
    ),
    Restaurant(
        id = 201269,
        name = "Cơm Tấm 144",
        address = "144 Phạm Ngũ Lão",
        image = "https://images.foody.vn/res/g21/201269/prof/s180x180/foody-mobile-hmb-s-jpg-178-635875058308162444.jpg"
    ),
    Restaurant(
        id = 29785,
        name = "Suối Ngàn Cafe - Nguyễn Huy Điển",
        address = "2 Bis Nguyễn Huy Điển, P. 7",
        image = "https://images.foody.vn/res/g3/29785/prof/s180x180/foody-mobile-l9kqb2bl-jpg-136-636038240592583554.jpg"
    ),
    Restaurant(
        id = 191804,
        name = "Lado Coffee",
        address = "Khu Tập Thể Biên Phòng, 683/1 Nguyễn Kiệm, P. 3",
        image = "https://images.foody.vn/res/g20/191804/prof/s180x180/foody-mobile-t1-jpg-563-635937212653017917.jpg"
    ),
    Restaurant(
        id = 195597,
        name = "Quán Lâm Bô - Xiên Que & Lẩu Nhúng",
        address = "1 Quang Trung",
        image = "https://images.foody.vn/res/g20/195597/prof/s180x180/foody-mobile-y-jpg-470-635857010749580560.jpg"
    ),
    Restaurant(
        id = 193595,
        name = "Đông Hà - Bánh Canh Cá Lóc - Nguyên Hồng",
        address = "27Bis Nguyên Hồng, P. 1",
        image = "https://images.foody.vn/res/g20/193595/prof/s180x180/foody-mobile-banh-canh-hai-lang-q-523-635851836915633235.jpg"
    ),
    Restaurant(
        id = 259345,
        name = "Xiên Nướng Phố Vui",
        address = "102/44 Lê Văn Thọ, P. 25",
        image = "https://images.foody.vn/res/g26/259345/prof/s180x180/foody-mobile-foody-xien-nuong-jpg-728-636053031133753220.jpg"
    ),
    Restaurant(
        id = 79534,
        name = "Lúa Cafe",
        address = "179 Thống Nhất, P. 11  ",
        image = "https://images.foody.vn/res/g8/79534/prof/s180x180/foody-mobile-lua-cafe-tp-hcm-140608113744.jpg"
    ),
    Restaurant(
        id = 868050,
        name = "Kyoto SaBo - Quán Ăn Nhật Bản",
        address = "138/40/11 Nguyễn Duy Cung, P. 12",
        image = "https://images.foody.vn/res/g87/868050/prof/s180x180/foody-upload-api-foody-mobile-yuyu-190619142557.jpg"
    ),
    Restaurant(
        id = 698663,
        name = "Bún Đậu Tre - Quang Trung",
        address = "775 Quang Trung, P. 12",
        image = "https://images.foody.vn/res/g70/698663/prof/s180x180/foody-mobile-c2-jpg.jpg"
    ),
    Restaurant(
        id = 736009,
        name = "Hok Tea - Trà Sữa Đài Loan - Bạch Đằng",
        address = "223 - 225 Bạch Đằng, P. 3",
        image = "https://images.foody.vn/res/g74/736009/prof/s180x180/foody-upload-api-foody-mobile-hmbb-jpg-180424173003.jpg"
    ),
    Restaurant(
        id = 720463,
        name = "Ăn Vặt Na Shop - Chân Gà Sả Tắc - Shop Online",
        address = "169 Huỳnh Văn Nghệ, P. 12",
        image = "https://images.foody.vn/res/g73/720463/prof/s180x180/foody-mobile-hmn-jpg.jpg"
    ),
    Restaurant(
        id = 248982,
        name = "T.A Trà Sữa & Boardgame",
        address = "226/2 Nguyễn Thái Sơn, P. 4",
        image = "https://images.foody.vn/res/g25/248982/prof/s180x180/foody-mobile-dhfjkgkfjhl-jpg-257-636023753846267906.jpg"
    ),
    Restaurant(
        id = 126434,
        name = "Nướng Ngói 133",
        address = "133 Đường Số 11, P. 11",
        image = "https://images.foody.vn/res/g13/126434/prof/s180x180/foody-mobile-vyuig-jpg-833-635623743317265230.jpg"
    ),
    Restaurant(
        id = 703133,
        name = "Lẩu Dê Bình Dương - Đường Số 27",
        address = "44A Đường Số 27",
        image = "https://images.foody.vn/res/g71/703133/prof/s180x180/foody-mobile-1-jpg.jpg"
    ),
    Restaurant(
        id = 739551,
        name = "Nước Mía Xanh Lá",
        address = "109 Đường Số 8, P. 11",
        image = "https://images.foody.vn/res/g74/739551/prof/s180x180/foody-upload-api-foody-mobile-1-jpg-180521085522.jpg"
    ),
    Restaurant(
        id = 119303,
        name = "Bánh Phô Mai Ông Già",
        address = "133/36/60 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g12/119303/prof/s180x180/foody-mobile-foody-banh-pho-mai-o-747-635584691616795373.jpg"
    ),
    Restaurant(
        id = 1262,
        name = "Dạ Khúc Cafe",
        address = "32/100 Đường 59, P. 14",
        image = "https://images.foody.vn/res/g1/1262/prof/s180x180/foody-mobile-da-khuc-cafe.jpg"
    ),
    Restaurant(
        id = 750651,
        name = "Royal Tea Viet Nam By HongKong - 5 Quang Trung",
        address = "5 Quang Trung",
        image = "https://images.foody.vn/res/g76/750651/prof/s180x180/foody-upload-api-foody-mobile-hmn-jpg-180614093625.jpg"
    ),
    Restaurant(
        id = 670159,
        name = "Bún Đậu Mẹt Mắm Tôm",
        address = "282 Thống Nhất, P. 16",
        image = "https://images.foody.vn/res/g68/670159/prof/s180x180/foody-mobile-c2-jpg-349-636341761885893476.jpg"
    ),
    Restaurant(
        id = 178062,
        name = "Dê Tươi Mái Lá",
        address = "1 Huỳnh Văn Nghệ, P. 12",
        image = "https://images.foody.vn/res/g18/178062/prof/s180x180/foody-mobile-hmb-h-jpg-622-635809354088548078.jpg"
    ),
    Restaurant(
        id = 294284,
        name = "Menu24h.vn - Tổ Chức Tiệc Lưu Động ",
        address = "334/5 Nguyễn Văn Nghi",
        image = "https://images.foody.vn/res/g30/294284/prof/s180x180/foody-mobile-c2-jpg-732-636149743076727616.jpg"
    ),
    Restaurant(
        id = 711697,
        name = "Chả Lụa Bò 39 - Shop Online",
        address = "709 Phan Văn Trị, P. 7",
        image = "https://images.foody.vn/res/g72/711697/prof/s180x180/foody-mobile-wf-jpg.jpg"
    ),
    Restaurant(
        id = 251690,
        name = "TYKEO - The Drink And Workshop",
        address = "497/8/17 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g26/251690/prof/s180x180/foody-mobile-asd-jpg-588-636167213811062542.jpg"
    ),
    Restaurant(
        id = 719514,
        name = "Ẩm Thực Chay Thiện Trí",
        address = "Nhà Công Vụ Số 2, Phạm Ngũ Lão, P. 3",
        image = "https://images.foody.vn/res/g72/719514/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 699610,
        name = "Cỏ Coffee",
        address = "1189 Phan Văn Trị, P. 10",
        image = "https://images.foody.vn/res/g70/699610/prof/s180x180/foody-mobile-t6-jpg.jpg"
    ),
    Restaurant(
        id = 33341,
        name = "Cơm Tấm Tứ Hải",
        address = "300 Phan Huy Ích, P. 12",
        image = "https://images.foody.vn/res/g4/33341/prof/s180x180/foody-mobile-com-tam-tu-hai-tp-hcm.jpg"
    ),
    Restaurant(
        id = 702779,
        name = "Hủ Tiếu Nam Vang Ngọc Nhung",
        address = "534 Nguyễn Thái Sơn",
        image = "https://images.foody.vn/res/g71/702779/prof/s180x180/foody-mobile-t2-jpg.jpg"
    ),
    Restaurant(
        id = 281921,
        name = "1987 - Quán Trà Sữa",
        address = "214/9 Nguyễn Oanh, P. 17",
        image = "https://images.foody.vn/res/g29/281921/prof/s180x180/foody-mobile-11-jpg-223-636112609249239785.jpg"
    ),
    Restaurant(
        id = 688348,
        name = "A Ke - Mì Xào Giòn",
        address = "Nguyễn Thái Sơn, P. 4",
        image = "https://images.foody.vn/res/g69/688348/prof/s180x180/foody-mobile-xax-jpg-880-636409780273043426.jpg"
    ),
    Restaurant(
        id = 748978,
        name = "Venus Coffee & Tea",
        address = "80/113 Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g75/748978/prof/s180x180/foody-upload-api-foody-mobile-venuscoffee-jpg-180612115530.jpg"
    ),
    Restaurant(
        id = 703484,
        name = "Bánh Canh Cua 36 - Lê Lai",
        address = "36 Lê Lai",
        image = "https://images.foody.vn/res/g71/703484/prof/s180x180/foody-mobile-gh-jpg.jpg"
    ),
    Restaurant(
        id = 227741,
        name = "Quán Chay An Khang",
        address = "179 Cây Trâm",
        image = "https://images.foody.vn/res/g23/227741/prof/s180x180/foody-mobile-an-khang-jpg.jpg"
    ),
    Restaurant(
        id = 88033,
        name = "Bánh Xèo Miền Trung - Nguyễn Văn Lượng",
        address = "Nguyễn Văn Lượng",
        image = "https://images.foody.vn/res/g9/88033/prof/s180x180/foody-mobile-banh-xeo-jpg-561-636133344915558335.jpg"
    ),
    Restaurant(
        id = 190917,
        name = "Hoa Ban Quán - Đặc Sản Heo Tộc",
        address = "245/31 Đường Số 20 (Dương Quảng Hàm), P. 5",
        image = "https://images.foody.vn/res/g20/190917/prof/s180x180/foody-mobile-h1g-jpg-449-635852755832283227.jpg"
    ),
    Restaurant(
        id = 723083,
        name = "Little Saigon - Bún Đậu Mẹt",
        address = "376 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g73/723083/prof/s180x180/foody-upload-api-foody-mobile-hmd-jpg-180312112831.jpg"
    ),
    Restaurant(
        id = 131706,
        name = "Phở Chú Phương - Đường Số 18",
        address = "16/6 Đường Số 18, P. 8",
        image = "https://images.foody.vn/res/g14/131706/prof/s180x180/foody-mobile-hmb-pho-jpg-651-635651418518296409.jpg"
    ),
    Restaurant(
        id = 300928,
        name = "Gocsi Cafe",
        address = "Hẻm 89 Đường Số 59, P. 14",
        image = "https://images.foody.vn/res/g31/300928/prof/s180x180/foody-mobile-t1-jpg-897-636167090297439239.jpg"
    ),
    Restaurant(
        id = 715881,
        name = "Trà Sữa Milky Homemade",
        address = "319/20 Lê Văn Thọ, P. 9",
        image = "https://images.foody.vn/res/g72/715881/prof/s180x180/foody-mobile-hmkk-jpg.jpg"
    ),
    Restaurant(
        id = 25262,
        name = "XP Garden Cafe - Cafe Sân Vườn",
        address = "94/1048 Dương Quảng Hàm, P. 6",
        image = "https://images.foody.vn/res/g3/25262/prof/s180x180/foody-mobile-hp7yf680-jpg-224-635797289143952474.jpg"
    ),
    Restaurant(
        id = 279774,
        name = "Bún Chả Cá Minh Sơn - Đường Số 12",
        address = "34D Đường Số 12, P. 11",
        image = "https://images.foody.vn/res/g28/279774/prof/s180x180/foody-mobile-15l-jpg-897-636108515621412607.jpg"
    ),
    Restaurant(
        id = 90195,
        name = "Quán Sen - Nhậu Bình Dân",
        address = "310/29A Đường15, Dương Quảng Hàm, P. 5",
        image = "https://images.foody.vn/res/g10/90195/prof/s180x180/foody-mobile-33-1-jpg-635437093404350988.jpg"
    ),
    Restaurant(
        id = 628365,
        name = "Trà Sữa Nhà Làm - Quang Trung",
        address = "656/65/4 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g63/628365/prof/s180x180/foody-mobile-1-jpg-600-636437443085883001.jpg"
    ),
    Restaurant(
        id = 693524,
        name = "Suri Quán - Đùi Gà Bó Xôi",
        address = "656/65/2 Quang Trung, P. 11",
        image = "https://images.foody.vn/res/g70/693524/prof/s180x180/foody-upload-api-foody-mobile-hj-gif-180807091902.jpg"
    ),
    Restaurant(
        id = 4181,
        name = "My Way Cafe - Phan Văn Trị",
        address = "18 Phan Văn Trị, Phường 10",
        image = "https://images.foody.vn/res/g1/4181/prof/s180x180/foody-mobile-my-way-cafe-phan-van-tri.jpg"
    ),
    Restaurant(
        id = 142623,
        name = "Trâm Anh - Lẩu Nấm Giao Hàng Tận Nơi",
        address = "129/7 Đường Số 3, P. 11",
        image = "https://images.foody.vn/res/g15/142623/prof/s180x180/foody-mobile-images802367_naulau_-872-635694659328853613.jpg"
    ),
    Restaurant(
        id = 227734,
        name = "Fins Coffee - Cây Trâm",
        address = "8 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g23/227734/prof/s180x180/foody-mobile-foody-checkin-fins-c-136-635961360492423063.jpg"
    ),
    Restaurant(
        id = 31654,
        name = "Quán Xuân - Cháo Gỏi Vịt",
        address = "35 -37 Lê Văn Thọ , P. 8",
        image = "https://images.foody.vn/res/g4/31654/prof/s180x180/foody-mobile-7ztur6s2-jpg-653-636014085021883514.jpg"
    ),
    Restaurant(
        id = 753796,
        name = "Bún Đậu Ngon Cây Trâm",
        address = "298 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g76/753796/prof/s180x180/foody-upload-api-foody-mobile-21l-jpg-180907083726.jpg"
    ),
    Restaurant(
        id = 222099,
        name = "Phở Vị Hoàng",
        address = "2 - 4 Lê Lai, P. 3",
        image = "https://images.foody.vn/res/g23/222099/prof/s180x180/foody-mobile-hmb-s-jpg-800-635945231243154731.jpg"
    ),
    Restaurant(
        id = 143560,
        name = "HUP Acoustic Cafe - Nguyễn Văn Lượng",
        address = "312 Nguyễn Văn Lượng, P. 16",
        image = "https://images.foody.vn/res/g15/143560/prof/s180x180/foody-mobile--9-_hinhmob-jpg-838-635697090762044715.jpg"
    ),
    Restaurant(
        id = 215514,
        name = "Su Que 79",
        address = "120 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g22/215514/prof/s180x180/foody-mobile-c2-jpg-991-635925914215874991.jpg"
    ),
    Restaurant(
        id = 670141,
        name = "Siucha - Trà Sữa",
        address = "204 Lê Văn Thọ, P. 11",
        image = "https://images.foody.vn/res/g68/670141/prof/s180x180/foody-mobile-kinh-doanh-tra-sua-j-122-636342621927336142.jpg"
    ),
    Restaurant(
        id = 119061,
        name = "Hủ Tiếu Miền Tây - Nguyễn Văn Bảo",
        address = "Hẻm 59 Nguyễn Văn Bảo, P. 4",
        image = "https://images.foody.vn/res/g12/119061/prof/s180x180/foody-mobile-buih-jpg-298-635582234259738787.jpg"
    ),
    Restaurant(
        id = 290784,
        name = "Dasis Tea & Coffee House - Emart Gò Vấp",
        address = "Emart Gò Vấp, 366 Phan Văn Trị, P. 5",
        image = "https://images.foody.vn/res/g30/290784/prof/s180x180/foody-mobile-12-jpg-347-636180826812716093.jpg"
    ),
    Restaurant(
        id = 108409,
        name = "Phở Lộc - Quang Trung",
        address = "96 Quang Trung, P. 10",
        image = "https://images.foody.vn/res/g11/108409/prof/s180x180/foody-mobile-hmb-pho-jpg-516-635548622192316370.jpg"
    ),
    Restaurant(
        id = 97126,
        name = "Cò Kếu Bờ Sông - Quán Ăn Ven Sông",
        address = "147B Đường Số 23 , P. 5",
        image = "https://images.foody.vn/res/g10/97126/prof/s180x180/foody-mobile-foody-co-keu-bo-song-574-635487984709449743.jpg"
    ),
    Restaurant(
        id = 789629,
        name = "The Menu Restaurant - Pizza & Pasta",
        address = "188 Phan Huy Ích, P. 12",
        image = "https://images.foody.vn/res/g79/789629/prof/s180x180/foody-upload-api-foody-mobile-hmnnn-jpg-181023103332.jpg"
    ),
    Restaurant(
        id = 209034,
        name = "Trà Sữa Happy Life - Phan Huy Ích",
        address = "18 - 19 Phan Huy Ích, P. 12",
        image = "https://images.foody.vn/res/g21/209034/prof/s180x180/foody-mobile-12036817_62416915772-951-635900215971073882.jpg"
    ),
    Restaurant(
        id = 222894,
        name = "Simple Coffee - Acoustic Live",
        address = "498 Cây Trâm, P. 9",
        image = "https://images.foody.vn/res/g23/222894/prof/s180x180/foody-mobile-hfrutmst-jpg-790-635953792826189786.jpg"
    ),
    Restaurant(
        id = 122262,
        name = "Coconut Coffee ",
        address = "730/46 Lê Đức Thọ, P. 15",
        image = "https://images.foody.vn/res/g13/122262/prof/s180x180/foody-mobile-coconut-coffee-jpg-237-635969971013697499.jpg"
    ),
    Restaurant(
        id = 731325,
        name = "Bánh Mì Xíu Mại Gốc Đà Lạt",
        address = " 76 Đường Số 1, P. 16",
        image = "https://images.foody.vn/res/g74/731325/prof/s180x180/foody-upload-api-foody-mobile-foody-banh-mi-xiu-ma-180413154451.jpg"
    ),
    Restaurant(
        id = 703109,
        name = "Hồng Ngọc - Bún Chả Hà Nội & Bún Đậu Mắm Tôm",
        address = "210 Lê Đức Thọ, P. 6",
        image = "https://images.foody.vn/res/g71/703109/prof/s180x180/foody-mobile-hmmmmmmmmmm-jpg.jpg"
    ),
    Restaurant(
        id = 131446,
        name = "Chè Bưởi Vĩnh Long - Thống Nhất",
        address = "399 Thống Nhất, P. 11",
        image = "https://images.foody.vn/res/g14/131446/prof/s180x180/foody-mobile-4-jpg.jpg"
    ),
    Restaurant(
        id = 741043,
        name = "Bánh Tráng Trộn & Cuốn - Lê Đức Thọ",
        address = "57/38 Lê Đức Thọ, P. 7",
        image = "https://images.foody.vn/res/g75/741043/prof/s180x180/foody-upload-api-foody-mobile-foody-banh-trang-tro-180523092858.jpg"
    ),
    Restaurant(
        id = 291837,
        name = "Gấu Crepe & Milk Tea",
        address = "108 Đường Số 1, P. 16",
        image = "https://images.foody.vn/res/g30/291837/prof/s180x180/foody-upload-api-foody-mobile-26l-jpg-180917103333.jpg"
    ),
    Restaurant(
        id = 778009,
        name = "The Check In - Sữa Đậu Nành & Sữa Chua Lắc",
        address = "381 Lê Văn Thọ, P. 9",
        image = "https://images.foody.vn/res/g78/778009/prof/s180x180/foody-upload-api-foody-mobile-44l-jpg-180919090558.jpg"
    ),
)