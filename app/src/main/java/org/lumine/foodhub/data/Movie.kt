package org.lumine.foodhub.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Movie(
    val id: Long? = null,
    val overview: String? = null,
    @SerialName(value = "poster_path") val posterPath: String? = null,
    val title: String? = null,
)