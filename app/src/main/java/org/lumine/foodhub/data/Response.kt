package org.lumine.foodhub.data

import kotlinx.serialization.Serializable

@Serializable
data class Response(
    val results: List<Movie>? = null,
)