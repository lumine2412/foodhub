# FoodHub

## Demo video

![](./demo.webm)

UI tested on Google Pixel 3a XL API 30.

## Members

| Name                  | ID       |
|-----------------------|----------|
| Đỗ Thế Khang          | 19207076 |
| Nguyễn Quốc Huy       | 19207074 |
| Trịnh Ngọc Huy        | 19207075 |
| Nguyễn Hoàng Quốc Huy | 19207007 |